Sub aa()
'
' aa Macro
'

'
    Workbooks.Add Template:= _
        "C:\Users\Christian\Documents\Modèles Office personnalisés\donne.xltm"
    Range("J3:P3").Select
    ActiveCell.FormulaR1C1 = "ARD"
    Range("A1:AG1").Select
    ActiveCell.FormulaR1C1 = "Titre"
    Range("I7:P10").Select
    ActiveCell.FormulaR1C1 = "1"
    Range("AA3").Select
    ActiveCell.FormulaR1C1 = "1"
    Range("AA4").Select
    ActiveCell.FormulaR1C1 = "3SA"
    Range("AB4").Select
    ActiveCell.FormulaR1C1 = " ="
    Range("AA5").Select
    ActiveCell.FormulaR1C1 = "10©"
    With ActiveCell.Characters(Start:=3, Length:=1).Font
        .Name = "Symbol"
        .FontStyle = "Normal"
        .Size = 11
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .Color = -16776961
        .TintAndShade = 0
        .ThemeFont = xlThemeFontNone
    End With
    Range("Y7").Select
    ActiveCell.FormulaR1C1 = "1SA"
    Range("Z7").Select
    ActiveCell.FormulaR1C1 = "-"
    Range("AA7").Select
    ActiveCell.FormulaR1C1 = "2SA"
    Range("AB7").Select
    ActiveCell.FormulaR1C1 = " -"
    Range("Y8").Select
    ActiveCell.FormulaR1C1 = "3SA"
    Range("Z8").Select
    ActiveCell.FormulaR1C1 = " -"
    Range("Z8").Select
    ActiveCell.FormulaR1C1 = "fin"
    Range("AC6:AG10").Select
    ActiveCell.FormulaR1C1 = "Blabla" & Chr(10) & "toti"
    Range("Y11:AG14").Select
   Application.PrintCommunication = True
    ActiveSheet.ExportAsFixedFormat Type:=xlTypePDF, Filename:= _
        "C:\Home-Christian\dup1-out.pdf", Quality:=xlQualityStandard, _
        IncludeDocProperties:=True, IgnorePrintAreas:=False, OpenAfterPublish:= _
        True
    Sheets("Donne-2").Select
    Range("J3:P3").Select
    ActiveCell.FormulaR1C1 = "X987"
    Range("I7:P10").Select
    ActiveCell.FormulaR1C1 = "2"
    Range("AA3").Select
    ActiveCell.FormulaR1C1 = "2"
    Range("A1:AG1").Select
    ActiveCell.FormulaR1C1 = "Titre"
    ActiveSheet.ExportAsFixedFormat Type:=xlTypePDF, Filename:= _
        "C:\Home-Christian\dup2-out.pdf", Quality:=xlQualityStandard, _
        IncludeDocProperties:=True, IgnorePrintAreas:=False, OpenAfterPublish:= _
        True
    Application.Goto Reference:="Classeur1!aa"
End Sub
