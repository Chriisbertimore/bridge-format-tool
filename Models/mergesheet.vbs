Sub Merge()
'
' Macro1 Macro
'

'   Merge 8 sheets
    Sheets(N$).Select
    Sheets.Add After:=ActiveSheet
    Sheets("Feuil1").Select
    Sheets("Feuil1").Name = "Toutes"
 '
    Sheets("1").Select
    Range("A1:AG14").Select
    Selection.Copy
    Sheets("Toutes").Select
    Range("A4").Select
    Selection.PasteSpecial Paste:=xlPasteColumnWidths, Operation:=xlNone, _
        SkipBlanks:=False, Transpose:=False
    ActiveSheet.Paste
'
    Sheets("2").Select
    Range("A1:AG14").Select
    Application.CutCopyMode = False
    Selection.Copy
    Sheets("Toutes").Select
    Range("A22").Select
    Selection.PasteSpecial Paste:=xlPasteColumnWidths, Operation:=xlNone, _
        SkipBlanks:=False, Transpose:=False
    ActiveSheet.Paste
    ActiveWindow.SmallScroll Down:=12
'
    Sheets("3").Select
    Range("A1:AG2").Select
    Application.CutCopyMode = False
    Selection.Copy
    Range("A1:AG14").Select
    Application.CutCopyMode = False
    Selection.Copy
    Sheets("Toutes").Select
    Range("A40").Select
    Selection.PasteSpecial Paste:=xlPasteColumnWidths, Operation:=xlNone, _
        SkipBlanks:=False, Transpose:=False
    ActiveSheet.Paste
'
    Sheets("4").Select
    Range("A1:AG14").Select
    Application.CutCopyMode = False
    Selection.Copy
    Sheets("Toutes").Select
    Range("A58").Select
    Selection.PasteSpecial Paste:=xlPasteColumnWidths, Operation:=xlNone, _
        SkipBlanks:=False, Transpose:=False
    ActiveSheet.Paste
'
    Sheets("5").Select
    Range("A1:AG14").Select
    Selection.Copy
    Sheets("Toutes").Select
    Range("A76").Select
    Selection.PasteSpecial Paste:=xlPasteColumnWidths, Operation:=xlNone, _
        SkipBlanks:=False, Transpose:=False
    ActiveSheet.Paste
 '
    Sheets("6").Select
    Range("A1:AG14").Select
    Selection.Copy
    Sheets("Toutes").Select
    Range("A94").Select
    Selection.PasteSpecial Paste:=xlPasteColumnWidths, Operation:=xlNone, _
        SkipBlanks:=False, Transpose:=False
    ActiveSheet.Paste
 '
    Sheets("7").Select
    Range("A1:AG14").Select
    Selection.Copy
    Sheets("Toutes").Select
    Range("A112").Select
    Selection.PasteSpecial Paste:=xlPasteColumnWidths, Operation:=xlNone, _
        SkipBlanks:=False, Transpose:=False
    ActiveSheet.Paste
'
    Sheets("8").Select
    Range("A1:AG14").Select
    Selection.Copy
    Sheets("Toutes").Select
    Range("A130").Select
    Selection.PasteSpecial Paste:=xlPasteColumnWidths, Operation:=xlNone, _
        SkipBlanks:=False, Transpose:=False
    ActiveSheet.Paste

    ActiveWorkbook.Save
End Sub
