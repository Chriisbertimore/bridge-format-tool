Bridge Club de Grenoble - 26 rue du Col. Dumont 38000 Grenoble           
Ecole de bridge                STAGE DE RENTREE J3             2019.09.14

Donne -   1  a              Donneur:N   Vul:T  Contrat:3SA/N    Entame:AP
                            Resultat:9    Commentaires                   
             P-ARDV         |N |E |S |O |                                
             C-X987         |3S|. |  |  |                                
             K-6543                                                      
             T-2                                                         
                                                                         
P-X987          N       P- 2                                             
C-6543         O E      C- ARDV                                          
K-2             S       K- X987                                          
T-ARDV                  T- 6543                                          
                                                                         
             P-6543                                                      
             C-2                                                         
             K-ARDV                                                      
             T-X987                                                      
                                                                         

Donne -   4  d              Donneur:N   Vul:N  Contrat:3SA/O    Entame:AP
                            Resultat:10   Commentaires                   
             P-ARDV         |N |E |S |O | Ouest affranchit ses Carreaux e
             C-X987         |- |- |- |1S|                                
             K-6543         |- |3S|- |- |                                
             T-2            |- |  |  |  |                                
                                                                         
P-2             N       P- X987                                          
C-ARDV         O E      C- 6543                                          
K-X987          S       K- 2                                             
T-6543                  T- ARDV                                          
                                                                         
             P-6543                                                      
             C-2                                                         
             K-ARDV                                                      
             T-X987                                                      
                                                                         

Donne -   5  e              Donneur:N   Vul:P  Contrat:3K/S     Entame:4K
                            Resultat:9    Commentaires                   
             P-A9742        |N |E |S |O | Pas focement 6  cartes a Pique.
             C-AV5          |1P|- |2K|- | 3K est limit� a 11H avec 6carte
             K-V5           |2P|- |3K|. | 3T �gal.                       
             T-D43                                                       
                                                                         
P-RX8           N       P- V63                                           
C-RX73         O E      C- 9642                                          
K-X84           S       K- D3                                            
T-R98                   T- AVX2                                          
                                                                         
             P-D5                                                        
             C-D8                                                        
             K-AR9762                                                    
             T-765                                                       
                                                                         

						P-1
Bridge Club de Grenoble - 26 rue du Col. Dumont 38000 Grenoble           
Ecole de bridge                STAGE DE RENTREE J3             2019.09.14

Donne -   6  e              Donneur:E   Vul:N  Contrat:4C/E     Entame:DK
                            Resultat:11   Commentaires                   
             P-VX9          |E |S |O |N | 3K est un Texas pour les Coeurs
             C-D9           |2T|- |2K|- | 4C+1.                          
             K-VX32         |2S|- |3K|- |                                
             T-AR98         |3C|- |4C|. |                                
                                                                         
P-32            N       P- AD84                                          
C-R86532       O E      C- AVX                                           
K-64            S       K- ARD                                           
T-532                   T- DVX                                           
                                                                         
             P-R765                                                      
             C-74                                                        
             K-9875                                                      
             T-764                                                       
                                                                         

Donne -   7  e              Donneur:S   Vul:E  Contrat:4C/O     Entame:RP
                            Resultat:10   Commentaires                   
             P-RDV73        |S |O |N |E | 2C limite la main d'Ouest.     
             C-7            |- |1C|1P|2K| L'affranchissement des Carreaux
             K-D63          |- |2C|- |4C|                                
             T-AX7          |. |  |  |  |                                
                                                                         
P-A84           N       P- X5                                            
C-RDVX85       O E      C- A92                                           
K-X5            S       K- AR874                                         
T-R53                   T- V84                                           
                                                                         
             P-962                                                       
             C-643                                                       
             K-V92                                                       
             T-D962                                                      
                                                                         

Donne -   8  e              Donneur:O   Vul:T  Contrat:4P/N     Entame:2P
                            Resultat:11   Commentaires                   
             P-ARDX98       |O |N |E |S | 11 levees possibles.           
             C-AV           |- |2T|- |2K|                                
             K-DVX          |- |2P|- |4P|                                
             T-AX           |. |  |  |  |                                
                                                                         
P-3             N       P- 542                                           
C-6432         O E      C- R75                                           
K-32            S       K- A854                                          
T-RV7432                T- D85                                           
                                                                         
             P-V76                                                       
             C-DX98                                                      
             K-R976                                                      
             T-96                                                        
                                                                         

						P-2
Bridge Club de Grenoble - 26 rue du Col. Dumont 38000 Grenoble           
Ecole de bridge                STAGE DE RENTREE J3             2019.09.14

Donne -   9  e              Donneur:N   Vul:N  Contrat:4C/S     Entame:RK
                            Resultat:10   Commentaires                   
             P-A9853        |N |E |S |O | Sur un bicolore 2C annonce 6 ca
             C-RD4          |1P|- |1S|- | 10 ou 11 levees.               
             K-V            |2T|- |2C|- |                                
             T-DV94         |3C|- |4C|. |                                
                                                                         
P-DVX           N       P- R642                                          
C-5            O E      C- 763                                           
K-RDX3          S       K- 9642                                          
T-RX876                 T- A5                                            
                                                                         
             P-7                                                         
             C-AVX982                                                    
             K-A875                                                      
             T-32                                                        
                                                                         

Donne -   10 e              Donneur:E   Vul:E  Contrat:4C/N     Entame:XT
                            Resultat:10   Commentaires                   
             P-R8           |E |S |O |N | 2 SA indique un main reguliere 
             C-ADX876       |- |1P|- |2C| 10 levees avec la defausse d'un
             K-V74          |- |2S|- |4C|                                
             T-D2           |. |  |  |  |                                
                                                                         
P-V2            N       P- AX53                                          
C-V43          O E      C- R2                                            
K-D532          S       K- 986                                           
T-V543                  T- X976                                          
                                                                         
             P-D9764                                                     
             C-95                                                        
             K-ARX                                                       
             T-AR8                                                       
                                                                         

Donne -   11 e              Donneur:S   Vul:T  Contrat:7P/O     Entame:4T
                            Resultat:13   Commentaires                   
             P-V4           |S |O |N |E | 3P enchere de chelem. 4SA Black
             C-D7654        |- |1P|- |2T| 5SA demande les Rois.          
             K-D64          |- |2K|- |3P| L'affranchissement des Trefles 
             T-X94          |- |4S|- |5P|                                
                                                                         
P-ARX85         N       P- D32                                           
C-A2           O E      C- R3                                            
K-RVX92         S       K- A53                                           
T-5                     T- ARD32                                         
                                                                         
             P-976          |- |5S|- |6C|                                
             C-VX98         |? |? |? |? |                                
             K-87           |? |? |? |? |                                
             T-V876                                                      
                                                                         

						P-3
Bridge Club de Grenoble - 26 rue du Col. Dumont 38000 Grenoble           
Ecole de bridge                STAGE DE RENTREE J3             2019.09.14

Donne -   12 e              Donneur:O   Vul:P  Contrat:6P/E     Entame:2K
                            Resultat:12   Commentaires                   
             P-965          |O |N |E |S | 2P un As majeur; 2SA main regul
             C-X765         |- |- |2K|- | 4SA demande des Rois. 5K un Roi
             K-X76          |2P|- |2S|- | Ouf! Il manque 2 cles mis le RP
             T-X97          |3T|- |3P|- |                                
                                                                         
P-A874          N       P- DVX2                                          
C-V984         O E      C- ARD                                           
K-R954          S       K- ADV                                           
T-6                     T- RDV                                           
                                                                         
             P-R3           |4T|- |4S|- |                                
             C-32           |? |? |? |? |                                
             K-832          |? |? |? |? |                                
             T-A85432                                                    
                                                                         





































						P-4
