#include <stdio.h>

#define BRI_RECORD_SIZE 78
#define DGE_RECORD_SIZE 68  /* 4 * 1700 */
#define DUP_CONTROL_AREA_SIZE 10
#define DUP_RECORD_SIZE BRI_RECORD_SIZE + DGE_RECORD_SIZE + DUP_CONTROL_AREA_SIZE
#define DGE_HAND_SIZE 17  /* 12 + 4 */

unsigned char board [DUP_RECORD_SIZE];
FILE *dupfile;
/* Translation table 0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ */ 
unsigned char t[] = "--23456789:;<=>?@AbcdefghiVRlmnopDrsXuvwxyz";


void print_hand (unsigned char * p, int index) {
int limit = index + DGE_HAND_SIZE;
unsigned char c;

    while (index < limit) {
	c = p[index++];
	switch (c) {
	  case 0x06:
	       printf ("Piques: "); break;	
	  case 0X03: 
	       printf ("Coeurs: "); break;	
	  case 0X04: 
	       printf ("Carreaux: "); break;	
	  case 0X05: 
	       printf ("Trèfles: "); break;	
	  case '2':
	  case '3':
	  case '4':
	  case '5':
	  case '6':
	  case '7':
	  case '8':
	  case '9':
	  case 'T': 
	  case 'J':  
	  case 'Q': 
	  case 'K': 
	  case 'A': 
	       printf ("%c ", t[c-0x30] ); break;
	  default: 
	       printf ("Error: invalid character %d \n", c); 
	}
    }
}

void board_dump (unsigned  char *b) {
int i, r, control[10];
char fname [10] ;
static int count = 0;
FILE* out;
	
	for (i=0; i<10; i++) control[i] = b[147+i];
        count++;
        sprintf (fname, "./d%02d", count); 
	out = fopen (fname, "wb");
	fprintf (stderr, "Creating file %s \n", fname);
        r = fwrite (b, DUP_RECORD_SIZE, 1, out);
	fclose (out);
}

int main (int argc, char *argv[]) {
int i, r;
FILE *fdest, *fdup;
char *dest;

   if (argc < 4) fprintf (stderr, "Syntax: adup dest_file input_files\n");
   dest = argv [1];
   fdest = fopen (dest, "wb");
   for (i=2; i<argc; i++) {
       fdup = fopen (argv[i], "rb");
       while ((r = fread (board, DUP_RECORD_SIZE, 1, fdup)) == 1) {
          fwrite (board, DUP_RECORD_SIZE, 1, fdest);
       }
       fclose (fdup);
   }
   fprintf (stderr, "Created %s \n", dest);
   fclose (fdest); 
   return r;
}
