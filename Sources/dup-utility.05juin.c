/*
 *  Utility programs to manipulate Bridge duplication files
 *  Christian Bertin - December 2018
 *  (christian.bert1@free.fr)
 *  This program source is under GNU General Public License v2 and later
 */
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <strings.h>
#include <malloc.h>
#include <libgen.h>
#include <stdarg.h>
#include <dirent.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <stdlib.h>

#define STRINGIFY(x) #x 
#define RADIX idup
#define VERSION(X)  STRINGIFY(X) " 2.0"
#define CONFIG_FILE(X) STRINGIFY(X) ".cfg"
#define TMPNAME(X) STRINGIFY(X) "XXXXXX"
#define BATFILENAME "pbn_to_xl.bat"
#define DEFAULTMODELNAME "c:\\Home-Christian\\Dropbox\\Private\\Dup-utils\\Models\\2MF.csv"
#define DEFAULTDEFNAME "c:\\Home-Christian\\Dropbox\\Private\\Dup-utils\\Models\\2MF.def"

uint8_t *pword =
  "C:\\\"Program Files\"\\\"Microsoft Office 15\"\\root\\office15\\winword.exe /x /f ";
uint8_t *pcmd = "cmd.exe";

#define forever while (1)
#define NOT !
#define LINE_SIZE 80
#define FATAL(n, ...)						\
  do { fprintf (stderr,__VA_ARGS__);  fprintf (stderr, "\n");   \
    exit (n);							\
  } while(0) 
#define ERROR(n, ...)						\
  do { fprintf (stderr,__VA_ARGS__);  fprintf (stderr, "\n");   \
    fflush (stderr); return n;					\
  } while(0)
#define SYNTAX(p, ...)						\
  do { fprintf (stderr, "Syntax : %s", synth [p] __VA_ARGS__);	\
    fprintf (stderr, "\n"); fflush (stderr); return -1;		\
  } while(0)
#define LOGERR(...)					        \
  do { fprintf (stderr, __VA_ARGS__); fprintf (stderr, "\n"); \
    fflush (stderr);						\
  } while (0)
#define LOG(...)					        \
  do { fprintf (stdout, __VA_ARGS__);                           \
    fflush (stdout);						\
  } while (0)
#define DEBUG(...)                                              \
  if (debug) 							\
  do { fprintf (stderr, __VA_ARGS__); fprintf (stderr, "\n");   \
    fflush (stderr);						\
  } while (0)

#define U(c) ((((c)>='a') && ((c)<='z'))? ((c)-0x20): (c))
#define isablank(c) ((c==' ')||(c=='\t'))
#define isadigit(c) ((c>='0')&&(c<='9'))
#define isaseparator(c)				                \
  ((c==',')||(c==';')||(c==';')||(c=='.'))
#define isaterminator(c) ((c=='\n')|| (c=='\0'))
#define TRUE 1
#define FALSE 0

#define BOARD_ELEMENT 13
#define MAX_BID 32
#define MAX_SITE 24
#define MAX_EVENT 25
typedef struct _comment {
  uint8_t *kstring     /* (8) string nul-terminated address */
  uint8_t *knext;      /* (8) next line, NULL if last one */
  uint16_t ksize;      /* (2) strlen(kfirst); */
  uint8_t  kchars [1]; /* (ksize+1 bytes) string area at least '\0'  */
} comment_t,
  *pkom_t;
/* 
 * Board representation : A board contains 4 hands, one per cardinal position
 * Each hand has 13 random cards, in 4 suits.
 * A card is defined by its suit [S,H,D,C] ands its height [A,K,Q,J,T,9,8,7,6,5,4,3,2,1]
 * Suit number s is in [0-3], 0 for S
 * Card number c is in [0-12], 0 for Ace .. 12 for Two 
 * Hand number h is in [0-3],  0 for North, 1 for East, 2 for South, 3 for West
 * Table uint8_t bb[13] is a vector of 52 2-bit elements for encoding the hands
 * Byte 0 = [ AS.AH.AD.AC] ... byte 12 = [2S.2H.2D.2C] 
 * Example: bb[3] == 0x0E => South has the Queen of Diamonds and North the three other queens.
 * Initially, all cards are given the West (0xFF).
 * - tag is a character between ['a' and 'z'] allocated to a series of boards
 * - number is the board index, it is independent of tag
 * - comment is spread on 13 strings c1 to c13 (null-terminated).
 * - dealer is 0 for 'N', 1 for 'E', 2 for 'S', 3 for 'O', 255 for unknown
 * - declarer  0 for 'N', 1 for 'E', 2 for 'S', 3 for 'O', 255 for unknown
 * - vulnerability is 0 for 'P' (None), 1 for 'NS', 2 for 'EW', 3 for 'T' (All), 255 for unknown
 * - contract is an unsigned byte encoding the contract and risk with the formula :
 *   Value = Level * 5 + Denomination + 100 if doubled + 200 if redoubled
 *   Level = [1-7]
 *   Denomination = 0 for Club, 1 for Diamond, 2 for Heart, 3 for Spade and 4 for No-Trump
 *   Default value is 255
 * - starter is is 0 for 'N', 1 for 'E', 2 for 'S', 3 for 'O'. It is the player 
 *   who bid first (normally, the dealer unless  someone called out of turn
 * - Bidding is a max. 16 bid sequence where each call is encoded in an unsigned byte
 *   like contract for the bid which are in the form Level-Denomination
 *   Other declarations are Pass (0), AP for all pass (1), * (3 passes after a bid at least)(2)
 *   X (3) and XX (4)
 * - result is the expected number of tricks made by the declarer, 255 if unkown
 * - leadcard is a number computed as 13*s+c with s in [0-3] and c in [0-12], 255 if unknown. As is 0, Spade is 0.
 * - Site is the location, a string (max.19).
 * - date is encoded a time_t (# of second since 1/1/1900)
 * - kfirst is the address of the first line of comment, klast the address of the last line of comments.
 *   When no comment defined kfirst == klast == NULL
 *   Lines of comments form a linked list. No limit in umber of lines. Line limited to 32767 characters.
 *   Lines of comment do not hold the \n character. This one is added if needed only.
 * - next is (NULL) if the board is the last of the series, otherwise a pointer to next board.
 *
 */
struct board { 
  struct board *next;
  uint16_t number;
  uint8_t tag, dealer, vulnerability, declarer, contract, starter, leadcard, result;
  uint8_t bb [BOARD_ELEMENT];
  time_t date;
  comment_t *kfirst, *klast;
  uint8_t bidding [MAX_BID];
  uint8_t event [MAX_EVENT];
  uint8_t site [MAX_SITE];
};
#define BOARD_SIZE (sizeof(struct board))
#define MAXBOARD 128
/* Board under construction */
struct board b;
/* Input buffer */
uint8_t buf [14];
/* Static board buffer for BRI decoding */
uint8_t bri [BOARD_ELEMENT];
/* Dynamic board buffer for DGE decoding */
uint8_t *dge;
/* Sequensor : seq |i] is true if board n�i+1 is specified */
_Bool seq [MAXBOARD];

/* 
 * DUP Format definition: 
 *     BRI record(78) + DGE record(68) + Control area(10) 
 * BRI encoding: 
 * Only North, East and South hands are encoded 
 * A hand is 13 * 2 characters representing a number from 1 to 52 
 * Spade [01..13], Heart [14..26], Diamond [27..39], Club [40..52] in the order A, K,...,2.
 * DGE encoding:
 * North, East, South and West hands are encoded in 4 consecuttive 17-byte records:
 * S1, <n> card bytes, S2, <m> card bytes, S3 <p> card bytes, S4 <q> card bytes
 * By construction in a valid deal n + m + p + q == 13
 * Suit byte Si is 0x06 for Spade, 0x03 for Heart, 0x04 for Diamond, 0x05 for Club 
 * Cards are represented by one ascii character among 
 * [A,K/R,Q/D,J/V,T/X,9,8,7,6,5,4,3,2,1] 
 * BRI and DGE decodng results must be identical
 */
#define BRI_RECORD_SIZE 78  /* 3 * 26 */
#define BRI_HAND_SIZE 26    /* 2 * 13 */
#define DGE_RECORD_SIZE 68  /* 4 * 17 */
#define DGE_HAND_SIZE 17    /* 13 + 4 */
#define CONTROL_AREA (BRI_RECORD_SIZE + DGE_RECORD_SIZE)
#define DUP_CONTROL_AREA_SIZE 10

/* Board linked list control variables */
struct board *item, *next, *last = NULL, *first = NULL;
enum algo { BRI, DGE, BRE, PBN };
uint8_t *algo_name[] = { "BRI", "DGE", "BRE", "PBN" };
uint8_t algo_size [4] = { BRI_HAND_SIZE, DGE_HAND_SIZE, 0 , 0 };  
uint8_t ctrl [10];
#define DUP_RECORD_SIZE BRI_RECORD_SIZE + DGE_RECORD_SIZE + DUP_CONTROL_AREA_SIZE
uint8_t board_record [DUP_RECORD_SIZE];
_Bool optbox = TRUE, alloc [MAXBOARD];
uint8_t box_order [MAXBOARD];
uint8_t dealers [MAXBOARD];
uint8_t declarers [MAXBOARD];
uint8_t vulns [MAXBOARD];

/* Lucida font translation table */
uint8_t lucida [] = {
  /*0   1,  2,  3,  4,  5,  6,  7,  8,  9 */
    0,  1,  2,  3,  4,  5,  6,  7,  8,  9, /*  0*/
   10, 11, 12, 13, 14, 15, 16, 17, 18, 19, /* 10*/
   20, 21, 22, 23, 24, 25, 26, 27, 28, 29, /* 20*/
   30, 31, 32, 33, 34, 35, 36, 37, 38, 39, /* 30*/
   40, 41, 42, 43, 44, 45, 46, 47, 48, 49, /* 40*/
   50, 51, 52, 53, 54, 55, 56, 57, 58, 59, /* 50*/
   60, 61, 62, 63, 64, 65, 66, 67, 68, 69, /* 60*/
   70, 71, 72, 73, 74, 75, 76, 77, 78, 79, /* 70*/
   80, 81, 82, 83, 84, 85, 86, 87, 88, 89, /* 80*/
   90, 91, 92, 93, 94, 95, 96, 97, 98, 99, /* 90*/
  100,101,102,103,104,105,106,107,108,109, /*100*/
  110,111,112,113,114,115,116,117,118,119, /*110*/
  120,121,122,123,124,125,126,127,128,129, /*120*/
  130,131,132,133,134,135,136,137,138,139, /*130*/
  140,141,142,143,144,145,146,147,148,149, /*140*/
  150,151,152,153,154,155,156,157,158,159, /*150*/
  160,161,162,163,164,165,166,167,168,169, /*160*/
  170,171,172,173,174,175,176,177,178,179, /*170*/
  180,181,182,183,184,185,186,187,188,189, /*180*/
  190,191,192,193,194,195,196,197,198,199, /*190*/
  200,201,202,203,204,205,206,207,208,209, /*200*/
  210,211,212,213,214,215,216,217,218,219, /*210*/
  220,221,222,223,224,225,226,227,228,229, /*220*/
  230,231,232,233,234,235,236,237,238,239, /*230*/
  240,241,242,243,244,245,246,247,248,249, /*240*/
  250,251,252,253,254,255                  /*250*/
};

/* PBN files have a comment section, a declarer, a contract, a result, a dealer, a vulnerability 
 * An Auction and a Play sections (at least the leadcard)
 */

enum pbn {
  _pbn, _event,_site, _date, _west, _north, _east, _south, _board, _dealer,
  _vulnerable, _deal, _scoring, _declarer, _contract, _result, _comment,
  _auction, _play, _none };
struct key {
  uint8_t * key;
  uint8_t ix;
  _Bool init;
};
struct key _PBN [] = {
  { "% PBN \"", 5, FALSE},
  { "[Event \"", 8 , FALSE},
  { "[Site \"", 7 , FALSE},
  { "[Date \"", 7 , FALSE},
  { "[West \"", 7 , FALSE},
  { "[North \"", 8 , FALSE},
  { "[East \"", 7 , FALSE},
  { "[South \"", 8 , FALSE},
  { "[Board \"", 8 , FALSE},
  { "[Dealer \"", 9, FALSE },
  { "[Vulnerable \"", 13, FALSE },
  { "[Deal \"", 7, FALSE },
  { "[Scoring \"", 10, FALSE },
  { "[Declarer \"", 11, FALSE },
  { "[Contract \"", 11, FALSE },
  { "[Result \"", 9, FALSE },
  { "{", 1, FALSE },
  { "[Auction \"", 10, FALSE },
  { "[Play \"", 7, FALSE },
  { NULL, 0, FALSE }
};

uint8_t figure (uint8_t col) {
  // switch (col) { case 'P': return 6;  case 'C': return 3; case 'K': return 4;  case 'T': return 5; default: return col; }
  return col;
}

/* Predicate functions */
_Bool is_N (uint8_t);
_Bool is_o (uint8_t);
_Bool is_r (uint8_t);
_Bool is_d (uint8_t);
_Bool is_E (uint8_t);
_Bool is_s (uint8_t);
_Bool is_S (uint8_t);
_Bool is_u (uint8_t);
_Bool is_O (uint8_t);
_Bool is_e (uint8_t);
_Bool is_t (uint8_t);
_Bool is_1 (uint8_t);
_Bool is_0 (uint8_t);
_Bool  ucl (uint8_t);
_Bool  lcl (uint8_t);
_Bool  acc (uint8_t);
_Bool digit (uint8_t);
_Bool letter (uint8_t);
_Bool card (uint8_t);
_Bool colour (uint8_t);
_Bool is_nul (uint8_t);
_Bool is_ (uint8_t);
_Bool lambda (uint8_t);

/* Finite state automaton for comment updates */
enum act { abuf, aout, asub, aconv, aexit, anone };
struct tr { uint8_t trans; _Bool (*pred)(uint8_t); enum act act; }
  fstate [] = {
  /* State 0 */
  /*  0 */  {  9, is_N,  abuf },
  /*  1 */  { 15, is_E, abuf },
  /*  2 */  { 19, is_S, abuf },
  /*  3 */  { 37, is_1, abuf },
  /*  4 */  { 23, is_O,  abuf },
  /*  5 */  { 39, card, abuf },
  /*  6 */  { 35, letter, aout },
  /*  7 */  { 43,  is_nul, aexit },
  /*  8 */  { 0,  lambda, aout },
  /* State 1 : N - */
  /*  9 */  { 11,  is_o, abuf },
  /* 10 */  { 0,  lambda, aout },
  /* State 2 : No - */
  /* 11 */  { 13, is_r,  abuf },
  /* 12 */  { 0,  lambda, aout },
  /* State 3 : Nor -*/
  /* 13 */  { 31, is_d, abuf },
  /* 14 */  { 0,  lambda, aout },
  /* State 4 : E - */
  /* 15*/  { 17, is_s,  abuf },
  /* 16 */  { 0,  lambda, aout },
  /* State 5 : Es - */
  /* 17 */  { 33, is_t,  abuf }, /* Est- */
  /* 18 */  { 0,  lambda, 31 },
  /* State 6 : S - */
  /* 19 */  { 21, is_u, abuf },
  /* 20 */  { 0,  lambda, aout },
  /* State 7 : Su - */
  /* 21 */  { 31, is_d,  abuf },
  /* 22 */  { 0,  lambda, aout },
  /* State 8 : O - */
  /* 23 */  { 25, is_u, abuf },
  /* 24 */  { 0,  lambda, aout },
  /* State 9 : Ou - */
  /* 25 */  { 27, is_e, abuf },
  /* 26 */  { 0,  lambda, aout },
  /* State 10 Oue - */
  /* 27 */  { 29, is_s, abuf },
  /* 28 */  { 0,  lambda, aout },
  /* State 11  : Oues - */
  /* 29 */  { 31, is_t, abuf },
  /* 30 */  { 0,  lambda, aout },
  /* State 12 Final NSO */
  /* 31 */  { 35,  letter, aout },
  /* 32 */  { 0,  lambda, asub },
  /* State 13  : Est ? */
  /* 33 */  { 35,  is_, aout },
  /* 34 */  { 31,  lambda, anone },
  /* State 14 : Ident */
  /* 35 */  { 35, letter, aout },
  /* 36 */  { 0, lambda, aout },
  /* State 15 : 1-  */
  /* 37 */  { 39, is_0, abuf },
  /* 38 */  {  0, lambda, aout },
  /* State 16 Card or 10 */
  /* 39 */  { 41, colour, abuf },
  /* 40 */  { 35, lambda, aout },
  /* State 17 : Card-Colour ? */
  /* 41 */  { 35, letter, aout },
  /* 42 */  { 0,  lambda, aconv }
  /* 43 */
};
#define EXIT_STATE (sizeof (fstate )/sizeof (struct tr))

/* Temporary variables for PBN board encoding */
uint8_t dealer, vulnerability, declarer;
comment_t comment, leadcard, result;
uint8_t contract;
uint8_t site [MAX_SITE];
uint8_t site1 [] = "Bridge Club de Grenoble";
uint8_t site2 [] = "26 rue du Col. Dumont";
uint8_t site3 [] = "38000 Grenoble";
uint8_t school [] = "Ecole de bridge du BCG";
time_t  date = 0;
struct tm sdate;
uint8_t cdate  [10] = { '\0' };
uint8_t event [MAX_EVENT] = {'\0'};
uint16_t debug = 0;
uint8_t imode = 0;
enum pmode { text, excel };
uint16_t count = 1;
/* Next board to be created */
uint8_t tag ='a';
uint8_t bn = 1;

uint8_t ttf[] = "ARDVX98765432";
uint8_t tta[] = "AKQJT98765432";
uint16_t mask [] = {
  0x8000, 0x4000, 0x2000, 0x1000, 0x0800, 0x0400, 0x0200,
  0x0100, 0x0080, 0x0040, 0x0020, 0x0010, 0x0008
};

/* Table used for DGE decoding and command word decomposition
 *  0-12 : DGE card value
 * 13-16 : DGE suit value (13=Spade, 14=Heart...)
 *    17 : Word separator : space, tab, null or linefeed
 *   254 : character legal in a file name 
 *   255 : character illegal in a file name
 */
uint8_t ascii [] = {
  /*0   1,  2,  3,  4,  5,  6,  7,  8,  9*/
  17,255,255, 14, 15, 16, 13,255,255, 17, /* 00*/
  17,255,255,255,255,255,255,255,255,255, /* 10*/
  255,255,255,255,255,255,255,255,255,255,/* 20*/
  255,255, 17,254,255,254,254,255,255,255,/* 30*/
  255,255,255,254,254,254,254,255,254,254,/* 40*/
  12, 11, 10,  9,  8,  7,  6,  5,254,254, /* 50*/
  255,254,255,254,255,  0,254,254,  2,254,/* 60*/
  254,254,254,254,  3,  1,254,254,254,254,/* 70*/
  254,  2,  1,254,  4,254,  3,254,  4,254,/* 80*/
  254,254,255,255,254,254,255,254,254,254,/* 90*/
  254,254,254,254,254,254,254,254,254,254,/*100*/
  254,254,254,254,254,254,254,254,254,254,/*110*/
  254,254,254,255,255,255,255,255         /*120*/
};

enum orientation { north, east, south, west };
/* Suit code for DGE */
uint8_t SC[] = { 0x6, 0x3, 0x4, 0x5 };
/* Masks to decode or encode the 2)bit position for a couple card-sui */
uint8_t mask1 [4] = { 0b00111111, 0b11001111, 0b11110011, 0b11111100 };
uint8_t mask0 [4] = { 0b11000000, 0b00110000, 0b00001100, 0b00000011 };
/* French orientation - N=0, E=1, S=2, O=3 */
uint8_t *orient [4] = { "Nord -", "Est  -", "Sud  -", "Ouest-" };
uint8_t *orientf [4] = { "Nord", "Est", "Sud", "Ouest" };
uint8_t *oriente [4] = { "North", "East", "South", "West" };
/* English and French orientation initials */
uint8_t OE [4] = { 'N', 'E', 'S', 'W' };
uint8_t OF [4] = { 'N', 'E', 'S', 'O' };
/* English and French Suit initials */
uint8_t SE[4] = { 'S', 'H', 'D', 'C' };
uint8_t SF[4] = { 'P', 'C', 'K', 'T' };
/* English and French Bidding Declarations */
uint8_t *EBD [6] = { "Pass", "AP", "*", "X", "XX", NULL };
uint8_t *FBD [6] = { "Passe", "TP", "FIN", "X", "XX", NULL };
/* Table to determine who is the declarer :
 *  x is the denominations 0(P), (1)C, (2)K, (3)T, (4)SA
 *  y1 is the level 1 to 7
 *  y2 is the declarer 0(N), 1(E), 2(S), 3(O)
 */
uint8_t FBID [MAX_BID][2];
uint8_t biggest_bid = 0, biggest_contract = 0, biggest_declarer = 255;
uint8_t xdouble, xredouble;
 /* English and French Bidding Colours - letters or graphic charcaters */
uint8_t EBC [6] = {'C', 'D', 'H', 'S', 'N', 0 };
uint8_t FBC [6] = {'T', 'K', 'C', 'P', 'S', 0 };
uint8_t GBC [6] = { 0x5, 0x4, 0x3 , 0x6, 'S', 0 };
/* English and French Vulnerability terms */
uint8_t V[4] = { 'P', 'N', 'E', 'T' };
uint8_t *VE[4] = { "None", "NS", "EW", "All" };
uint8_t *VF[4] = { "Personne", "NS", "EO", "Tous" };
/* Pretty print table */
#define MAXZ 75
uint8_t z[19][MAXZ];
/* Suit starting coordinates in "z" */
uint16_t X[] = { 4, 9, 14, 9 };
uint16_t Y[] = { 15, 30, 15, 2 };
/* Strings s to be written in z at line x, column y) */
struct s { uint8_t *s; uint16_t x, y; };
struct s tch [] = {
  { "Donne - ", 2, 0 },
  { "Donneur:", 2, 28 }, { "Vul:", 2, 40 },
  { "Contrat:", 2, 47 }, { "Entame:  ", 2, 64 },
  { "Resultat:   ", 3, 28 }, { "Commentaires", 3, 42 },
  { "P-", 4, 13 }, { "P-", 9, 24 }, { "P-", 14, 13 }, { "P-", 9, 0 },
  { "C-", 5, 13 }, { "C-", 10, 24 }, { "C-", 15, 13 }, { "C-", 10, 0 },
  { "K-", 6, 13 }, { "K-", 11, 24}, { "K-", 16, 13 }, { "K-", 11, 0 },
  { "T-", 7, 13 }, { "T-", 12, 24 }, { "T-", 17, 13 }, { "T-", 12, 0 },
  { "N",  9, 16 }, { "E",  10, 17 }, { "S",  11, 16 }, { "O", 10, 15 }
};

/* Auction encoding: level [1-7], denomination T=0, K=1, C=2, P=3, SA=4
 * Bid = level * 5 + denomination : [5-39] unnamed
 * Declaration : Pass=0, AP=1, * /FIN=2, X=3, XX=4 [0-4]
 * Error ; 255
 */
enum _bval { b_pass=0, b_all=1, b_star=2, b_double=3, b_redouble=4, b_bid=5, b_error=255 };

struct _engb {
  uint8_t *eng_bid; uint8_t *fre_bid; uint8_t bvalue; } valid_bid [] = {
  "", "", b_pass,
  "-", "-", b_pass,
  "P", "P", b_pass,
  "PASS", "PASSE", b_pass,
  "AP", "TP", b_all,
  "*", "FIN", b_star,
  "*", "F", b_star,
  "X", "X", b_double,
  "XX", "XX", b_redouble,
  NULL, NULL, b_error
};

enum envkw {
  k_site1, k_site2, k_site3,
  k_school, k_event, k_date,
  k_tag, k_board_number, k_dealer, k_vulnerability,
  k_contract, k_declarer,  k_leadcard, k_result,
  k_bid11, k_bid12, k_bid13, k_bid14,
  k_bid21, k_bid22, k_bid23, k_bid24,
  k_bid31, k_bid32, k_bid33, k_bid34,
  k_bid41, k_bid42, k_bid43, k_bid44,
  k_norths, k_northh, k_northd, k_northc,
  k_easts, k_easth, k_eastd, k_eastc,
  k_souths, k_southh, k_southd, k_southc,
  k_wests, k_westh, k_westd, k_westc,
  k_com1, k_com2, k_com3, k_com4, k_com5,
  k_com6, k_com7, k_com8, k_com9,  k_com10,
  k_com11, k_com12, k_com13, k_end
};

struct _pbn_kw {
  uint8_t * kw;  uint8_t cx, cy; int8_t sz;
} BAT [] = {
  { "SITE1", 0, 0, 0 }, { "SITE2", 0, 25, 0 },
  { "SITE3", 0, 59, 0 }, { "SCHOOL", 1, 0, 0 },
  { "EVENT", 1, 25, 0 }, { "DATE", 1, 60, 0 },  
  { "TAG", 2, 9, 1 }, { "BOARD_NUMBER", 2, 10, 2 },
  { "DEALER", 2,36, 1 }, { "VULNERABILITY", 2, 44, 1 },
  { "CONTRACT", 2, 55, 5 }, { "DECLARER", 2, 55, 1 }, 
  { "LEADCARD", 2,71, 2 }, { "RESULT", 3,37, 2 },
  { "B11", 5, 29, 2 }, { "B12", 5, 32, 2 },
  { "B13", 5, 35, 2 }, { "B14", 5, 38, 2 },
  { "B21", 6, 29, 2 }, { "B22", 6, 32, 2 },
  { "B23", 6, 35, 2 }, { "B24", 6, 38, 2 },
  { "B31", 7, 29, 2 }, { "B32", 7, 32, 2 },
  { "B33", 7, 35, 2 }, { "B34", 7, 38, 2 },
  { "B41", 8, 29, 2 }, { "B42", 8, 32, 2 },
  { "B43", 8, 35, 2 }, { "B44", 8, 38, 2 },
  { "NORTHS", 4, 15, 13 }, { "NORTHH", 5, 15, 13 },
  { "NORTHD", 6, 15, 13 }, { "NORTHC", 7, 15, 13 },
  { "EASTS", 9, 27, 13 }, { "EASTH", 10, 27, 13 },
  { "EASTD", 11, 27, 13 }, { "EASTC", 12, 27, 13 },
  { "SOUTHS", 14, 15, 13 }, { "SOUTHH", 15, 15, 13 },
  { "SOUTHD", 16, 15, 13 }, { "SOUTHC", 17, 15, 13 },
  { "WESTS", 9, 2, 13 }, { "WESTH", 10, 2, 13 },
  { "WESTD", 11, 2, 13 }, { "WESTC", 12, 2, 13 },
  { "COM1", 4, 42, 32 }, { "COM2", 5, 42, 32 },
  { "COM3", 6, 42, 32 }, { "COM4", 7, 42, 32 },
  { "COM5", 8, 42, 32 }, { "COM6", 9, 42, 32 },
  { "COM7", 10, 42, 32}, { "COM8", 11, 42, 32 },
  { "COM9", 12, 42, 32 }, { "COM10", 13, 28, 46},
  { "COM11", 14, 28, 46 }, { "COM12", 15, 28, 46 },
  { "COM13", 16, 28, 46 },
  { NULL, 0, 0, 0 }
};

/* Input line for interactive mode */
uint8_t line [LINE_SIZE+1];
uint8_t *dupname;
/* Extension table */
uint8_t rpath [PATH_MAX];
uint8_t *T_ext [] = {
  ".PBN", ".pbn", ".DUP", ".dup",
  NULL
};


FILE *conf_file;
FILE *dupfile;
FILE *pbnfile;
FILE *csvfile;
FILE *deffile;

uint8_t *workdir;
uint8_t rootdir[4*LINE_SIZE] = ".";
uint8_t origdir[4*LINE_SIZE] = ".";
uint8_t targetdir[4*LINE_SIZE] = ".";
uint8_t file_name[4*LINE_SIZE] = ".";
uint8_t tmpname [4*LINE_SIZE];
uint8_t currentfile [4*LINE_SIZE] = {'\0'};
uint8_t deffilename [4*LINE_SIZE] = DEFAULTDEFNAME;
uint8_t csvfilename [4*LINE_SIZE] = {'\0'};
uint8_t modelfilename [4*LINE_SIZE] = DEFAULTMODELNAME;
#define MAXCONF 9
uint8_t *od;
uint8_t *odirs [MAXCONF] = { NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};
/* List for bridge filenames */
int64_t *ltem, *lfirst=NULL, *llast=NULL;
static uint32_t nbl = 0;
uint8_t wkd [LINE_SIZE];
uint8_t year[5], month[3], day [3];
/* Table to drive csv capture from an Excel complete model */
#define MAXL 12
#define NBOARD 8
#define MAXLINE 80
#define MAXLL (MAXL*NBOARD)
#define MAXC 12

/* Driver is the same for all board */
int8_t driver [MAXL][MAXC];

#define GEN(x) x
enum field {
   GEN(donneur), GEN(bnum), GEN(vul), GEN(contrat),
   GEN(declarant), GEN(entame), GEN(resultat),
   GEN(o1), GEN(n1), GEN(e1), GEN(s1),
   GEN(o2), GEN(n2), GEN(e2), GEN(s2),
   GEN(o3), GEN(n3), GEN(e3), GEN(s3),
   GEN(o4), GEN(n4), GEN(e4), GEN(s4),
   GEN(o5), GEN(n5), GEN(e5), GEN(s5),
   GEN(np), GEN(nc), GEN(nk), GEN(nt),
   GEN(ep), GEN(ec), GEN(ek), GEN(et),
   GEN(op), GEN(oc), GEN(ok), GEN(ot),
   GEN(sp), GEN(sc), GEN(sk), GEN(st),
   GEN(c1), GEN(c2), GEN(c3), GEN(c4),
   GEN(c5), GEN(c6), GEN(c7), GEN(c8),
   GEN(c9), GEN(c10), GEN(c11),
   GEN(c12), GEN(c13), GEN(niet)
};

#undef GEN
#define GEN(x) #x
uint8_t *field_name[] = {
   GEN(donneur), GEN(bnum), GEN(vul), GEN(contrat),
   GEN(declarant), GEN(entame), GEN(resultat),
   GEN(o1), GEN(n1), GEN(e1), GEN(s1),
   GEN(o2), GEN(n2), GEN(e2), GEN(s2),
   GEN(o3), GEN(n3), GEN(e3), GEN(s3),
   GEN(o4), GEN(n4), GEN(e4), GEN(s4),
   GEN(o5), GEN(n5), GEN(e5), GEN(s5),
   GEN(np), GEN(nc), GEN(nk), GEN(nt),
   GEN(ep), GEN(ec), GEN(ek), GEN(et),
   GEN(op), GEN(oc), GEN(ok), GEN(ot),
   GEN(sp), GEN(sc), GEN(sk), GEN(st),
   GEN(c1), GEN(c2), GEN(c3), GEN(c4),
   GEN(c5), GEN(c6), GEN(c7), GEN(c8),
   GEN(c9), GEN(c10), GEN(c11),
   GEN(c12), GEN(c13), GEN(niet)
};

enum mode_action {learning, capture};
enum mode_action a_mode = learning;

int ib =1;
unsigned char field_s [MAXLINE+1];

/* Syntax table */
enum dup { 
  adup, bdup, cdup, cddup, ddup, edup, fdup, hdup, hedup, indup, idup, info, kdup,
  ldup, mdup, ndup, odup, pdup, prdup, pxdup, qdup, rdup, sdup, tdup, udup,
  wdup, wkdup, xdup, xldup, xmdup, zdup,
  ido, iv, ide, ic, ir, ie, ied, icom, ia, it, 
  nodup
};

uint8_t *synth [] = {
  "assemble  : a destfile [dupfile]*",
  "b         : b [pbnfile]+",
  "clear     : c [cfgfile]",
  "chdir     : cd dir",
  "distribute: [d|de|dv]",
  "enter     : e [bn][dupfile]",
  "file      : [f|fo|ft|fc]",
  "headers   : h [board-list]",
  "help      : ? ",
  "insert    : i boardlist bp",
  "interact  : i [file]*",
  "info      : [ido|iv|ic|ide|ir|ie|ied|icom|ia|it] board-list",
  "configure : k [conffile]",
  "load      : l file [boardlist]*",
  "more      : m file",
  "new       : n [file]",
  "origin    : [o|o1|o2|o3|o4] [dir]",
  "pivote    : p file [bn]-[0|90|180|70]*",
  "print     : pr [destfile]",
  "print-xl  : px [destfile]",
  "quit      : q ",
  "remove    ; r file [boardlist]*",
  "show      : s file [boardlist]*",
  "target    : [t|t1|t2|t3|t4] [dir]",
  "unique    : u file [radical]",
  "write     : w [destfile]",
  "workdir   : [w1|w2|w3|w4|w5|w6|w7|w8|w9]",
  "exchange  : x file [bn XY-UV]*",
  "excel-l   : xl csvfile",
  "excel-m   : xm [modelfile]",
  "zoom      : z",
  "",
  "file      is [$n|dupfile|pbnfile|csvfile|modelfile]",
  "boardlist is [bn|bn-bm]"
  "$n        is nth pathname in search",  
  NULL
};

uint8_t *e_help [] = {
  "  Characters to fill a hand :",
  "  AaRrKkDdQVvJjXxTt9876543210 : fill/remove card symbol",
  "  <ret> : stop the current suit",
  "  - : erase previous card symbol (rightmost between [])",
  "  c : clear (erase all previous chars (betweeen [])",
  "  q : leave without keeping the board definition",
  "  p : move to previous suit",
  "  n : move to next suit",
  "  P : move to previous position",
  "  N : move to next position",
  "  ? : show this help",
  "  . : other characters are silently ignored",
  NULL
};

/* Program banner */
void print_banner (void) {
  LOGERR ("Utility program to operate DUP/PBN files - " VERSION(RADIX));
  LOGERR ("Author: christian.bert1@free.fr - February 2019");
  LOGERR ("This program is under GNU General Public License v2 or later");
  LOGERR ("? for help.");
}

uint8_t *catenate (uint8_t *d, uint8_t *s) {
  uint8_t c; uint8_t *p = tmpname;
  while ((c=*d) != 0) *p++ = *d++;
  *p++ = '\\';
  do { *p++ = c = *s++; } while (c != 0);
  return tmpname;
}
					   

/* Get absolute pathname */
void make_absolute ( uint8_t *rep) {

  uint8_t *p=rep, *t, c0, c1, d0, d1, c; int n;
  c0 = U(rep [0]); c1 = U(rep [1]);
  /* Skip drive prefix */
  if (c1 == ':' && c0 >= 'A' && c0 <= 'Z') p+=2;
  d0 = p [0]; d1 = p [1];
  /* Result dir in tmpname */
  if (d0 == '.')
    if (d1 == '.')  { /* relative to workdir's parent */
      strcpy (tmpname, workdir); n = strlen (workdir);
      while (workdir [n] != '\\') n--;
      t= tmpname + n + 1;
      do { *t++ = c = *p++; } while (c != 0);
    }
    else { /* relative to workdir */
      catenate (workdir, rep);
    }
  else
    if (d0 == '/' || d0 == '\\') { /* absolute */
      strcpy (tmpname, rep);
    }
    else { /* relative to workdir */
      catenate (workdir, rep);   
    }
  strcpy (rep, tmpname);
}

/* Predicate functions */

_Bool is_N (uint8_t c) { return c == 'N'; }

_Bool is_o (uint8_t c) { return c == 'o'; }

_Bool is_r (uint8_t c) { return c == 'r'; }

_Bool is_d (uint8_t c) { return c == 'd'; }

_Bool is_E (uint8_t c) { return c == 'E'; }

_Bool is_s (uint8_t c) { return c == 's'; }

_Bool is_S (uint8_t c) { return c == 'S'; }

_Bool is_u (uint8_t c) { return c == 'u'; }

_Bool is_O (uint8_t c) { return c == 'O'; }

_Bool is_e (uint8_t c) { return c == 'e'; }

_Bool is_t (uint8_t c) { return c == 't'; }

_Bool is_1 (uint8_t c) { return c == '1'; }

_Bool is_0 (uint8_t c) { return c == '0'; }

_Bool  ucl (uint8_t c) { return ((c>='A')&&(c<='Z'));}

_Bool  lcl (uint8_t c) { return ((c>='a')&&(c<='z'));}

_Bool  acc (uint8_t c) { return
    (c=='�')||(c=='�')||(c=='�')
    ||(c=='�')||(c=='�')||(c=='�')||(c=='�')
    ||(c=='�')||(c=='�')
    ||(c=='�')||(c=='�')
    ||(c=='�')||(c=='�')||(c=='�')
    ||(c=='�'); }

_Bool digit (uint8_t c) { return ((c>='0')&&(c<='9'));}

_Bool letter (uint8_t c) { return ucl(c)|lcl(c)|acc(c); }

_Bool card (uint8_t c) { return
    (c=='A')||(c=='R')||(c=='D')||(c=='V')||(c=='X')||((c>='1')&&(c<='9')); }

_Bool colour (uint8_t c) { return (c=='P')||(c=='C')||(c=='K')||(c=='T'); }

_Bool is_nul (uint8_t c) { return c == '\0'; }

_Bool is_ (uint8_t c) { return c == '-'; }

_Bool lambda (uint8_t c) { return TRUE; }

/* Raise string to uppercase and put a null character at the end */
void uppercase (uint8_t *b) {
  uint8_t c;

  while ((c = *b) != '\n' && c != '\0') 
    *b++ = U(c); 
  *b = '\0';
}

/* Read a line from standard input, null-terminated and uppercased */
uint8_t * nfgets (uint8_t *buf, uint16_t n, FILE *stream) {
  uint8_t sz, *r;
  if(stream == stdin) fflush (stream);
  r = fgets (buf, n, stream);
  sz = strlen (buf); if (buf [sz-1] == '\n') buf [sz-1] = '\0';
  return r;
}

/* Read a line from standard input, null-terminated and uppercased */
uint8_t * ufgets (uint8_t *buf, uint16_t n, FILE *stream) {

  uint8_t *r;
  r = nfgets (buf, n, stream);
  uppercase (buf);
  return r;
}

uint8_t yesno (uint8_t *question) {
  uint8_t c;
  
  forever {
    fprintf (stderr, "%s [y|n|q]? :", question); fflush (stderr);
    ufgets (buf, 2, stdin); c = buf[0];
    if (c == 'Q' || c == 'N' || c == 'Y') return c;
    else return 'O';
  }
}

void getdate (void) {
  time_t t;
  struct tm tm;

  t = time(NULL);
  tm = *localtime(&t);
  sprintf (year, "%4d", tm.tm_year + 1900);
  sprintf (month, "%02d", tm.tm_mon + 1);
  sprintf (day, "%02d",  tm.tm_mday);
}				  

uint16_t allocate_board (void) {
  /* Allocate a board into item global variable */
  item =(struct board *) malloc (BOARD_SIZE);  
  if (item == NULL) return 0;
  if (first == NULL) first = item;
  if (last != NULL) last->next = item;
  last = item; item->next = NULL;
  return 1;
}

uint8_t *substitute (uint8_t *buf, uint8_t *subs [], uint8_t a) {
  uint8_t i = 0;
  for (i=0; i<4; i++)
    if (strcmp (buf, subs [i]) == 0) return subs [(i+a)%4];
  return NULL;
}

uint8_t *pmov (uint8_t *pn, uint8_t *p) {
  uint8_t c;
  if (p == NULL) return NULL;
  while (*p != '\0') { c = *p++; *pn++ = c; }
  return pn;
}

void change_comment (uint8_t pn, uint8_t po, uint8_t *subs[], uint8_t rot) {
  uint8_t buf [6];
  uint8_t sz, nsz, t, c, nc, cc, d, e, *pb, *p;

  /* pn is the destination, pb is the intermediary buffer, po is the origin */
  pb = buf;
  /* Finite state automaton starts at t= 0 with first po character */
  t = 0; c = *po++;
  *pn = *pb = '\0';
  do {
    if (c == '\0') break;
    if ((fstate [t].pred (c))) {
      switch (fstate [t].act) {
      case asub:
	p = substitute (buf, orientf, rot);
	if (debug) LOG ("%d : %s <> %s, %s ->\n", c, buf, p, p);
	pn = pmov (pn, p); (*(pb = buf)) = '\0'; 
	/* Note: current character c will be re-examined */
	break;
      case aout:
	if (debug) LOG ("%d : %s%c ->\n", c, buf, c);
	pn = pmov (pn, buf); (*(pb = buf)) = '\0';
	*pn++ = c;
	c = *po++;
	break;
      case abuf:
	if (debug) LOG ("%d : %c -> buf\n", c, c);
	*pb++ = c; *(pb)= '\0';
	c = *po++;
	break;
      case aconv:
	/* Colour character */
	cc = *(pb - 1); nc = (*(pb-1)) = figure (cc); 
	if (debug) LOG ("%d : %c <> %c, %s%c ->\n", c, cc, nc, buf, c);
	pn = pmov (pn, buf); (*(pb = buf)) = '\0';
	*pn++ = c;
	c = *po++;
	break;
      case aexit:
	if (debug) LOG ("%d -> and exit\n",c);
	*pn++ = c;
	break;
      case anone:
	break;
      }
      t = fstate [t].trans;      
    }
    else
      t++;
  } while (t != EXIT_STATE);
  *pn = '\0';
  DEBUG ("Comment update:%s\n -> %s\n", po, pn);
}

void update_comment (struct board *item, uint8_t rot) {
  pkom_t pold, pnew;

  /* Walk the comment list */
  pold = item->kfirst;
  while (pold != NULL) {   
    if ((pold->kstring[0] != 0)
      {
	/* We assume a 10% increase for updated comment */
	sz = pold->ksize;
	nsz = (uint16_t) 0.1 * sz;
	/* Allocation pkom_t + string and initialization */
	pnew = (pkom_t) malloc (sz + 1 + offsetof (comment_t, kchars));
	pnew->ksize  = 0;
	pnew->kstring = (uint8_t *)(pnew + offsetof (comment_t, kchars));
	pnew->kstring [0] = '\0';
	pnew->knext = NULL;
	po = pold->kstring;
	pn = pnew->kstring;
	change_comment (po, pn, orientf, rot);
	/* NB: pold is not deallocated before being overwritten */
	pold->kstring = pn:
	pold->ksize= strlen (pn);
      }
    pold = pold->knext;
  }
}

/* Remove and free all entries of previous search operation */
void delete_list (void) {
  int64_t *next;

  ltem = lfirst;
  while (ltem != NULL) {
    next = (int64_t *) *ltem;
    if (debug) LOGERR ("Freeing 0x%8xd\n", ltem);
    free (ltem);
    ltem = next;
    nbl =  0;
  }
  lfirst = llast = NULL;
}

/* Search for nth name in lfirst list */
uint8_t *find_name (uint32_t num) {
  uint32_t n = 1;

  ltem = lfirst;
  while (ltem != NULL) {
    if (n == num) return ((uint8_t *)(ltem+1));
    ltem = (uint64_t *)*ltem; n++;
  }
  return NULL;
}

/* Write 2-bit position in byte board[card] in bit n, n+1 depending on suit */
void set (uint8_t *board, uint16_t card, uint16_t suit, uint16_t position) {
  uint8_t byte, m1;

  byte = board [card]; m1 = mask1 [suit]; 
  board [card] = (byte & m1) | (position << (2*(3-suit)));
}

/* Read the 2-bit position from bits n, n+1 of byte board[card] depending on suit */
uint16_t get (uint8_t *board, uint16_t card, uint16_t suit){
  uint8_t byte, m0, position;

  byte = board [card]; m0 = mask0 [suit];
  position = (byte & m0) >> (2*(3-suit));
  return position;
}

/* Initialize hand with 0xFF  */
void init_hand (uint8_t a []) { 
  uint16_t i;  
  /* Initialize all position values with 0x3 (West) for BRI complementary hand */
  /* Byte 0: AP, AC, AK, AT - 4 times 0b11,  Byte 1: RP, ... */
  for (i=0; i<BOARD_ELEMENT; i++) a [i] = 0xFF;
}

/* Populate z table |19, 85] with fixed strings */
void build_frame (void) {
  uint16_t x, y, i;
  uint16_t l, c;
  uint8_t *p;
  
  for (x=0; x<19; x++) { for (y=0; y<MAXZ-1; y++) z[x][y]=' ';  }
  for (i=0; i<sizeof(tch)/sizeof(struct s); i++) {
    x = tch[i].x; y = tch[i].y; p = tch[i].s; while (*p != '\0') z[x][y++] = *p++;
  }
}

/* Build sequensor from input buffer  */
uint8_t build_sequensor (_Bool seq [MAXBOARD], uint8_t *board_list) {
  uint8_t c, error = 0, *p, min, max;

  /* Board i is selected if seq[i-1] or if board_list is NULL  */
  for (min=0; min<128; min++) seq [min] = (board_list == NULL);
  if (board_list==NULL) return 0;

  /* Board list is a sequence of board numbers or intervals separated by a separator */
  p = board_list; c=*p++;
  while (c != '\n' && c != '\0' && ! error) {
    min = max = 0;
    while (isadigit (c)) { min = min*10 + (c-0x30); c=*p++; }
    if (c == '-') {
      c = *p++; while (isadigit (c)) { max = max*10 + (c-0x30); c=*p++; }
    } else max = min;
    if (min>127 || max>127 || min<1 || max<1) error = 1;
    if (min>0) while (min<=max) { seq [min-1] = TRUE; min++; }
    if (isaseparator (c)) c = *p++; 
    else if (! isaterminator (c)) error = 1;
  }
  if (error) LOGERR ("Incorrect board list : %s", board_list);
  return error;
}

/* Fetch next number from buffer p */
uint32_t getnum (uint8_t *p) {
  uint16_t v = 0, c;
  c = *p; if (c<'0' || c>'9') return 255;
  while (((*p)<= 0x39)&&((*p)>=0x30)) v=v*10 + *(p++)-0x30; 
  return v;
}

/* Extract extension from filename */
uint8_t* get_extension (uint8_t *name) {
  uint16_t s;
  uint8_t c, *p;

  s =strlen (name); p =name+s;
  do { c = *--p; } while (c != '.' && p > name);
  if (c == '.') return p; else return NULL; 
}

/* Extract radical from filename */
uint8_t* get_radical (uint8_t *name) {
  static uint8_t targ [LINE_SIZE];
  uint8_t c, *p = targ, *s = name;

  while ((c = *s) != '.' && c != '\0' && p < targ+LINE_SIZE) *p++ = *s++;
  *p = '\0';
  return targ;
}

/* Decode character card c */
uint16_t getcard (uint8_t c) {
  uint16_t i;
  /* As=0, Roi=1, ..., 2=12 */
  i = ascii [c];
  return (i < 13)? i : 0xFF;
}

/* Decode next character as a suit name */
uint16_t getsuit(uint8_t s) {
  uint16_t i;
  /* Pique=0, Coeur=1, Carreau=2, Trefle= 4 */
  for (i=0; i<4; i++) if (s==SF[i]) return i;
  return 0xFF;
}

enum language { french, english };
/* Decrypt contract byte to string */
void decrypt_contract (uint8_t c, uint8_t *s, enum language l) {
  uint8_t last, r;

  /* s as long as 6 bytes to account 7SAXX\0 */
  last = 2; r = c;
  if (c > 100) r = c - 100; 
  if (c > 200) r = c - 200; 
  if (r < 5 || r > 39) { s [0] = s [1] = '?'; }
  else {
    s [0] = 0x30 + r/5; /* level */
    s [1] = (l==french)? FBC[r%5] : EBC [r%5]; /* denomination */
    if ((c%5 == 4)) { s [2] = (l==french)? 'A' : 'T'; last = 3; }
  }
  if (c > 100) s [last++] = 'X';
  if (c > 200) s [last++] = 'X';
  s [last] = '\0';
}

/* Decrypt bidding byte to string */
void decrypt_bidding (uint8_t c, uint8_t *s, enum language l) {

  uint8_t decl;
  
  /* s as long as 5 bytes to account "Pass" */
  if (c > 39) { s [0] = s [1] = '?'; s[2] = '\0'; }
  else {
    if (c<5) strcpy (s, (l==english)? EBD [c]: FBD [c]);
    else {	 
      s [0] = 0x30 + c/5;
      s [1] = (l==french)? FBC [c%5] : EBC [c%5];
      /* Add second letter if no trump */
      if (c%5 == 4) { s [2] = (l==french)? 'A' : 'T'; s [3] = '\0';}
      else  s[2] = '\0';
    }
  }
}

/* Decode a board designation : [0-9]+
 * bn = 0 if incorrect number 
 */
uint8_t *get_board_number (uint8_t *p, uint16_t *bn) {
  uint16_t v, c;
  
  c = *p; v = 0;
  while (c <= 0x39 && c>=0x30) {
    v =v*10 + c-0x30;
    c = *(++p);
  }
  *bn = v;
  return p;
}

/* Build an argv list from character buffer 'buf', return number of words */
uint16_t setargs (uint8_t *buf, uint16_t lim, uint8_t *wd []) {
  uint8_t *p = buf, *plim = p + lim, c; 
  uint16_t wc = 0, i;
  forever {  
    c = *p;
    while (ascii [c] == 17 && p < plim && c != '\n') c = *++p;
    if (p < plim && c != '\n') wd [wc] = p; else break; 
    while ( ascii [c] != 17 && c != '\n' && p < plim) c = *++p;
    wc++; *p = '\0';
    if (p == plim || c == '\n') break;
    c = *++p;
  }
  wd [wc] = NULL;
  if (debug) for (i=0; i<wc; i++) LOGERR ("wd [%d] = %s", i, wd [i]);
  return wc;
}

/* Decode hands along BRI or DGE algorithm (a), 3 for BRI and 4 for DGE */
/* p points to the board record region to decode, limit gives the upper bound index */
void hand_decode (enum algo a, uint8_t *p, uint16_t limit, uint8_t *hand) {
  
  uint16_t x;
  uint16_t v, suit, card, position, h;
  uint8_t c;
  uint16_t size = algo_size[a];
  uint16_t inc = (a == BRI) ? 2 : 1;
  suit = 0;
  for (x = 0; x < limit; x += inc) {
    position = (x / size); h = 4 * position;
    switch (a) {
    case BRI:
      v = (p[x]-0x30)*10+(p[x+1]-0x30);
      card = (v-1)%13;  suit = (v-1)/13;
      if (v>52) LOGERR ("BRI: Invalid code at index %x", x);
      else set (hand, card, suit, position);
      break;
    case DGE:
      card = ascii [p[x]];
      if (card < 13) set (hand, card, suit, position);
      else if (card>12 && card<17) suit = card - 13; 
      else LOGERR ("DGE: Invalid code at index %x", x);
      break;
    default: 
      LOGERR ("Algorithm non supported %d", a);
      return;
    } 
  }
}

uint16_t board_decode (uint8_t *b) {
  uint16_t i;

  /* Decode 3 hands for BRI */
  hand_decode (BRI, b, 3*BRI_HAND_SIZE, bri);
  /* Decode 4 hands for DGE */
  hand_decode (DGE, b + BRI_RECORD_SIZE, 4*DGE_HAND_SIZE, dge);
  /* Ctrl is a static buffer */
  for (i=0; i<10; i++) ctrl[i] = b [BRI_RECORD_SIZE+DGE_RECORD_SIZE+i];
  return 0;
}

void encode_dup_board (uint8_t *r, uint8_t *b, uint16_t count) {
  uint16_t i, v, card, suit, position, i_bri, i_dge;
  uint8_t LAST[6];
  /* Encode 3 hands for BRI an DGE*/
  if (debug) for (i=0; i<DUP_RECORD_SIZE; i++) r [i] = 0xFF;
  i_bri =0; i_dge = BRI_RECORD_SIZE;
  for (position=0; position <4; position++) {  
    for (suit=0; suit<4; suit++) {
      r [i_dge++] = SC[suit];
      for (card=0; card<13; card++) {
	if ( get (b, card, suit) == position) {
	  v = 1+card+suit*13;
	  if (position<3) { r [i_bri++] = 0x30+v/10; r [i_bri++] = 0x30+v%10; }
	  r [i_dge++] = tta [card];
	}
      }
    }
  }
  for (i=0; i<7; i++) r [CONTROL_AREA+i] =  ctrl[i];
  sprintf (LAST, "%d", count); 
  i=0; 
  while ((i<3) && (LAST[i] !='\0')) { r [CONTROL_AREA+7+i] = LAST [i]; i++; }
  while (i<3) r [CONTROL_AREA+7+(i++)] = ' ';
}

void dump_board (enum algo a, uint8_t *board, uint8_t tag, uint16_t bn) {
  uint16_t m;
  uint16_t h, suit, card, i;

  LOGERR ("Encodage %s - Donne %c%02d:", algo_name[a], tag, bn);
  /* Dump encoding */
  printf ("Board address 0x%08x : ", board); 
  for (i=0; i<13; i++) printf ("0x%02x ", board[i]); printf ("\n");

  for (h=0; h<4; h++) {
    printf ("%s", orient [h]);
    for (suit=0; suit<4; suit++) {
      printf ("%c-", SF[suit]);
      for (card=0; card<BOARD_ELEMENT; card++) if (get (board, card, suit) == h) printf("%c ", ttf[card]);
    }
    printf("\n");
  }
  fflush (stdout);
}

void pz (uint8_t *from, uint8_t *to, uint8_t max) {
  uint8_t i = 0;
  while (i<max && (from[i] != '\0')) { to [i]= from [i]; i++; }
}

/* Print the auctions in a table with 4 calls per line */
void dump_bidding (struct board *b, uint8_t x, uint8_t y) {
  uint8_t i, d, ca, xa, ya, ib, c, call [6], sz, pass = 0;
  uint8_t m [5] =  { 1, 4, 7, 10, 13 };

  d = b->starter;
  /* Headers */
  for (i=0; i<5; i++) z [x][y+m[i]-1] = '|';
  for (i=0; i<4; i++) z [x][y+m[i]] = OF [(d+i)%4];
  /* Bidding calls; 4 per line, maximum 4 lines */
  ib = 0;
  do {
    xa = x + 1 + ib / 4; ya = y + m [ib % 4] - 1; 
    if (ib % 4 == 0) for (i=0; i<5; i++) z [xa][y+m[i]-1] = '|';
    c = b->bidding [ib]; if (c == 0) pass++; else pass=0;
    decrypt_bidding (c, call, french);
    switch (ca = call [0]) {
    case 'P':  z [xa] [ya+1] = '-'; break;
    case 'F': case 'T': z [xa] [ya+1] = 0x7F; break;
    case '1'...'7': z [xa] [ya+1] = ca; z [xa] [ya+2] = FBC [c%5]; break;
    default:
      z [xa] [ya+1] = ca;
      if (call [1] != '\0') z [xa] [ya+2] = call [1];
    }
    if (pass == 3 && ib != 2) c = 2;
    ib++;
   } while (c != 1 && c != 2 && ib < 16);
}

/* Responsive print : */
#define INC ((pm == text)? 32 :42)

/* Build z matrix from board *b */
void build_z (struct board *b, enum pmode pm) {
  uint16_t m, x, y, y0, ikx, xx, yy, sz;
  _Bool eoc;
  uint16_t h, k, suit, i, card;
  uint8_t c, j, r, lc, bid [6], inc, *p, *ppz, *plimit;
  pkom_t po;
  uint8_t maxline = INC;
  /* MAXZ is the maximum characters on an horizontal line */
  
  /* Populate fixed parts */
  build_frame ();
      
  for (k=k_site1; k<k_end; k++) {
    xx= BAT [k].cx; yy = BAT [k].cy; sz = BAT [k].sz;
    switch (k) {
      /* Site, School, Event and date */
    case k_site1:
       BAT [k].sz = strlen (site1);
      break;
    case k_site2:
      BAT [k].sz = strlen (site2);
      break;
    case k_site3:
       BAT [k].sz = strlen (site3);
      break;
    case k_school:
      BAT [k].sz = strlen (school);
      break;
    case k_event:
       BAT [k].sz = strlen (event);
      break;
    case k_date:
       BAT [k_date].sz = strlen (cdate);
      break;
      /* Tag and number */
    case k_tag:
      z[2][9] = b->tag;
      break;
    case k_board_number:
      j =yy;
      if (b->number>9) { j++; z[xx][yy] = 0x30+(b->number/10); }
      z[xx][j] = 0x30+(b->number%10);
      break;
      /* Dealer */
    case k_dealer:
      if (b->dealer != 255) z[xx][yy] = OF [b->dealer];
      break;
      /* Vulnerability */
    case k_vulnerability:
      if (b->vulnerability != 255) z[xx][yy] = V [b->vulnerability];
      break;
      /* Contract */
    case k_contract:
      decrypt_contract (b->contract, bid, french);
      BAT [k_contract].sz = strlen (bid);
      i=0; while ((c = bid [i]) != '\0') z [xx][yy + i++] = c;
      z[xx][yy+i]='/';
      if (b->declarer != 255) z [xx][yy+i+1] =  OF [b->declarer];
      BAT [k_declarer].cy = yy+i+1;
      break;
      /* Declarer */  
    case k_declarer:
      break;
      /* Leading card */
    case k_leadcard:
      if ((lc = b->leadcard) != 255) {
	z[xx][yy] = ttf [lc %13]; z[xx][yy+1] = SF [lc / 13];
      }
       break;
      /* Result in number of tricks */
    case k_result:
      j = yy;
      if ((r = b->result) != 255) {
	if (r > 9) z[xx][j++] = '1';
	z[xx][j] = '0' + r % 10;
      }
      break;
        /* bidding sequence */
    case k_bid11:
      if (b->bidding [0] != 255) dump_bidding (b, 4, 28);
      break;
    case k_bid12 ... k_bid44 :
      break;    
      /* 4 Hands of 4 colours */
    case k_norths ... k_westc:
      /* Find back orientation and suit */
      j = k - k_norths; h = j /4; suit = j % 4;
      y = yy;
      for (card=0; card<BOARD_ELEMENT; card++)
	if (h == get (b->bb, card, suit)) z [xx][y++] = ttf[card];
      BAT [k].sz = y-yy;
      break;
      /* Comments */
    case k_com1 ... k_com12:
      /* Comment line number */
      cnb = k - k_com1;
      po= b->kfirst; while ((cnb-- != 0 ) && po != NULL)) po = po->next;
      p = po->kstring ;  ppz = z [xx] + yy; plimit = ppz + sz - 2;
      /* Copy as much as possible, inserting .. to show the line is truncated nnd no /n */
      while (*p != '\0' && ppz < plimit) *ppz++ = *p++;
      if (ppz >= plimit) { *ppz++ = '.';  *ppz++ = '.'; }
      break;
    }
  }
}

/* Print one deal to xlname excel file using prname bat) */
uint8_t pretty_print_bat (
   struct board *b,
   FILE* stream,
   uint8_t *xlname,
   uint8_t *xlmodel,
   uint8_t sheet
)
{
  enum envkw k;
  uint8_t xx, yy, sz, r, i;
 
  /* 
   * VBA open macro executes when its excel file opens
   * PBN_EXCELFILE and PBN_SHEET must be passed to VBA Workbook open macro
   * PBN_LAST is passed only on last board to merge them into "all" sheet
   * All other PBN_ variables are setting cell values
   * This macro :
   * - creates a new sheet copy of the first sheet "model"
   * - rename it with the integer value of PBN_SHEET
   * - selects it
   * - populates it with values of other PBN_variables
   * if PBN_LAST is "YES" creates a last sheet caled "All" and copies the
   * cntents of sheet 1 to LAST
   * suppress the first sheet ("model") 
   * - saves and exits only if PBN_EXCELFILE is set
   * VBA macro does not do anything if PBN_EXCELFILE is not set
   */
   if (! debug) fprintf (stream, "echo off\n");
  /* Produce a new sheet and populate it */
  fprintf (stream, "set PBN_EXCELFILE=%s\n", xlname);
  fprintf (stream, "set PBN_SHEET=%d\n", sheet);
  if (b == last) fprintf (stream, "set PBN_LAST=YES\n");
  build_z (b, excel); 
  k = k_site1;
  while (BAT[k].kw != NULL) {
    xx= BAT [k].cx; yy = BAT [k].cy; sz = BAT [k].sz;
    fprintf (stream, "set PBN_%s=", BAT[k].kw);
    i=0; while (i<sz) {
      fprintf (stream, "%c", z [xx][yy+i]);
      i++;
    }
    fprintf (stream, "\n");
    k++;
  }
  /* Line to call Excel under setting of previous env variables */
  fprintf (stream,
    "C:\\\"Program Files\"\\\"Microsoft Office 15\"\\root\\office15\\excel.exe %s\n", xlname);
}

/* Print deal to stream  (text) */
void pretty_print_board (struct board *b, FILE *stream) {
  uint8_t x, y, c;
  
  /* Print to text stream */
  build_z (b, text);
  for (x=2; x<18; x++) {
    y=0; while (((c= z[x][y++]) != 0) && (y<MAXZ-1)) putc (c, stream);
    putc ('\n', stream);
  }
  putc ('\n', stream);
  putc ('\n', stream);
  fflush (stream);  
}

/* Remove 'item' from the linked list, requires 'prec' as preceding element */
struct board * remove_board (struct board *item, struct board *prec) {
  struct board * next_item;
  uint8_t *pcom;

  next_item =  item->next;
  if (prec != NULL) prec->next = next_item;
  if (item == first) first = item->next;
  if (item == last) last = prec;
  free (item);
  /* return the following element */
  return next_item;  
}

uint8_t check_board (uint8_t nb [4], struct board *item) {
  uint8_t c, s, p;

  /* Initialize array result */
  for (p=0; p<4; p++) nb [p] = 0;
  /* Check that the four hands of a board have 13 cards */
  for (s=0; s<4; s++)
    for (c=0; c<13; c++)
      nb [get (item->bb, c, s)]++;
  for (p=0; p<4; p++) if (nb [p] != 13) return 0;
  return 1;
}

/* Init FBID table and biggest call */
void init_bidding_history (void) {
  uint8_t i;
  
  for (i=0; i<MAX_BID; i++) FBID [i][0]= FBID [i][1]= 255; /* undef */
  biggest_bid = biggest_contract = 0;
  biggest_declarer = 255;
  xdouble = xredouble = 0;
}

/* Compute dealer N=0, E=1, S=2, O=3 from board number 1, 2... */
uint8_t compute_dealer (uint8_t n) { return (n - 1) % 4; }

/* Compute vulnerability P=0, N=1, E=2, T=3 from board number 1, 2... */
uint8_t compute_vulnerability (uint8_t n) { return ((n-1)%4 + (n-1)/4) %4; }

void init_item () {
  uint8_t i;
  enum pbn k;
  
  /* All is set to default */
  DEBUG ("Board #%d parameter initialization", bn);
  b.tag = tag;
  b.number = bn;
  b.dealer = compute_dealer (bn);
  b.vulnerability = compute_vulnerability (bn);
  b.declarer = 0;
  b.contract = 255;
  b.starter = 1;
  b.leadcard = 0;
  b.result = 7;
  /* All cards in West's */
  init_hand (b.bb);
  /* All comments empty */
  b.kfirst = b.klast = NULL;
  b.bidding [0] = 255;
  init_bidding_history ();
  for (k=0; k<_none; k++) _PBN [k].init = FALSE;
}

/* Board (b) is committed, i.e. a new item is allocated
 * and board b is copied to item to the exception of 'next' field.
 */
uint8_t commit_item (void) {

  if (allocate_board () == 0) ERROR (0, "Memory allocation problem");
  b.next = item->next;
  *item = b;
  return 0;
}

/* Load dup file <origdir>/<name> into memory boards */
uint16_t load_dup_file (uint8_t *pathname, uint8_t *board_list, uint8_t tag) {
  uint16_t r, i, rc = 0, cb = 0;
  uint8_t c1, c2, ck, *ext, nb[4];

  init_item ();
  dupfile = fopen (pathname, "rb");
  if (dupfile == NULL) ERROR (0, "File %s does not exist", pathname);
  if (build_sequensor (seq, board_list) == 1) return 0;
  while ((r = fread (board_record, DUP_RECORD_SIZE, 1, dupfile)) == 1) {
    if (r != 1) ERROR (0, "Read error", pathname);
    if (seq [rc]) { /* Board has been selected */
      init_item (); commit_item ();
      dge = item->bb; init_hand (dge);
      init_hand (bri); 
      board_decode (board_record);
      if (debug) { dump_board (BRI, bri, tag, bn); dump_board (DGE, dge, tag, bn); }
      /* Compare results */
      for (i=0; i<BOARD_ELEMENT; i++)
	if (bri[i]!=dge[i]) ERROR (0, "Board encodings #%d differ", rc);
      /* Show and check the new board */
      pretty_print_board (item, stdout);
      ck = check_board (nb, item);
      LOGERR ("Board %d is %scomplete [%d][%d][%d][%d]",rc+1,((ck==1)?"":"in"), nb[0],nb[1],nb[2],nb[3]);
      bn++; cb++;
    } else if (debug) LOGERR ("Skipping board %d", rc+1);
    /* Next board */
    rc++;
  }
  fclose (dupfile);
  if (cb != 0) LOG ("Loading %d boards from %s under tag '%c'\n", cb, pathname, tag);
  return cb;
}

void decode_string (uint8_t *f, uint8_t *t, uint8_t max) {
  uint8_t i, c;

  i = 0; 
  while ((c = *f) != '\"' && c != '\n' && max)
    t [i++] = *f++;
}

uint8_t decode_vulnerability (uint8_t *p) {

  if (strncmp (p, "None", 4) == 0) return 0;
  if (strncmp (p, "NS", 2) == 0) return 1;
  if (strncmp (p, "EW", 2) == 0) return 2;
  if (strncmp (p, "All", 3) == 0) return 3;
  if (strncmp (p, "Both", 4) == 0) return 3;
  return 255;
}

uint8_t decode_short_vulnerability (uint8_t *p) {
  uint8_t c;
  /* from French */  
    switch (*p) {
    case 'P': return 0;
    case 'E': return 1; 
    case 'S': return 2; 
    case 'O': return 3; 
    default:  return 0;
  }
}

uint8_t decode_declarer (uint8_t *p) {
  uint8_t c;
  /* From English */
    switch (*p) {
    case 'N': return 0;
    case 'E': return 1; 
    case 'S': return 2; 
    case 'W': return 3; 
    default:  return 255;
  }
}

uint8_t decode_dealer (uint8_t *p) {
uint8_t c;
 
  switch (*p) {
  case 'N': return 0;
  case 'E': return 1;
  case 'S': return 2;
  case 'W': return 3;
  default:  return 255;
  }
}

  /* Encode a string which represents a valid bid in language l */
enum _bval encode_bid (uint8_t *s, enum language l) {
  uint8_t c, d, level, id, sz;
  struct _engb *p;
  uint8_t *BC = ((l==english)? EBC : FBC);
  
  /* Find fix auction in valid_bid array  */
  p = valid_bid;
  while (p->eng_bid != NULL)
    if (strcmp (s, ((l==english)? p->eng_bid : p->fre_bid)) == 0) return p->bvalue;
    else p++;
  /* <level><denom> */
  sz = strlen (s);
  if (sz<2 || sz>4) return b_error;
  c= s [0]; d = s [1];
  if (c < '1' || c > '7') return b_error;
  level = c - 0x30;
  for (id=0; id<5; id++)
    if (d == BC [id]) return 5 * level +id;
  return b_error;
 }

void decode_auction (uint8_t *p, FILE *pnb) {
  uint8_t ia = 0, pass = 0, call [6], ic, c, n, starter;
  _Bool eoa = FALSE, eof = FALSE;
  if ((b.starter = decode_dealer (p))== 255) return;
  do {
    if (! (eof = (NULL == fgets (buf, 255, pnb)))) {
      /* 4 calls to decode */
      p = buf;
      do {
	while ((c = *p) == ' ') p++;
	ic = 0;
	while ((c = *p) != ' ' && c != '\t' && c != '\n' && ic < 4) {
	  call [ic] = c; ic++; p++;
	}
	call [ic] = '\0';
	b.bidding [ia] = n = encode_bid (call, english);
	if (n == 0) pass++; else pass = 0;
	/* Stop if eof or call is AP/* or 3 Pass or + 16 calls */
	eoa = (n == 255) || eof || ( pass==3 && ic != 3)  || n==1 || n==2 || ia==15;
      } while (((++ia % 4) != 0) && ! eoa);
    }
  } while (! eoa);
}

uint8_t encrypt_contract (uint8_t *p, enum language l) {
  uint8_t c, level=0, denom=0, risk=0, r;

  /* As encoded in the PBN format in english */
  c = *p;
  if (c < '1' || c > '7')
    return 255;
  else
    level = c - 0x30;
  c = *(++p);
  for (denom=0; denom<5; denom++) {
    if (l==english && c == EBC [denom]) break;
    if (l==french && c == FBC [denom]) break;
  }
  if (denom == 5) return 255; /* not found */

  if ( *(p+1) == 'X') risk += 100;
  if ( *(p+2) == 'X') risk += 100;

  return 5*level+denom + risk;
}

uint16_t decode_pbn_board (uint8_t *p) {
  uint8_t c, suit, pos;

  /* Decode dealer prefix */
  c =(p[7]); 
  if (c=='E') pos = 1; else if (c=='S') pos = 2; else if (c=='W') pos = 3; else if (c=='N') pos = 0;
  else  ERROR (0, "Invalid dealer in deal string \'%c\' in pbn file", c);
  suit = 0; p += 9; c = *p;
  while (c != '\"') {
    if (ascii [c] < 13) set (b.bb, ascii[c], suit, pos);
    else if (c == '.') suit = (suit+1)%4;
    else if (c == ' ') { pos = (pos+1)%4; suit = 0; }
    else ERROR (0, "Invalid characters in pbn format");
    c = *++p;
  }
  return 1;
}

void decode_leadcard (uint8_t *p, FILE *file) {
  uint8_t c, s, e, d;

  e = b.leadcard  = 255;
  d = b.declarer;
  switch (p[7]) { /* leader */
  case 'N': e = 0; break;
  case 'E': e = 1; break;
  case 'S': e = 2; break;
  case 'W': e = 3; break;
  default:  return;
  }
  if ((d != 255) && ((d+1)%4 != e))
    LOGERR ("Inconsistent leader (%c) and declarer (%c)",
	    (e==255)? '?': OE[e], (d==255)? '?': OE[d]);
  if (ufgets (p, 255, file) != NULL) {
    switch (p[0]) { /* suit */
    case 'S': s = 0; break;
    case 'H': s = 1; break;
    case 'D': s = 2; break;
    case 'C': s = 3; break;
    default:  return;
    }
    c = ascii [p [1]]; /* card */
    if (s <4 && c < 13) {
      b.leadcard = 13 * s + c;
    }
    /* else b.leadcard = 255 */
  }
}

/* Encode a deal along PBN format starting by pos == dealer */
/* "dealer: SSS.HHHH.DDDD.CC SSSS.HHH.DDDD.CC SSS.HHH.DDDD.CCC SSS.HHh.D.CCCCCCC" */
uint8_t * encode_pbn_hands (uint8_t bb[13], uint8_t dealer) {
  uint16_t pos, suit, card, i;
  uint8_t *b = line;

  for (i = 0; i<4; i++) {
    pos= (dealer + i) % 4;
    for (suit=0; suit<4; suit++) {
      for (card=0; card<BOARD_ELEMENT; card++) {
	if (pos == get (bb, card, suit)) *b++ = tta [card];
      }
      if (suit<3) *b++ = '.';
    }
    if (i<3) *b++ = ' ';
  }
  *b++ = '\0';
  return line;
}

void dump_comment (struct board *b, FILE *f) {
  int8_t ic, clast;
  kcom_t po =  b->first;
  * Find the last non empty comment */
  ic = 0; clast = -1;
  while (po ! =NULL) { if (po->string[0] != 0) clast = ic; po = po->knext; ic++; }
  if (clast != -1) {
    fprintf (f, "{");
    for (ic=0, po = b->kfirst; ic<=clast; ic++) {
      fprintf (f, "%s\n", po->string);
      po = po->knext;
    }
    fprintf (f, "}\n"); 
  }
}

void encode_pbn_board (FILE *destfile, struct board *item, uint8_t cb) {
  uint8_t b, v, d, dc, dd, *vr, rd [5], ld, lc, ia, scont[6], sbid[5];
  _Bool eoa = FALSE;

  /* PBN 2.1 requires 15 mandatory tags */
  fprintf (destfile, "%% PBN 2.1\n");
  /* 1*/ fprintf (destfile, "[Event \"%s\"]\n", event);
  /* 2*/ fprintf (destfile, "[Site \"Bridge Club de Grenoble\"]\n");
  /* getdate (); */
  /* 3*/ fprintf (destfile, "[Date \"%s\"]\n", cdate);
  /* 4*/ fprintf (destfile, "[Board \"%d\"]\n", cb);
  /* 5*/ fprintf (destfile, "[West \"?\"]\n");
  /* 6*/ fprintf (destfile, "[North \"?\"]\n");
  /* 7*/ fprintf (destfile, "[East \"?\"]\n");
  /* 8*/ fprintf (destfile, "[South \"?\"]\n");
  d = (item->dealer != 255)? OE [item->dealer]: '?';
  /* 9*/ fprintf (destfile, "[Dealer \"%c\"]\n", d);
  if (item->vulnerability == 255) LOGERR ("Vulnerability must be defined");
  /*10*/ fprintf (destfile, "[Vulnerable \"%s\"]\n", VE[item->vulnerability]);
  /*11*/ fprintf (destfile, "[Deal \"%c:", 'N');
  /*  */ fprintf (destfile, "%s ", encode_pbn_hands (item->bb, 0));
  /*  */ fprintf (destfile, "\"]\n");
  /*12*/ // fprintf (destfile, "[Scoring \"?\"]\n");
  dd = (item->declarer != 255)? OE [item->declarer]: '?';
  /*13*/ fprintf (destfile, "[Declarer \"%c\"]\n", dd);
  decrypt_contract (item->contract, scont, english);
  /*14*/ fprintf (destfile, "[Contract \"%s\"]\n", scont);
  sprintf (rd, "%d", item->result);
  vr = (item->result != 255)? (uint8_t *)rd: (uint8_t *)"?";
 /*15*/ fprintf (destfile, "[Result \"%s\"]\n", vr);
  /*16*/ 
    update_comment (item, 0); /* Only colours */
    dump_comment (item, destfile);
  /*17*/ if (item->bidding[0] != 255) {
    fprintf (destfile, "[Auction \"%c\"]\n", OE [item->starter]);
    ia = 0;
    do {
      b = item->bidding [ia];
      decrypt_bidding (b, sbid, english);
      fprintf (destfile, "%s", sbid);
      fprintf (destfile, "%c", (ia%4 == 3)? '\n': ' ');
      ia++;
    } while (b != 1 && b != 2 && ia < 16);
    /* Terminate line except when already done */
    if (ia%4 != 0) fprintf (destfile, "\n");   
  }
  /*18*/ if ((lc = item->leadcard) != 255) {
    ld = (item->declarer + 1) % 4;
    fprintf (destfile, "[Play \"%c\"]\n", OE [ld]);
    fprintf (destfile, "%c%c - - -\n", SE [lc / 13], tta [lc % 13]);
  }
  if (item->next != NULL) fprintf (destfile, "\n");
}

void double_buffer (uint8_t **pa: uint16_t *ps) {
  uint8_t *a = *pa;
  uint16_t s = *ps;
  if (2*d > 32768) ERROR(-1, "Overflow of omment buffer");
  *ps = 2 * s;
  (*pa) = (uint8_t *) malloc (2*s);
  memmove (*pa, pa, s);
}

 void keep_comment (struct board *b, uint8_t *buf, uint8_t  ix, FILE * file) {
  uint8_t *p, ch, cc;
  _Bool eoc = FALSE;
  uint16_t nc=0, ib, dsize = 32;
  pkom_t pnew, prec  = NULL;
  uint8_t *comment = (uint8_t *) malloc (dsize);
  p = buf + ix;
  do {
    cc = *p;
    /* } is end of comment (eoc) */
    eoc = (cc == '}');
    if (! eoc) { if (nc == (dsize-1)) double_comment (&comment, &dsize); comment [nc++] = cc; }
    if ((cc == '\n')|| eoc) {
      /* New line *: allocate a kom entry and fetch a new buffer */
      comment [nc-1] = '\0';
       /* Allocation pkom_t + string and initialization */
      pnew = (pkom_t) malloc (nc + 1 + offsetof (comment_t, kchars));
      pnew->ksize  = nc;
      pnew->kstring = (uint8_t *)(pnew + offsetof (comment_t, kchars));
      pnew->kstring [0] = '\0';
      strcpy (pnew->kstring, comment);
      pnew->knext = NULL;
      b->klast = pnew;
      if (prec == NULL) b->kfirst = pnew; else prec->knext = pnew;
      prec = pnew;
      eoc = (fgets (buf, 255, file) == NULL); p = buf;
    } else p++;
  } while (! eoc);
}

/* Check and compute a date DD/MM/YY */
time_t check_date (uint8_t *p) {
  uint8_t sz = strlen (p);
  uint8_t d, m, y;

  if (sz != 8) return 0;
  if (p[2] != '/' || p[5] != '/') return 0;
  if (p[0]<0x30 || p[0]> 0x39) return 0;
  if (p[1]<0x30 || p[1]> 0x39) return 0;
  if (p[3]<0x30 || p[3]> 0x39) return 0;
  if (p[4]<0x30 || p[4]> 0x39) return 0;
  if (p[6]<0x30 || p[6]> 0x39) return 0;
  if (p[7]<0x30 || p[7]> 0x39) return 0;
  d = (p [0]-0x30)*10 + (p[1]-0x30); if (d > 31) return 0;
  m = (p [3]-0x30)*10 + (p[4]-0x30); if (m > 12) return 0;
  y = (p [6]-0x30)*10 + (p[7]-0x30);
  sdate.tm_mday = d; sdate.tm_mon = m-1; sdate.tm_year = y+100;
  return mktime (&sdate);
}

/* Read event and date from console */
uint8_t read_event_date (void) {
  _Bool doit = FALSE;
  uint8_t sz;
  time_t d;

  /* Return values in event and date, struct tm sdate and cdate */
  doit = (event [0] == '\0');
  if (! doit) {
    fprintf (stderr, "Event = %s ", event);
    doit = (yesno ("Do you want to change it?") == 'Y');
  } 
  while (doit) {
    fprintf(stderr, "Enter event: "); fflush (stderr);
    nfgets (event, MAX_EVENT, stdin);
    doit = strlen (event) == 0;
    if (doit) return 'Q';
  }   
  doit = (date == 0);
  if (! doit) {
    fprintf (stderr, "Date = %s ", cdate); fflush (stderr);
    doit = (yesno ("Do you want to change it?") == 'Y');
  } 
  while (doit) {
    fprintf(stderr, "Enter date [JJ/MM/AA]? "); fflush (stderr);
    nfgets (cdate, 13, stdin);
    if (strlen (cdate) == 0) return 'Q';
    doit = ((d = check_date (cdate)) == 0);
    if (doit)
      LOGERR ("Invalid date; %s", cdate);
    else
      date = d;
  }
  return 0; 
}

/* Read card from console */
/* A card is the number 15*S + C with S in [0,3] and C in [0, 12] */
/* Spade is 0, Ace is 0 */
uint8_t read_card (uint8_t *name, uint8_t vdef) {
  uint8_t c,s,ec,es;

  if (vdef == 255) s = c = '?'; else { s = SF [vdef/13]; c = ttf [vdef % 13]; } 
  printf ("%s ([ARDVX98765432][PCKT]) [%c%c]?", name, c, s); fflush (stdout);
  forever {
    ufgets (line, 3, stdin);
    c = line[0]; s = line[1];
    if (c == '?') return 255;
    if (c == '\0') return vdef;
    if (c=='Q') return 254;
    if ((ec = getcard (c)) != 255 && (es =getsuit (s)) != 255)
      return 13 * es + ec;
    LOGERR ("Invalid %s: %c%c", name, c, s);
  }
}

/* Read number from console */
uint8_t read_number (uint8_t *name, uint8_t vdef) {
  uint8_t c,s, n, *rdef, snum [4];

  itoa (vdef, snum, 10);
  if (vdef == 255) rdef = "?"; else rdef = snum;

  printf ("%s [0-13]? (%s)", name, rdef); fflush (stdout);
  forever {
    ufgets (line, 3, stdin);
    c = line[0];
    if (c == '?') return 255;
    if (c == '\0') return vdef;
    if (c=='Q') return 254;
    if ((n = getnum (line)) <= 13) return n;
    LOGERR ("Invalid %s: %s %d", name, line, n);
  }
}

/* Check if a string represents a valid contract */
_Bool check_contract (uint8_t *s, enum language l) {
  uint8_t c, d, x, xx, sz;

  sz = strlen (s); 
  if (sz<2 || sz>4) return FALSE;
  c = s[0]; d = s [1];
  if ((c > '7' || c < '1')
    || ((l==french) && (d != 'T' && d != 'K' && d != 'C' && d != 'P' && d != 'S'))
    || ((l==english) && (d != 'C' && d != 'D' && d != 'H' && d != 'S' && d != 'N')))
    return FALSE; 
  if (sz>=3 && s[2] != 'X') return FALSE;
  if (sz>=4 && s[3] != 'X') return FALSE;
  return TRUE;
}


/* Read contract for board <tag><bn> from console in french */
uint16_t read_contract (struct board *it) {
  uint16_t kc, sz, nbc, cdef;
  uint8_t vdef[6], c, d, x, xx;

  /* Current value may be inconsistent */
  decrypt_contract (biggest_contract, vdef, french);
  if (check_contract (vdef, french)) it->contract = biggest_contract;
  else {
    decrypt_contract (it->contract, vdef, french);
    if (! check_contract (vdef, french)) {
      it->contract = 255;
      strcpy (vdef, "??");
    }
  }
  printf ("Contract [1-7][TKCPS][X|XX]? (%s)", vdef); fflush (stdout);
  forever {
    ufgets (line, 5, stdin);
    c = line [0];
    if (c == '\0') return 1;
    if (c == '?') return 255;
    if (c == 'Q') return 254;
    if (check_contract (line, french)) {
      it->contract = encrypt_contract (line, french); 
      return 0;
    }
  }
}

/* Read element for board <tag><bn> from console */
uint8_t read_elem (uint8_t *name, uint8_t *values, uint8_t vdef) {
  uint8_t i, sz, c, rdef;

  sz = strlen (values);
  rdef = values [vdef];
  printf ("%s ", name);
  for (i=0; i<sz; i++) printf ("|%c", values[i]);
  printf ("]? (%c)", rdef); fflush (stdout);
  forever {
    ufgets (line, 2, stdin);
    c = line[0];
    if (c == '\0') return vdef;
    if (c=='Q') return 254;
    for (i=0; i<sz; i++) if (c==values[i]) return i;
    LOGERR ("Invalid %s: %s", name, line);
  }
}

/* Mark the player who first bids a given denomination */
void mark_first_bid (uint8_t c, uint8_t b) {
  uint8_t level, denom;

  xdouble = (c == 4); xredouble = (c == 5);
  if (c < 5 || c > 39) return;
  /* Denomination and level clear the risks */
  xdouble = xredouble = 0;
  level = c / 5; denom = c % 5;
  if (FBID [denom][0] == 255)  /* first bid */
    FBID [denom][1] = b;
  /* Always update level */
  FBID [denom][0] = level;
  if (level >= biggest_bid) {
    biggest_bid = level;
    biggest_contract = 5*level+denom+100*xdouble+100*xredouble;
    biggest_declarer = FBID [denom][1];
  }
}

/* Read bidding for board <tag><bn> from console */
uint16_t read_bidding (struct board *it) {
  uint8_t c, b, ibx, sz, r, v, pass, i, j;
  _Bool eob = FALSE, eof = FALSE;

  /* Start with dealer's bid */
  if (it->dealer != 255)
    it->starter = it->dealer;
  else
    if ((r = read_elem ("Starter", "NESO", 255)) != 254)
      it->starter = r;
    else
      ERROR (-1, "Cannot fill bidding without a starter");
  /* Initialize FBID */
  init_bidding_history ();
  b = it->starter; ibx = 0; pass = 0;
  do {
     forever {
       /* [1-7][TKCPS]|[-|X|XX|*|F|<|>|Q] */
       printf ("%s's bid (#%d)? ", oriente[b], ibx+1);
      fflush (stdout);
      eof = (NULL == ufgets (line, 5, stdin));
      if (!eof) {
	c = line [0];
	if (c == 'Q') return 254;
	if (c == '>' && ibx < 16)  break;
	if (c == '<' && ibx > 0) { ibx -= 2; break; }
	  if ((v = encode_bid (line, french)) != b_error) {
	    it->bidding [ibx] = v;
	    mark_first_bid (v, b);
	    pass = (v==0)? pass+1 : 0;	
	    if (pass == 3 && ibx != 2) v = 2;
	    break;
	} else {
	  LOGERR ("Invalid bid: %s\n", line);
	  LOGERR ("declarations are [1-7][TKCPS]| or [-|X|XX|*|F|<|>|Q]");
	  continue;
	}
      }
     } /* forever */
     ibx++; b = (it->starter + ibx) % 4; /* Corresponding bidder */
  } while (! eof && (v!=1) && (v!=2) && ibx <16);
  if (biggest_contract != 0) {
      decrypt_contract (biggest_contract, line, french);  
      printf ("Auction tesrminated with Contract %s and Declarer is %s\n", line, oriente [biggest_declarer]);
      fflush (stdout);
  }
}

/* Notepad Edition of comment for board <tag><bn> from console */
void edit_comment (struct board *it) {
  uint16_t kc, sz, nbc;
  uint8_t *pcom, c, r, eob, leader = 0, suit, card, ic;
   int32_t fd;
  char buf [90];
  uint8_t  *tmpdir, *tmpfile;;
  FILE *sd;

  /* TMP env variable */
  tmpdir = getenv ("TMP");
  if (tmpdir == NULL) { fprintf (stderr, "No TMP resource\n"); exit(-1); }
  if (debug) fprintf (stderr, "%s\n", tmpdir);
  /* Build a unique tmp file name */
  sz = strlen (tmpdir);
  tmpfile = (char *) malloc (sz +12);
  strcpy (tmpfile, tmpdir); strcat (tmpfile, TMPNAME(RADIX));
  fd =  mkstemp (tmpfile);
  if (debug) fprintf (stderr, "%s\n", tmpfile);
  
  printf ("Board %c%d:\n", it->tag, it->number); fflush (stdout);
  /* Write previous comment */
  sd = fdopen (fd, "w");
  for (ic=0; ic<MAX_NB_COMMENT; ic++) fprintf (sd,"%s", it->kom [iv]);
  fclose (sd);
  /* Call Notepad  for editing */
  r = _spawnlp (P_WAIT, "notepad.exe", "notepad.exe", tmpfile, NULL);
  if (r != 0) fprintf (stderr, "Spawn error %d\n", r);
  /* Open and read new comment */
  sd = fopen (tmpfile, "r");
  ic =0; 
  while (fgets (line, LINE_SIZE, sd) != NULL && ic <MAX_NB_COMMENT)  {
    sz = strlen (line);
    if ((sz) < MAX_COM) strncpy (it->kom [ic++], line, MAX_COM);
  }
  fclose (sd);
  unlink (tmpfile);}

/* Read ALL for board <tag><bn> from console
 * dealer, vulnerability, contract, declarer, leadcard, result and comment
 */
uint16_t read_all (struct board *it) {
  uint8_t r, vdef, *sdef, snum [4];

  /* Event and date */
  if ((it->event [0] == '\0') || (cdate [0] == '\0'))
    read_event_date ();
  /* Dealer */
  vdef = compute_dealer (it->number);
  if ((r = read_elem ("Dealer", "NESO", vdef)) != 254) it->dealer = dealer = r;
  else return r;
  /* Vulnerability */
  vdef = compute_vulnerability (it->number);
  if ((r = read_elem ("Vulnerability", "PNET", vdef)) != 254) it->vulnerability = vulnerability = r;
  else return r;
  /* Bidding sequence */
  if ((r = read_bidding (it)) == 254) return 254;
  /* Contract */
  if ((r = read_contract (it)) == 254) return 254;
  /* Declarer */
  vdef = (biggest_declarer < 4)? biggest_declarer :(it->declarer <4)? it->declarer : 0;
    if ((r = read_elem ("Declarer", "NESO", vdef)) != 254) it->declarer = declarer = r;
  else return r;
  /* Leading card */
  vdef = (it->leadcard < 51)? it->leadcard: 0;
  if ((r = read_card ("Leading card", it->leadcard)) != 254) it->leadcard = leadcard = r;
  else return r;
  /* Result, i.e. number of tricks won by declarer */
  vdef = (it->result<14)? it->result: 0;
   if ((r = read_number ("Result", vdef)) != 254) it->result = result = r; 
  else return r;
  /* Comment */
  edit_comment (it);
}

_Bool empty_line (uint8_t *p) {
  uint8_t c;
  
  while ((c = *p++) != '\n') if (c != ' ' && c != '\t') return FALSE;
  return TRUE;
}

/* Load PBN file <origdir>/<name> into memory boards */
uint16_t load_pbn_file (uint8_t *pathname, uint8_t *board_list, uint8_t tag) {
  uint8_t buf [256], c, c1, c2, nl = 0;
  uint16_t r, cb=0, rc = 0, i, suit, card, pos;
  uint8_t nb[4], ck;
  enum pbn k = 0;
  _Bool eof, Doit;
  time_t d;
  uint8_t idate [12];

  pbnfile = fopen (pathname, "r");
  if (pbnfile==NULL) ERROR (0, "Bridge file %s does not exist", pathname);
  if (build_sequensor (seq, board_list) == 1) return 0;
  init_item ();
  do {
    eof = (NULL == fgets (buf, 255, pbnfile));
    if (debug) LOGERR (buf);
    /* A semi-empty line or an end-of-file terminates the on-going board definition */
    if (eof || empty_line (buf)) {
      if (seq [rc]) {
	if (_PBN [_deal].init) {
	  /*Commit, show and check the new board */
	  commit_item ();
	  pretty_print_board (item, stdout);
	  ck = check_board (nb, item);
	  LOGERR ("Board %d is %scomplete [%d][%d][%d][%d]",rc+1,((ck==1)?"":"in"), nb[0],nb[1],nb[2],nb[3]);
	  /* Initialize parameters for next board */
	  bn++; cb++;
	  init_item ();
	} else
	  LOGERR ("%dth Board is missing tag %s, cannot be committed!", (_PBN [_deal].init)?"":"[Deal]");
      }
      rc++; /* Records are separated by a semi-empty line */
      if (eof) break; /* or terminate on an eof ! */
      continue; 
    }
    /* Look up PBN key table */
    k = 0;
    Doit = (board_list == NULL);
    while ((0 != strncmp (buf, _PBN [k].key, _PBN [k].ix)) || _PBN [k].init)
      /* keyword different OR keyword equal but already initialized! */
      k++;
    switch (k) {
    case _event:
      if (Doit) {
	decode_string (buf+8, event, i<MAX_EVENT); 
	strcpy (b.event, event);
      }
      break;
    case _site:
      if (Doit) decode_string (buf+7, site, MAX_SITE);
      break;
    case _date:
      if (Doit) {
	strncpy (idate, buf+7, 8); idate [8] = '\0';
	d = check_date (idate);
	if (d != 0) { b.date = date = d; strcpy (cdate, idate); }
      }
      break;
    case _north: case _east: case _south:    case _west: case _board:
    case _scoring: break;
    case
      _dealer: b.starter = b.dealer = decode_dealer (buf+9);
      break;
    case _vulnerable:
      b.vulnerability = decode_vulnerability (buf+13);
      break;
    case _deal:
      if (seq [rc]) decode_pbn_board (buf);
      else LOGERR ("Skipping board %d", rc+1);
      break;
    case _contract:
      b.contract = encrypt_contract (buf+11, english);
      break;
    case _result:
      b.result = getnum (buf+9);
      break;
    case _declarer:
      b. declarer = decode_declarer (buf+11);
      break;
    case _comment : /* Comment */
      if (buf [0] == '{' && seq [rc])
        keep_comment (&b, buf, 1, pbnfile);
      break;
    case _auction:
      decode_auction (buf+10, pbnfile);
      break;
    case _play:
      decode_leadcard (buf, pbnfile);
      break;
    default: if (debug) LOGERR ("Unexpected keyword %s", buf); break;
    }
    if (k != _none) _PBN [k].init = TRUE;
  } while (NOT eof);
  fclose (pbnfile);
  if (cb != 0) LOG ("Loading %d boards from %s under tag '%c'\n", cb, pathname, tag);
  if (debug) LOGERR ("Number of records : %d", rc);
  
  fflush (stdin);
  return cb;
}

/* Load file <origdir>/<name> into memory boards */
uint16_t load_file (uint8_t *name, uint8_t *board_list, uint8_t tag) {
  uint8_t *ext, c1, c2, n, r;

  if (name[0] == '$') {
    n = getnum (name+1); name = find_name (n);
    if (name == NULL) ERROR (-2, "Short name $%d not found", n);
  }
  uppercase (name);
  c1 = name [0]; c2 = name [1];
  if ((c1=='C' && c2==':') || c1=='/' || c1=='\\') strcpy (tmpname, name);
  else { strcpy (tmpname, origdir); strcat (tmpname, "/"); strcat (tmpname, name);}
  ext = get_extension(name);
  if (ext != NULL && strcmp (ext, ".DUP") == 0) r = load_dup_file (tmpname, board_list, tag);
  else if (ext == NULL || strcmp (ext, ".PBN") == 0) load_pbn_file (tmpname, board_list, tag);
  else ERROR (-1, "unknown file extension %s", ext);
  strcpy (currentfile, tmpname);
  return 0;
}

/* Write all defined boards to file <targetdir>/<name>] in PBN or DGE format */
uint8_t store_file_type (enum algo a, uint8_t *pathname, uint8_t mode) {
  uint16_t r, i, cb = 0;
  FILE *destfile;
  struct stat buf;

  /* Check if File exists */
  if (stat (pathname, &buf) == 0) {
      LOGERR ("Overwrite %s ", pathname);
      if (yesno ("") != 'Y') return -1;
  }
  while (event [0] == '\0' || date == 0) {
    r = read_event_date ();
    if (r == 'Q') return -2;
  }
  LOGERR ("All boards to be written under event =%s date=%s", event, cdate);
  if(yesno ("Do you confirm ?") != 'Y') return -2;

  /* All boards are written with the same Event and Date */
  destfile = fopen (pathname, "w");
  if (destfile == NULL) ERROR (-2, "File %s cannot be created", pathname);
  item=first; while (item!=NULL) {
    switch (a) {
    case PBN :
      encode_pbn_board (destfile, item, ++cb);
      break;
    case DGE:
      encode_dup_board (board_record, item->bb, count);
      fwrite (board_record, DUP_RECORD_SIZE, 1, destfile);
      break;
    }
    item=item->next;
  }
  fclose (destfile);
  LOG ("Writing file %s\n", pathname);
  strcpy (currentfile, pathname);
}

/* Store file <targetdir>/<name> from memory boards */
uint16_t store_file (uint8_t *name, uint8_t mode) {
  uint8_t *ext;   uint8_t c1, c2, n;

  if (mode) return 0;
  if (name[0] == '$') {
    n = getnum (name+1); name = find_name (n);
    if (name == NULL) ERROR (-2, "Short name $%d not found", n);
  }  uppercase (name);
  c1 = name [0]; c2 = name [1];
  if ((c2 = ':' && c1 == 'C') || (c1 == '/' || c1 == '\\')) strcpy (tmpname, name);
  else { strcpy (tmpname, targetdir); strcat (tmpname, "/"); strcat (tmpname, name);}
  ext = get_extension(name);
  if (ext != NULL && strcmp (ext, ".PBN") == 0) return store_file_type (PBN, tmpname, mode);
  if (ext == NULL || strcmp (ext, ".DUP") == 0) return store_file_type (DGE, tmpname, mode);
  ERROR (-1, "unknown file extension %s", ext);
  return 0;
}

/* Commands available in interactive mode */

/* Assemble dup files: a [-d] destfile [dupfile]* */
uint16_t f_adup (uint8_t *prog, int argc, uint8_t *argv[]) {
  uint16_t i, n, r;
  uint8_t *destname;

  if (argc < 2) SYNTAX (adup);  
  destname = argv [0];
  r = store_file (destname, imode);
  return r;
}

/* Load PBN file : b pbnfiles*/
uint16_t f_bdup (uint8_t *prog, int wc, uint8_t *word []) {
  uint16_t i, n, r;

  /* Load file into memory boards */
  if (wc < 2) SYNTAX (bdup); 
  while (wc >= 2) { 
    r = load_pbn_file (word [1],word [2], tag); wc--; word++; 
    if (r!=0) { tag++; count += r; }
  }
  return r;
}

/* Clear all : c */
uint16_t f_cdup (uint8_t *prog, int argc, uint8_t *argv[]) {
  struct board *prec;
  
  item = first;  prec = NULL;
  while (item != NULL) item = remove_board (item, prec);
  delete_list ();
  tag = 'a'; bn = 1; 
  LOG ("Full reset\n");
  currentfile [0] = '\0';
  init_item ();
  event [0] = '\0'; date = 0; cdate [0] = '\0';
  return 0;
}
  
/* Change directory : cd <dir> */
uint16_t f_cddup (uint8_t *prog, int argc, uint8_t *argv[]) {
  return 0;
}

/* Load configuration file : k [congig.file]+
 * File <radix>.cfg contains up to 9 pathnames
 * If prog is NULL, default configuration
 */
uint16_t f_kdup (uint8_t *prog, int argc, uint8_t *argv []) {
  uint16_t i, r, s;
  uint8_t *cfgname;

  cfgname = (argc > 1)? argv [1] : (uint8_t *) CONFIG_FILE(RADIX);
  conf_file = fopen (cfgname, "r");
  if (conf_file == NULL) {
    cfgname = catenate (rootdir, CONFIG_FILE(RADIX));
    conf_file = fopen (cfgname, "r");
    if (conf_file == NULL) {
      if (prog != NULL || debug)
	ERROR (-1, "Cannot open configuration file %s", cfgname);
      LOG ("Default configuration file %s not found\n", cfgname);
      return -1;
    }
  }
  /* Could open workdir/conf_file or root_dir/conf_file */
  i = 0; 
  while (nfgets (wkd, 127, conf_file) != NULL && i<MAXCONF) {
    od = (uint8_t *) malloc (LINE_SIZE);
    if (od == NULL) ERROR (-7, "No dynamic memory left");
    odirs [i] = od; strcpy (od, wkd);
    i++;
    if (i>=MAXCONF && debug)
      LOG ("Maximum number of origin directories is MAXCONF\n");
  }
  fclose (conf_file);
  LOG ("Loading %s configuration file\n", cfgname);
  LOG ("Origin directories :\n");
  while (i<MAXCONF) odirs [i++] = NULL;
  for (i=0; i<MAXCONF && odirs [i] != NULL; i++)
    LOG ("%d. %s\n", i+1, odirs [i]);
  return 0;
}

/* Pivote the four hands of a box by a number of quarters (0 to 3) */
void shift_hands (uint8_t b [13], uint8_t shiftc) {
  uint8_t card, suit, pos;

  if (shiftc == 0) return;
  for (card=0; card<13; card++) for (suit=0; suit<4; suit++) { 
      pos = get (b, card, suit);
      pos = (pos + shiftc ) % 4;
      set (b, card, suit, pos);
    }
}

/* Change the orientation words in comment */
void shift_comment (struct board *it, uint8_t a, enum language l) {
}

#define SHIFT(x,a) ((x)!=255)?((x) = ((x) + (a)) % 4): 255

/* Pivote the hands, the comment, dealer, declarer and bidding starter */
void shift_all (struct board *it, uint8_t a) {
  shift_hands (it->bb, a);
  SHIFT (it->dealer, a); SHIFT (it->declarer, a); SHIFT (it->starter,a);
  shift_comment (it, a, french);
}

/*
 * Distribute current board set to predefined boxes
 * Syntax : [de|dv] [max]
 * Option 1 (de) Board is pivoted to get board dealer = box dealer 
 * Option 2 (dv) Board vulnerability is respected by allocating the first
 * available box and pivoting the board to match box's dealer
 * MAXBOARD is the maximum of boxes
 */
uint16_t f_ddup (uint8_t *prog, int argc, uint8_t *argv []) {
  uint8_t n, ne, pe, ve, vd, pd, i, x, nb, a, dd, ndd, c, st;
  uint8_t rep [2], *pc;
  int16_t ia;

  optbox = (strcmp (prog, "dv") != 0);
  if (debug) LOGERR ("Distribution on %d boxes, option %s",
		     MAXBOARD, (optbox)?"etuis":"vulnerabilite");
  for (i=0; i<MAXBOARD; i++) alloc [i] = TRUE;
  ne = 0; /* First box */
  item = first; nb = 0;
  while (item != NULL) {
    pd = item->dealer; vd = item->vulnerability;
    dd = item->declarer; st = item->starter;
    switch (optbox) {
    case TRUE:  /* Option box's dealer */
      ne++;
      if (ne==MAXBOARD) LOGERR ("Box limit reached (%d)", ne);
      else alloc [ne-1] = FALSE;
      break;
    case FALSE: /* Option box's vulnerability */
      ne=255;
      for (x=0; x<MAXBOARD/4; x++) {
	n = 1+ 4*x +((vd+3*x)%4);
	if (alloc [n-1]) { ne = n; alloc [ne-1] = FALSE; break;}
      }
      if (ne==255) LOGERR ("Allocation unfeasible on %d boxes", MAXBOARD);
    }
    /* pe: Box position , pd: Board position */
    ve = ((ne-1)%4 + (ne-1)/4) % 4;
    pe = (ne-1) % 4;
    if (debug) LOGERR ("Etui %d, Don. %c, vuln. %c", ne, OF[pe], V[ve]);
    ia = pe - pd;
    a =((ia)<0)? ia+4: ia;
    if (debug) LOGERR ("Shift de %d quarts de tour", a);
    shift_all (item, a);
    ndd = item->declarer;
    item->vulnerability = ve;
    if (ve != vd) LOG ("Board %c%d : box %d, Vulnerability changed to %c!\n",
		       item->tag, item->number, ne, V[ve]);
    if (pe != pd) LOG ("Board %c%d : box %d, Dealer %c changed to %c\n",
	               item->tag, item->number, ne, OF[pd], OF[pe]);
    if (ndd != dd) LOG ("Board %c%d : box %d, Declarer %c changed to %c\n",
			item->tag, item->number, ne, OF[dd], OF[ndd]);
    /* Contract, Leading card, result do not change */
    /* Bidding sequence is unchanged, the starter has been shifted */
    update_comment (item, ia);
    box_order [nb] = ne;
    pretty_print_board (item, stdout);
    if (debug && item != last) {
      if ((c = yesno ("Next")) != 'N' || c != 'Q') break;
    }
    item = item->next; nb++;
  }
  LOG ("Distribution of %d boards on boxes: \n", nb);
  for (i=0; i<nb; i++) LOG ("%d ", box_order [i]); LOG ("\n");
}

void remove_symbol (uint8_t *p, uint8_t c) {
  uint8_t x;
  
  while ((x=*p) != '\0' && (x!=c)) p++;
  while (*p++ != '\0') *(p-1) = *(p);
}

/* Build the symbol sequence for pos in suit, return the number of cards */
uint16_t build_suit (struct board *item, uint16_t pos, uint16_t suit, uint8_t *st) {
  uint8_t *p;
  uint16_t c, nc=0;
  
  for (c=0; c<13; c++) if (pos == get (item->bb, c, suit)) st[nc++] = tta [c];
  st[nc] = '\0';
  return nc;
}

/* 
 * Enter a board definition from console. Syntax: e [dupfile]
 * Characters to fill the hand are;
 *    ARKDQVJXT9876543210 - to add or remove a card symbol
 *    - to erase the previous character, rightmost between []
 *    c to clear, i.e. erase all chars betweeen []
 *    q to leave without keeping the board definition
 *    p to move to previous suit
 *    n to move to next suit
 *    P to move to previous position
 *    N to move to next position
 *    ? to show this help
 *    . all other characters are just ignored
 *    <ret> to stop the current suit capture
 */
uint16_t edit_board (struct board *item, uint8_t *prog, int wc, uint8_t *word []) {
  uint16_t r, card, suit, pos, i, j, k, l, nbc,  nbt;
  uint8_t c, cc, ck, color [81], st [14], s[14], nb[4];
  uint8_t x, h, *p;
  uint8_t smove, pmove, leave;

  forever {
    /* Loop on North, East and South - West is complemented */
    pos = 0; nbt = 0; leave = 0;
    while (pos < 3) {
      /* Loop on suits P, C, K and T */
      suit = 0; pmove = 0; nbt = 0;     
      while (suit<4) {
	for (i=0; i<41; i++) LOGERR ("");
	LOGERR ("====================");
	for (k=0; k<3; k++) { for (l=0; l<4; l++) {
	    build_suit (item, k, l, s);
	    LOGERR ("%s %c : %s", orient [k], SF[l], s);
	  }
	  if (k != 2) LOGERR ("--------------------");
	}
	LOGERR ("====================");
	nbc = build_suit (item, pos, suit, st);
	LOGERR ("%s %c : [%s]? ", orient [pos], SF[suit], st);
	/* Reading in canonical mode ...*/
	fgets (buf, 14, stdin); 
	/* Loop on characters from input buffer */
	i = 0; smove = 0;
	while ((c = buf [i]) != '\0' && c != '\n') {
	  switch (c) {
	  case '?': case 'h': /* Help */
	    for (h = 0; e_help[h] != NULL; h++) LOGERR("%s", e_help[h]); break;
	  case '-': /* Remove last symbol */
	    if (nbc>0) {
	      nbc--; cc = st [nbc];
	      set (item->bb, ascii[cc], suit, 3);
	    }
	    break;
	  case 'c': /* Clear all */
	    while (nbc > 0) {
	      nbc--; cc = st [nbc];
	      set (item->bb, ascii[cc], suit, 3);
	    }
	    break;
	  case 'q': leave = 1; break;
	  case 'n': smove++; suit = (suit<3)? suit+1: 0; break;
	  case 'p': smove++; suit = (suit>0)? suit-1: 3; break;
	  case 'N': pmove++; suit = 0; pos = (pos<3)? pos+1: 0; break;
	  case 'P': pmove++; suit = 0; pos = (pos>0)? pos-1: 2; break;
	    /* Insertion of English and French card symbols (except q)*/
	  case 'a': case 'r': case 'k': case 'd': case 'v': case 'x': case 't': case 'j':
	    c -= ('a'-'A');
	  case 'A': case 'R': case 'K': case 'D': case 'Q': case 'V': case 'J': case 'X': case 'T':
	  case '9': case '8': case '7': case '6': case '5': case '4': case '3': case '2':
	    if (pos == get (item->bb, ascii[c], suit)) {
	      if (nbc>0) { /* Symbol is removed */
		nbc--; remove_symbol (st, c);
		set (item->bb, ascii[c], suit, 3);
	      }
	    } else
	      if (nbc<13) { /* Symbol is added */
		st [nbc++] = c;
		set (item->bb, ascii[c], suit, pos);
	      }
	    break;
	    /* case '\0': case '\n': exit conditions */
	  default: /* ignore */
	    break;
	  }
	  st [nbc] = '\0';
	  if (smove || pmove || leave) break;
	  i++;
	} /* End of buf [i] loop */
	fflush (stdin);
	if (pmove || leave) break;
	if (smove) continue;
	suit++;
	nbt += nbc;
      } /* End of suit loop */
      if (leave) break;
      if (pmove) continue;
      /* Move to next pos only if hand is complete */
      if (nbt == 13) pos++;
      else { 
	/* Same position is restarted with current status */
	LOGERR ("%s hand has too ", orient [pos]);
	if (nbt>13) LOGERR ("many cards (%d), restarting for deletion", nbt);
	if (nbt<13) LOGERR ("few cards (%d), restarting for addition", nbt);
	suit = 0; nbt = 0;
      } 
    } /* End of pos loop */
      /* Show board */
    pretty_print_board (item, stdout);
    ck = check_board (nb, item);
    LOGERR ("Board is %scomplete [%d][%d][%d][%d]", ((ck==1)?"":"in"), nb[0],nb[1],nb[2],nb[3]);
    c = yesno ("Redo edition");
    if (c == 'Y') continue;
    if (c != 'Q') return 1;
    if (c == 'N') return 0;
  }
} 

/* Recognize a file by a bridge format known extension */
_Bool is_bridge_format (uint8_t *name) {
  uint8_t i, *ext, **pext;

  ext = get_extension (name);
  if (ext == NULL) return FALSE;
  pext = T_ext;
  while (*pext != NULL) {
    if (strcmp (ext, *pext) == 0) return TRUE;
    pext++;
  }
}

/* Command fc ; files in configuration directory */
uint16_t f_fcdup (uint8_t *prog, int wc, uint8_t *word []) {
  uint8_t *tdir, i, *ext;
  DIR *dir;
  struct dirent *ent;
  uint16_t entry;

  LOG ("Configured directories :\n");
  for (i=0; i<MAXCONF && odirs [i] != NULL; i++) LOG ("%d. %s\n", i+1, odirs [i]);
  return 0;
}

/* Add a full pathname to lfirst/llast linked list */
uint8_t add_list (uint8_t *path) {
  uint16_t sz;

  sz = strlen (path)+1;
/* Room for a pointer and a string */
  ltem = (int64_t *) malloc (sz+8);
    if (ltem == NULL) ERROR (-1, "Search: out of memory...");
  *ltem = (int64_t) NULL;
  strcpy ((uint8_t *)(ltem+1), path);
  if (lfirst == NULL) lfirst = ltem;
  if (llast != NULL) *llast = (int64_t)ltem;
  llast = ltem;
  ++nbl;
  if (debug) LOGERR ("Adding %dth name to list : %s", nbl, path);
  return 0;
}

/* Recursive search of bridge files in directory tdir */
void search (uint8_t *tdir) {
  uint8_t  i, *dname;
  DIR *dir;
  struct dirent *ent;
  struct stat s;

  if ((dir = opendir (tdir)) == NULL) { LOGERR ("Could not open %s", tdir); return; }
  if (chdir (tdir) == -1) { LOGERR ("Could not chdir to %s", tdir); return; }
  if (debug) LOGERR ( "\nEntering %s directory :", tdir);
  /* add all bridge files within directory */
  while ((ent = readdir (dir)) != NULL) {
    dname = ent->d_name;
    /* Skip . and .. */
    if ((strcmp (dname, ".")==0) || (strcmp (dname, "..")==0)) continue;
    stat (dname, &s);
    if (s.st_mode & S_IFDIR) { /* Sub-directory */
      search (dname);
    } else
      if ((s.st_mode & S_IFREG) && is_bridge_format (dname)) {
	/* Bridge file */
	_fullpath (rpath, dname, PATH_MAX);
	add_list (rpath);
	if (debug) LOGERR ("%s", rpath);
      }
  }
  if (debug) LOGERR ( "\nLeaving %s directory :", tdir);
  if (chdir ("..") == -1) LOGERR ("Could not chdir to ..");
  closedir (dir);
}

/* Search files from origin (fo), target (ft) or working directory (f) */
uint16_t f_fdup (uint8_t *prog, int wc, uint8_t *word []) {
  uint8_t *tdir, i, n, *ext, *sp;
  DIR *dir;
  struct dirent *ent;
  
  if (lfirst != NULL) {
    LOGERR ("Deleting previous search list");
    delete_list ();
  }
  /* f prints out files from working directory, fo from origin and ft from target directory */
  if ((strcmp (prog, "f") == 0)) tdir = workdir;
  if ((strcmp (prog, "fo") == 0)) tdir = origdir;
  if ((strcmp (prog, "ft") == 0)) tdir = targetdir;
  /* Search tdir */
  search (tdir);
  /* Print list */
  LOGERR ("Bridge files in %s (%d): ", tdir, nbl);
  n = 1; ltem = lfirst;
  while (ltem != NULL) {
    if (n>99) sp = ""; else if (n>9) sp =" "; else sp ="  ";
    LOGERR ("%s$%d: %s", sp, n++, (uint8_t *)(ltem+1));
    ltem = (uint64_t *)*ltem;
   }
}

/* Help : syntax summary ? */
uint16_t f_help (uint8_t *prog, int wc, uint8_t *word []){
  uint16_t i = 0;
  
  LOGERR ("Syntax :");
  while (synth[i] != NULL ) {
    LOGERR (" %s", synth [i]);
    i++;
  }
}

/* Generic - Used for factoring code of in, r, s, ido, iv, ic, ide, ie, ied, ir, ia, it */
/* Syntax : f_gdup dupfile [<n>|<n>-<m>]* */
/* Nota Bene: Two enum dup : s for syntax, e for execution */
uint16_t f_gdup (enum dup s, enum dup e, int argc, uint8_t *argv[]) {
  uint16_t i, n, r = 0, min, max, counted = 0, vdef;
  struct board *prec;
  uint8_t *p; 
   
  if (argc<2) SYNTAX (s);
  dupname = argv[0];
  while ((p = argv[1]) != NULL) {
    p = get_board_number (p, &min); if (min == 0)
      ERROR (-1, "Invalid board number interval");
    switch (*p++) {
    case '\0': max =min; break;
    case '-': case ',':
      p = get_board_number (p, &max); if (max == 0)
	ERROR (-5, "Invalid board number interval");
      break;
    default: SYNTAX (s);
    }
    /* Walk the linked-list up to the first item */
    item = first; prec = NULL;
    while (item!=NULL && item->number < min) 
      { prec = item; item=item->next; }
    /* Execute command on item up to the last one of the interval */
    while (item!=NULL && item->number <= max) {
      if (s == info) pretty_print_board (item, stdout); 
      switch (e) {
      case rdup:
	item = remove_board (item, prec); break;
      case sdup:
	break;
      case ido:
	vdef = (item->dealer < 4)? item->dealer :0;
	if ((r = read_elem ("Dealer", "NESO", vdef)) != 254)
	  item->starter = item->dealer = dealer = r;
	break;
      case ide:
	vdef = (item->declarer < 4)? item->declarer :0;
	if ((r = read_elem ("Declarer", "NESO", vdef)) != 254) item->declarer = declarer = r; break;
      case ic:
	r = read_contract (item); break;
      case iv:
	vdef = (item->vulnerability < 4)? item->vulnerability :0;
	if ((r = read_elem ("Vulnerability", "PNET", vdef)) != 254) item->vulnerability = vulnerability = r; break;
      case ir:
	vdef = (item->result< 14)? item->result: 0;
	if ((r = read_number ("Result", vdef)) != 254) item->result = result = r; break;
      case ie:
	vdef = (item->leadcard < 52)? item->leadcard: 0;
	if ((r = read_card ("Leading card", vdef)) != 254) item->leadcard = leadcard = r; break;
      case ied:
	read_event_date (); break;
      case icom:
	edit_comment (item); break;
      case ia:
	read_bidding (item); break;
      case it:
	r = read_all (item); break;
      }
      if (r == 254) break;
      if (e != rdup) {
	pretty_print_board (item, stdout);
	item=item->next;
      }
      counted++;
    }
    argc--; argv++;
  }
  return counted;
}

/* Headers: Syntax : h [bn[-bm] */
uint16_t f_hdup (uint8_t *prog, int argc, uint8_t *argv[]) {
  uint16_t i, nb;

  if (argc>2) SYNTAX (hdup);
  item = first; nb = 0;
  while (item!=NULL) {
    nb++;
   dealers [nb] = item->dealer;
    declarers [nb] = item->declarer;
    vulns [nb] = item->vulnerability;
    item = item->next;
  }
  LOG ("Summary %d records:\n", nb);
  LOG ("-"); for (i=0; i<nb; i++) LOG ("%03d ", i+1); LOG ("\n");
  LOG ("D"); for (i=0; i<nb; i++) LOG (" %c  ", OF [dealers [i+1]]); LOG ("\n");
  LOG ("V"); for (i=0; i<nb; i++) LOG (" %c  ", V [vulns[i+1]]); LOG ("\n");
  LOG ("C"); for (i=0; i<nb; i++) LOG (" %c  ", OF [declarers [i+1]]); LOG ("\n");
  return 0;
}

/* info syntax: [ ido | ide | ic | iv | ir | ie | ied | ia | icom ] [ <bn> - <bn>-<bm> ] */
uint16_t f_ifdup (uint8_t *prog, int argc, uint8_t *argv[]) {

       if (strncmp (prog, "ido", 3) == 0)  f_gdup (info, ido, argc, argv);
  else if (strncmp (prog, "ide", 3) == 0)  f_gdup (info, ide, argc, argv);
  else if (strncmp (prog, "icom", 4) == 0) f_gdup (info, icom, argc, argv);
  else if (strncmp (prog, "ic", 2) == 0)   f_gdup (info, ic, argc, argv);
  else if (strncmp (prog, "iv", 2) == 0)   f_gdup (info, iv, argc, argv);
  else if (strncmp (prog, "ir", 2) == 0)   f_gdup (info, ir, argc, argv);
  else if (strncmp (prog, "ied", 3) == 0)  f_gdup (info, ied, argc, argv);
  else if (strncmp (prog, "ie", 2) == 0)   f_gdup (info, ie, argc, argv);
  else if (strncmp (prog, "it", 2) == 0)   f_gdup (info, it, argc, argv);
  else if (strncmp (prog, "ia", 2) == 0)   f_gdup (info, ia, argc, argv);
  else
    ERROR (-1, "Unknown info subcommand %s", prog);
}

/* Insert syntax:  i [<bn>|<bn>-<bm>] <bp> */
uint16_t f_indup (uint8_t *prog, int argc, uint8_t *argv[]) {
  uint16_t min, max, where;
  uint8_t i, *p;
  struct board *itemi, *itemj, *preci, *precj, *prec;

  if (argc<3) SYNTAX (indup);
  if ((p = argv[1]) != NULL) {
    p = get_board_number (p, &min);
    if (min == 0) ERROR (-1, "Invalid board number interval");
    switch (*p++) {
    case '\0': max =min; break;
    case '-': case ',':
      p = get_board_number (p, &max); if (max == 0)
	ERROR (-5, "Invalid board number interval");
      break;
    default: SYNTAX (indup);
    }
  }
  if ((p = argv[2]) != NULL) {
    p = get_board_number  (p, &where);
    if (where == 0) SYNTAX (indup);
  }      
  /* Walk the linked-list up to the where item */
  item = first; prec = NULL; i=1;
  while (item !=NULL && i != where) { prec = item; item =item->next; i++; }
  if (item == NULL) ERROR (-6, "Insertion place does not exist");
  itemi = first; preci = NULL; i=1;
  while (itemi != NULL && i != min) { preci = itemi; itemi = itemi->next; i++; }
  if (itemi == NULL) ERROR (-6, "wrong indexes for insert");   
  itemj = first; precj = NULL; i=1;
  while (itemj !=NULL && i != max) { precj = itemj; itemj =itemj->next; i++; }
  if (itemj == NULL) ERROR (-6, "wrong indexes for insert");
  /* Update links */
  preci->next = itemj->next; if (itemj->next == NULL) last = preci;
  itemj->next = item; 
  if (prec == NULL) first = itemi; else prec->next = itemi;
    LOG ("Inserted board indexes %d-%d at %d\n", min, max, where);
  return 0;
}

/* Load file into memory boards ; l filename */
uint16_t f_ldup (uint8_t *prog, int wc, uint8_t *word []) {
  uint16_t i, n, r;

  if (wc < 2) SYNTAX (ldup); 
  r = load_file (word [1], word [2], tag);
  if (r!=0) { tag++; count += r; }
  return r;
}

/* More dup file records: m [-d] dupfile  */
uint16_t f_mdup (uint8_t *prog, int argc, uint8_t *argv[]) {
  uint8_t rep [2], r, c;

  item=first; 
  while (item!=NULL) {
    pretty_print_board (item, stdout);
    if (item != last) {
      if ((c = yesno ("Next")) == 'N' || c == 'Q') break;
    }
    item=item->next;
  }
  fflush (stdin);
  return 0;
}

/* Edit an existing board : e [-d] bn [dupfile] */
uint16_t f_edup (uint8_t *prog, int argc, uint8_t *argv[]) {
  uint16_t ibn, r;
  uint8_t *p;
  
  if (argc<2) SYNTAX (edup);
  if ((p = argv[1]) != NULL)
    p = get_board_number (p, &ibn); if (ibn == 0)
      ERROR (-1, "Invalid board number");
  item=first; 
  while (item!=NULL && item->number != ibn) item=item->next;
  if (item == NULL) ERROR (-1, "Board %d not found", ibn);
  /* Edit an existing board (ibn) */
  r= edit_board (item, prog, argc, argv);
  return r;
}

/* New board : n [-d] [dupfile] */
uint16_t f_ndup (uint8_t *prog, int argc, uint8_t *argv[]) {

  uint16_t itag, ibn, r, leave, nbreg=0, i;
  uint8_t c;
  if (argc>=2) SYNTAX (ndup);
  forever {
    /* Prepare temporary board */
    b.number = bn; b.tag = tag; 
    /* Get dealer, vulnerability and comment */
    if (read_all (&b)==254) return nbreg;
    /* All cards are given to West for automatic completion */
    for (i=0; i<13; i++) b.bb [i] = 0xFF;
    /* Edit the board */
    leave = edit_board (&b, prog, argc, argv);
    c = yesno ("Register board");
    if (c == 'Q') { leave = 1;  break; }
    if (c == 'Y') {
      /*  Allocate a new board */
      if (allocate_board () == 0) ERROR (-1, "Memory allocation problem");
      b.next =  NULL;
      memmove (item, &b, sizeof(b));
      LOGERR ("Allocating board %c%d", tag, bn);
      bn++; nbreg++;
    }
    if (leave) break;
    c = yesno ("Another board");
    if ( c == 'Q' || c == 'N') { leave = 1; break; }
  }
  if (leave) LOGERR ("Quitting..."); 
  /* Write registered boards */
  if (argc == 2 && nbreg) r = store_file (argv[1], 0);
  fflush (stdin);
  if (nbreg) { tag++; LOGERR ("Created %d board(s)", nbreg); }
  return nbreg;
}

/* Show and define the origin directory: o[i] */
uint16_t f_odup  (uint8_t *prog, int wc, uint8_t *word []) {
  uint16_t entry;

  /* o prints out the current value */
  if ((strcmp (prog, "o") == 0) &&  wc == 1) {
    LOG ("Origin directory  is %s\n", origdir); return 0;
  }
  /* Command o1, o2, o3 ... o<n> */
  entry = prog [1] - 0x30 - 1;
  if (entry >= 0 && entry <= MAXCONF){
    if (odirs [entry] == NULL) ERROR (-1, "Undefined origin entry %d", entry+1);
    strcpy (origdir, odirs [entry]);
  }
  else {
    /* Define origin directory */
    strcpy (origdir, word [1]);
  }
  LOG ("Changing load directory to %s\n", origdir);
  return 0;
}

/* Pivote dup file records: p [-d] dupfile [<board>-<angle>]* */
uint16_t f_pdup (uint8_t *prog, int argc, uint8_t *argv[]) {
  uint16_t r, min, angle, shiftc, card, suit, position;
  struct board *prec;
  uint8_t *p, c, *pc;
  
  if (argc<2) SYNTAX (pdup);
  dupname = argv[0];
  while ((p = argv[1]) != NULL) {
    p = get_board_number (p, &min); if (min == 0)
      ERROR (-5, "Invalid board designation");
    c = *p++;
    if (c !='-' && c != ',') SYNTAX (pdup);
    if ((angle = getnum (p)) == 255) ERROR (-5, "Invalid angle %d", angle);
    shiftc = angle/90;
    /* Walk the linked-list up to the  item */
    item = first; prec = NULL;
    while (item!=NULL && item->number < min)
      { prec = item; item=item->next; }
    if (item!=NULL) shift_all (item, shiftc);
    update_comment (item, shiftc);
    LOG ("Pivoting board %d by %d\n", min, angle);
    pretty_print_board (item, stdout);
    argc--; argv++;
  }
  r = store_file (dupname, imode);
  return r;
}

/* Print deals to text : pr|px [-d] [prnfile|excelfile] */
uint16_t f_prdup (uint8_t *prog, int argc, uint8_t *argv[]) {
  uint8_t prname[256], xlname [256], xlmodel [256], currx[256], pcom[256];
  enum pmode pm;
  uint8_t sheet=0, *pext, r;
  FILE *prnfile;

  /* prname is either the .txt file or the .bat file to produce the xlname Excel file */ 
  pm = (strcmp (prog, "px") == 0)? excel : text;
  /* By default the Excel file is created in Target directory */
  strcpy (xlname, targetdir); strcat (xlname, "\\");
  /* Excel model is in the root directory, for the time being the working directory */
  strcpy (xlmodel, workdir);  strcat (xlmodel, "\\"); strcat (xlmodel, "donnes.xlsm");
  /* By default we reuse the basename of the PBN file to create the TXT file or XL file */
  strcpy (currx, currentfile); 
  if (NULL == (pext = get_extension (currx)))
    ERROR (1, "Currentfile w/o extension");
  if (pm == excel) {
    /* The BAT file should be allocated in the TMP directory */
    strcpy (prname, workdir); strcat (prname, "\\"); strcat (prname, BATFILENAME);
    if (argc == 1) {
      if (currentfile [0] != '\0') {
	strcpy (pext+1, "xlsm"); strcpy (xlname, currx);
      }
      else
	strcat (xlname, "donnes1.xlsm");
    }
    else if (argc == 2)
      strcat (xlname, argv [1]);
    if (debug) LOGERR ("Excel print:\n\t%s\n\t%s\n\t%s\n",
		       prname, xlname, xlmodel);
  }
  else { /* pm == text */
    strcpy (prname, targetdir); strcat (prname, "\\");
    if (argc == 1) {
      if (currentfile [0] != '\0') {
	strcpy (pext+1, "txt"); strcpy (prname, currx);
      }
      else
	strcat (prname, "donnes1.txt");
    }
    else
      if (argc == 2) strcat (prname, argv [1]);
    if (debug) LOGERR ("Text print:\n%s\n", prname);
  }
  /* Open text or bat file in either case */
  if ((NULL == (prnfile = fopen (prname, "w"))))
      ERROR (-2, "Cannot open %s", prname);
  /* Initialisation by copying from model */
  if (pm == text) {
    fprintf (prnfile, "%-24s%-35s%-14s\n", site1, site2, site3);
    fprintf (prnfile, "%-24s%-41s%-14s\n\\n", school, event, cdate);
  } else
    fprintf (prnfile, "copy %s %s\n", xlmodel, xlname);
  /* Walk the linked-list */
  item = first;
  while (item!=NULL) {
    sheet++;
    if (pm == text)
      pretty_print_board (item, prnfile);
    else
      pretty_print_bat (item, prnfile, xlname, xlmodel, sheet);
    item=item->next;
  }
  fclose (prnfile);
  /* Execute .txt or .bat */
  if (pm == text) {
    strcpy (pcom, pword); strcat (pcom, prname);
    r = _spawnlp (P_WAIT, pcmd, pcmd, "/c", pcom, NULL);
  }
  else
    r = _spawnlp (P_WAIT, pcmd, pcmd, "/c", prname, NULL);
  if (r != 0) fprintf (stderr, "Spawn error %s %d\n", ((pm == text)? (uint8_t *)"word": prname), r);
  LOGERR ("Diagrams available in %s\n", (pm == text)? prname: xlname);
}

/* Exit idup */
uint16_t f_quit  (uint8_t *prog, int wc, uint8_t *word []) {
  fflush (stdout); LOG ("");
  exit(0);
}

/* Remove dup file records; r [-d] dupfile [n|n-m]*  */
uint16_t f_rdup (uint8_t *prog, int argc, uint8_t *argv[]) {
  uint16_t r, deleted;

  deleted = f_gdup (rdup, rdup, argc, argv);
  LOG ("Removing %d records\n", deleted);
  count -= deleted;
  r = store_file (dupname, imode);
  return r;
}

/* Show dupfile : s dupfile [<n>|<n>-<m>]* */
uint16_t f_sdup (uint8_t *prog, int argc, uint8_t *argv[]) {
  uint16_t shown;
   
  shown = f_gdup (sdup, sdup, argc, argv);
  return 0;
}

/* Show and define the target directory : t<n> */
uint16_t f_tdup  (uint8_t *prog, int wc, uint8_t *word []) {
  uint16_t entry;

  /* t prints out the current value */
  if ((strcmp (prog, "t") == 0) &&  wc == 1) {
    LOG ("Target directory is %s\n", targetdir); return 0;
  }
  /* Command t1, t2, t3 or t<n> */
  entry = prog [1] - 0x30 - 1;
  if (entry >= 0 && entry <= MAXCONF){
    if (odirs [entry] == NULL) ERROR (-1, "Undefined configuration file entry %d", entry+1);
    strcpy (targetdir, odirs [entry]);
  }
  else {  /* Define target directory */
    strcpy (targetdir, word [1]);
  }
  LOG ("Changing target directory to %s\n", targetdir);
  return 0;
}

/* Unify dup file: u [-d] dupfile [<radical>] */
uint16_t f_udup (uint8_t *prog, int argc, uint8_t *argv[]) {
  uint16_t i, fn, r;
  uint8_t *rad;
  uint8_t fname [LINE_SIZE];
  uint8_t name [] = "board";
  FILE* destfile;

  strcpy (name, (imode == 0)? argv [0] : (uint8_t *)"board");
  if (argc>2) SYNTAX (udup);
  if (argc == 2) rad = argv[1]; else rad = basename (name); 
  item=first; fn = 1;
  while (item!=NULL) {
    sprintf (fname, "./%s-%02d.dup", rad, fn); 
    destfile = fopen (fname, "w");
    encode_dup_board (board_record, item->bb, 1);
    r = fwrite (board_record, DUP_RECORD_SIZE, 1, destfile);
    fclose (destfile);
    LOG ("Creating file %s \n", fname);
    item=item->next; fn++;
  }  
  return fn;
}

/* Write all boards into a file : w [filename] */
uint16_t f_wdup (uint8_t *prog, int wc, uint8_t *word []) {
  uint16_t r;
  uint8_t *filename = file_name;

  if (wc == 1)
    strcpy (filename, (currentfile [0] == '\0')? (uint8_t *)"Donnes.pbn": currentfile);
  else if (wc == 2)
    filename = word [1];
  else SYNTAX (wdup); 
  r = store_file (filename, 0);
  return r; 
}

/* Define the origin and target directories : w working-directory */
uint16_t f_wndup  (uint8_t *prog, int wc, uint8_t *word []) {
  prog [0] = 'o'; f_odup (prog, wc, word);
  prog [0] = 't'; f_tdup (prog, wc, word);
}

/* Exchange cards within a dupfile record: x [-d] dupfile [<board#> XY-ZT]* */
uint16_t f_xdup (uint8_t *prog, int argc, uint8_t *argv[]) {

  uint16_t i, n, r, ibn;
  uint16_t card1, suit1, position1;
  uint16_t card2, suit2, position2;
  uint8_t c1, c2, s1, s2, tn; 
  uint8_t *dupname, *p;
  
  dupname = argv[0];
  while (argc >= 3) {
    sscanf (argv[1], "%c%d", &tn, &ibn);
    p = argv[2];
    card1 = card2 = suit1 = suit2 = 0;
    card1 = getcard (c1 = *p++); suit1= getsuit(s1 = *p++);
    if (*p == '-' || *p == ',') { p++; card2 = getcard (c2 = *p++); suit2 = getsuit(s2 = *p++); }
    else 
      SYNTAX (xdup);
    if (card1 == 0xFF || suit1 == 0xFF || card2 == 0xFF || suit2 == 0xFF) 
      ERROR (-5, " board #%d : Invalid card: %c%c or %c%c", ibn, c1, s1, c2, s2);
    item = first; 
    while (item!=NULL && (item->tag < tn || (item->tag == tn && item->number < ibn))) item=item->next; 
    if (item == NULL) ERROR (-6, "Invalid board number (%d)", ibn);
    position1 = get (item->bb, card1, suit1); 
    position2 = get (item->bb, card2, suit2);
    if (position1 == position2) 
      ERROR (-5, "%c%c and %c%c are in same hand in board %c%d", c1, s1, c2, s2, tn, ibn);
    else {
      set (item->bb, card1, suit1, position2); 
      set (item->bb, card2, suit2, position1);
      LOG ("Exchanging cards %c%c and %c%c in board %c%d\n", c1, s1, c2, s2, tn, ibn);
      pretty_print_board (item, stdout);
    }
    argc -= 2; argv +=2;
  }
  r = store_file (dupname, imode);
  return r;
}



uint8_t seek_name (uint8_t *field_s) {
  int8_t a = donneur;
  while (a != niet) if (strcmp (field_s, field_name[a])==0) break; else a++;
  return (a);
}

void field_error (enum field a, uint8_t * buf) {
}

void set_board_parameter (int8_t a, char *buf, int sz) {
  /* Global struct board b and comment buffer is used */
  uint8_t s,c, suit, pos, p, v, fe=1;
  uint16_t vv;

   switch (a) {
   case donneur:
     if (fe = ((v = decode_dealer (buf)) != 255)); b.dealer = v;
     break;
    
  case bnum:
    get_board_number (buf, &vv);
    if (fe = (vv <= MAXBOARD)) b.number = vv;
    break;

  case vul:
    if (fe = ((v = decode_short_vulnerability (buf)) != 255)) b.vulnerability = v;
    break;

  case contrat:
    break;

  case declarant:
    break;

  case entame:
    s = getsuit (buf[1]); c = getcard (buf[0]);
    if (fe = ((s!=255) && (c<13))) b.leadcard = 13 * s + c;
    break;

  case resultat:
    break;

  case o1 ... s5:
    if (fe = ((v = encode_bid (buf, french)) != b_error)) b.bidding [a-o1] = v; 
    break;

  case np: case nc: case nk: case nt: case ep: case ec: case ek: case et:
  case op: case oc: case ok: case ot: case sp: case sc: case sk: case st:
    /* Compute position and suit */
    suit = (a-np) % 4; pos =(a-np) / 4;
    while ((c = *buf++)!='\0') {
      /* Case of 10 */
      if (c=='1') c='X'; if (c=='0') continue;
      if (pos != 3) set (b.bb, ascii[c], suit, pos);
      else {
	p = get (b.bb, ascii[c], suit);
	if (p != 3) LOGERR ("Anomaly in South cards: %c", c);
      }
    }
    break;
    
  case c1 ... c12:
    strcpy (b.kom[a-c1], buf);
    break;
   }
   if (! fe) LOGERR ("Incorrect %s field value: %s", field_name [a], buf);

}

void commit_board (int num)  {
   uint8_t nb[4], ck;
   
  DEBUG ("Recording of board # %d\n", num);
  commit_item ();
  pretty_print_board (item, stdout);
  ck = check_board (nb, item);
  LOGERR ("Board %d is %scomplete [%d][%d][%d][%d]",num,((ck==1)?"":"in"), nb[0],nb[1],nb[2],nb[3]);
  /* Increment board count */
  bn++; 
}  

uint8_t readword (unsigned char *field_s, int il, int it, unsigned char *buf, int sz) {
  uint8_t nc;
  fprintf (stderr, "Enter ta_[%d][%d] (Value=%s) ?", il,it, buf);
  fflush (stderr); fflush (stdin);
  fgets (field_s, MAXLINE, stdin);
  nc = strlen (field_s)-1; field_s [nc] = '\0'; /* strip \n */
  return nc;
}

uint8_t  initialize (int8_t a_mode) {
  /* Return 1: correct initialization ok; return 0: stop everything */
  int x, y, z; int br; uint8_t ff [32];
  int8_t a;
  
  switch (a_mode) {
  case capture:
    init_item ();
    for (int i=0; i<MAXL; i++) for (int j=0; j<MAXC; j++) driver[i][j] = niet;
    deffile = fopen (deffilename, "r");
    while ((br = fscanf (deffile, "%d;%d;%s", &x, &y, ff)) == 3) {
      if ((a = seek_name (ff)) == niet) LOGERR ("Unknown field: %s at [%d][%d]\n", ff, x, y);
      driver [x][y] = a;
    }
    fclose(deffile);
    if (debug) LOG ("Driver table loaded\n");
    csvfile = fopen (csvfilename, "r");
    if (csvfile == NULL) { fprintf (stderr, "CSV file %s not found\n", csvfilename); return 0; }
    break;
  
  case learning:
    strcpy (tmpname, "You are to write file "); strcpy (tmpname, deffilename);
    if (yesno (tmpname))  deffile = fopen (deffilename, "w");
    else return 0;
    for (int i=0; i<MAXL; i++) for (int j=0; j<MAXC; j++) driver[i][j] = niet;
    csvfile = fopen (modelfilename, "r");
    if (csvfile == NULL) { fprintf (stderr, "Model file %s not found\n", modelfilename); return 0; }
  }
  return 1;
}

uint8_t operate (unsigned char *field_s, int il, int it, unsigned char *buf, int sz,int8_t a_mode){
/* sz : size of value whereas lg: size of field_s, ex: field_s=donneur value=N */
/* In both modes, fied_s is fllled the the name of the field */
  int8_t a, b=donneur, interrupt = FALSE;

 if (sz<=1) return FALSE;
 switch (a_mode) {
/* Learning mode */
 case learning :
   a = niet;
   forever {
     if (readword (field_s,il,it,buf,sz) == 0) break;
     if (interrupt = (strcmp (field_s, "q") == 0)) break;
     if ((a = seek_name (field_s)) != niet) break;
     else {
       LOGERR ("Unknown field: %s at [%d][%d]\nExpected values:", field_s, il, it);
       while (b != niet) fprintf(stderr, "%s, ", field_name [b++]);
       LOGERR ("q to quit");
     }
   }
   driver [il][it] = a;
   break;

/* Capture mode */
 case capture:
   a = driver [il % MAXL][it]; strcpy (field_s, field_name [a]);
   if (a!=niet) set_board_parameter (a, buf, sz);
 }
 return interrupt;
}

void finalize (int8_t a_mode) {
  unsigned char flip, flop;
  switch (a_mode) {
  case capture:
    fclose (csvfile);
    
  case learning:
    for (int i=0; i<MAXL; i++)
      for (int j=0; j<MAXC; j++)
	if (driver[i][j] != niet)
	  fprintf (deffile, "%d;%d;%s\n", i, j, field_name [driver[i][j]]); 
    fclose (deffile);
    DEBUG ("Driver values are in %s", deffilename);
    fclose (csvfile);
  }
}

void follow_model ( int8_t a_mode) {
int i, il, it, sz;
 _Bool eof, eol, eot;
int ch;
unsigned char buf [MAXLINE+1];

  if (initialize (a_mode) == 0) return;
  eof = eol = eot = FALSE;
  il  = 1; it = 1; sz = 0;
  ib = 1;
  while (! eof) {
    while (! eol) {
      while (! eot) { /* tant qu'il y a des tokens */
	ch = getc (csvfile);
	if (eof = (ch == -1)) /* si fin de fichier */
	  eol=eot= TRUE;
	else if (eol = (ch == '\n')) /* si fin de ligne */
	  eot= TRUE;
	else if (eot = (ch == ';')) /* si fin de token */
	  eot = TRUE;
	if (eot) {
	  buf [sz++] = field_s [0] = '\0';
	  if (operate (field_s, il, it, buf, sz, a_mode)) eot = eol = eof = TRUE;
	  DEBUG ("Field[%d,%d]=%s -> %s", il, it, buf, field_s);
	  it++; sz = 0; eot = FALSE;
	} else
	  buf [sz++] = ch;
	if (eol || eof) break;
      }
    }
    /* New line starts a new board if il  == n*MAXL + 1 */
    il++; it = 1; sz = 0; eol = eot = FALSE;
    if ((il % MAXL) == 1) {/* Fin de sous-tableau */
      DEBUG ("Fin du  sous-tableau %d a la ligne %d", ib, il-1);
      if (a_mode == capture) commit_board (ib);
      /* Learning is the same on other boards */
     if (il > MAXL && a_mode == learning) break;
     ib++;
      DEBUG ("Nouveau sous-tableau %d a la ligne %d", ib, il);
      init_item ();
     }
  } /* eof */
  finalize (a_mode);
}

/* Analyze an Excel csv file as a board model */
uint16_t f_xmdup (uint8_t *prog, int argc, uint8_t *argv[]) {
  uint8_t *ptfile, *rad;
 /* Build a dynamic node list to drive csv capture into .PBN file */
  /* Find Excel Model file in CSV format */
  if (argc > 2) SYNTAX (xmdup);
  ptfile = (argc==1)? (uint8_t *)DEFAULTMODELNAME:(argc==2)?argv [1]: NULL;
  strcpy (modelfilename, ptfile);
  /* Def file has the same base as model file with .def extension */
  rad = get_radical (modelfilename); strcpy (deffilename, rad); strcat (deffilename,".def");
  DEBUG ("Model file is %s, Def file is %s", modelfilename, deffilename);
  follow_model (learning);
}

/* Load an Excel csv file as a board */
uint16_t f_xldup (uint8_t *prog, int argc, uint8_t *argv[]) {

/* Find Excel input file in CSV format */
  if (argc != 2) SYNTAX (xldup);
  strcpy (csvfilename, argv [1]);
  /* Structure dynamic to drive csv capture into .PBN file */
  follow_model (capture);
}

/* Toggle debug : z */
uint16_t f_zdup (uint8_t *prog, int argc, uint8_t *argv []) {
  
  debug = ~debug;
  LOGERR ("Debug %s", (debug)? "ON" : "OFF");
  return 0;
}

uint16_t f_ukwn (uint8_t *prog, int argc, uint8_t *argv []) {

  ERROR (-1, "Invalid program name %s", prog);
}

/* Interactive command dispatch table */
uint16_t f_idup (uint8_t *, int, uint8_t *[]);
struct dispatch { 
  uint8_t *iname; uint16_t (*f_com)(uint8_t*, int, uint8_t *[]);
} dispatch [] = {
  "a",  f_adup, /* assemble */
  "b",  f_bdup, /* pbn to dup */
  "c",  f_cdup, /* clear */
  "cd", f_cddup,/* change directory */
  "d",  f_ddup, /* distribute */
  "de", f_ddup, /* distribute etuis */
  "dv", f_ddup, /* distribute vulnerability */
  "e",  f_edup, /* enter */
  "fc", f_fcdup,/* directories in configuration file */
  "f",  f_fdup,/* search files in Working dir */
  "fo", f_fdup,/* search files in origin dir */
  "ft", f_fdup,/* search files in target dir */
  "h",  f_hdup, /* headers */
  "?",  f_help, /* syntax summary */
  "i",  f_indup,/* insert */
  "i",  f_idup, /* interact */
 "ido", f_ifdup,/* donneur */
 "ide", f_ifdup,/* declarant */
  "iv", f_ifdup,/* vulnerabiite */
 "ic",  f_ifdup,/* contrat */
 "ir",  f_ifdup,/* resultat */
 "ie",  f_ifdup,/* entame */
"ied",  f_ifdup,/* evenement et date */
 "icom",f_ifdup,/* commentaire */
 "ia",  f_ifdup,/* auctions */
 "it",  f_ifdup,/* tous */
  "k",  f_kdup, /* konfigure */
  "l",  f_ldup, /* load files */
  "m",  f_mdup, /* more */
  "n",  f_ndup, /* new board */
  "o",  f_odup, /* origin directory */
  "o1", f_odup, /* First entry of idup.cng */
  "o2", f_odup, /* Second */
  "o3", f_odup, /* Third */
  "o4", f_odup, /* Fourth */
  "o5", f_odup, /* Fifth */
  "o6", f_odup, /* Sixth */
  "o7", f_odup, /* Seventh */
  "o8", f_odup, /* Eightth */
  "o9", f_odup, /* Ninth */
  "p",  f_pdup, /* pivote */
  "pr", f_prdup,/* print to text file */
  "px", f_prdup,/* print to excel file */
  "q",  f_quit, /* quit */
  "r",  f_rdup, /* remove */
  "s",  f_sdup, /* show */
  "t",  f_tdup, /* target directory */
  "t1", f_tdup, /* 1st entry of idup.cfg */
  "t2", f_tdup, /* 2nd */
  "t3", f_tdup, /* 3rd */
  "t4", f_tdup, /* 4th */
  "t5", f_tdup, /* 5th */
  "t6", f_tdup, /* 6th */
  "t7", f_tdup, /* 7th */
  "t8", f_tdup, /* 8th */
  "t9", f_tdup, /* 9th */
  "u",  f_udup, /* unify */
  "w",  f_wdup, /* write file */
  "w1", f_wndup,/* o1 + t1 */
  "w2", f_wndup,/* o2 + t2 */
  "w3", f_wndup,/* o3 + t3 */
  "w4", f_wndup,/* o4 + t4 */
  "w5", f_wndup,/* o5 + t5 */
  "w6", f_wndup,/* o6 + t6 */
  "w7", f_wndup,/* o7 + t7 */
  "w8", f_wndup,/* o8 + t8 */
  "w9", f_wndup,/* o9 + t9 */
  "x",  f_xdup, /* exchange */
  "xl", f_xldup,/* load excel csv file */
  "xm", f_xmdup,/* learn excel csv file */
  "z",  f_zdup, /* zoom = debug */
  "-",  f_ukwn  /* unknown */
};
#define NB_COMMAND (sizeof(dispatch)/sizeof(struct dispatch))

/* Interactive mode */
uint16_t f_idup (uint8_t *prog, int argc, uint8_t *argv[]) {
  uint16_t i, sn, r, wc, choice;
  uint8_t *word [16], *base;
  /* Set interactive mode */
  imode = 1;
  /* Syntax : i [dupfile]* */
  while (argv [1] != NULL)  { 
    r = load_file (argv [1], argv [2], tag);
    if (r != 0) { tag++; count += r; }
    argc--; argv++;
  }
  LOGERR ("Working directory is %s", workdir); 
  forever {
    /* Read command line */
    printf ("%s$ ", workdir); fflush (stdout); fflush (stdin); 
    if (NULL == fgets (line, LINE_SIZE, stdin))
      FATAL (1, "standard input reading error");
    /* Word decomposition a la argc, argv */
    wc = setargs (line, LINE_SIZE, word); 
    base = word [0]; 
    if (base != NULL) {
      choice = 0;
      while (choice<(NB_COMMAND-1))
	if (strcmp (base, dispatch [choice].iname) == 0) break; else choice++;
      r = (*dispatch [choice].f_com)(base, wc, word); 
    }
  }
}

/* Program dispatch table */
enum program {
  assemble, bridge, distribute, enter, help, interact, more, neww_, pivote, printt, printx, remove_, show, unify, exchange, xlload, xlmodel, none};
/* Specify file input     a  b  d  e  h  i  m  n  p  pr px r  s  u  x xl xm  ukwn */
uint8_t input_file [] = { 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0 };
struct prog { 
  uint8_t *name; 
  uint16_t (*f_com)(uint8_t *prog, int argc, uint8_t *argv []);
} tprog [] = {
  "adup", f_adup, /* assemble */
  "bdup", f_bdup, /* bridge */
  "ddup", f_bdup, /* distribute */
  "edup", f_edup, /* enter */
  "hdup", f_help, /* help */
  "idup", f_idup, /* interact */
  "mdup", f_mdup, /* more */
  "ndup", f_ndup, /* new */
  "pdup", f_pdup, /* pivote */
 "prdup", f_prdup,/* print */
 "pxdup", f_prdup,/* print */
  "rdup", f_rdup, /* remove */
  "sdup", f_sdup, /* show */
  "udup", f_udup, /* unify */
  "xldup",f_xldup, /* exchange */
  "xmdup",f_xmdup, /* exchange */
  NULL,   f_ukwn, /* unknown */
};

#define NB_PROG (sizeof(tprog)/sizeof(struct prog))

/* Main dispatches commands based on program name */
int main (int argc, uint8_t *argv[]) {
  uint16_t n, r;
  uint8_t *base, *radical, *tn;
  enum program select;  

  /* Program name */
    /* Debug option */
  if ((argc>1) && (strcmp (argv [1], "-d")==0)) { argc--; argv++; debug = 1; }
  /* Print banner with author and GNU GPL license */
  print_banner ();
  /* Name and location of program */
  strcpy (rootdir, argv[0]); n = strlen (rootdir);
  base = basename (rootdir); radical = get_radical (base);
  while (*(rootdir+n) != '\\') n--; *(rootdir+n) = '\0';
  /* Print rootdir in absolute mode */
  make_absolute  (rootdir);
  LOGERR ("Root directory = %s", rootdir);
  /* Read default konfiguration file */
  f_kdup (NULL, 0, NULL); 
  /* Line prompt is current working directory */
  workdir = getcwd (NULL, 0);
  strcpy (origdir, workdir); 
  strcpy (targetdir, workdir); 
  /* Select function to perform based on program name */
  select = assemble; 
  while ((tn=tprog[select].name) != NULL && strcmp (tn,radical) != 0) select++;
  /* Check syntax and load file argument */
  if (input_file [select]) {
    if (argc < 2) SYNTAX (select);
    r = load_file (argv[1], argv[2], tag);
    if (r != 0) { tag++; count += r; }
    argc--; argv++; 
  }
  /* Call function */
  r = (*tprog [select].f_com) (base, argc, argv);
  return r;
}
