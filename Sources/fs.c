#include <stdint.h>
#include <stdio.h>
#include <string.h>

enum act { abuf, aout, asub, aexit };

struct tr { uint8_t trans; uint8_t cmin, cmax; enum act act; }
  fstate [] = {
  /* State 0 */
  /*  0 */  {  7,  'N','N', abuf },
  /*  1 */  { 13,  'E','E', abuf },
  /*  2 */  { 17,  'S','S', abuf },
  /*  3 */  { 21,  'O','O', abuf },
  /*  4 */  { 33, 'A','Z', aout },
  /*  5 */  { 0, '\0','\0',aexit },
  /*  6 */  { 0,   0,255, aout },
  /* State 1 */
  /*  7 */  { 9,   'O','O', abuf },
  /*  8 */  { 0,   0, 255,aout },
  /* State 2 */
  /*  9 */  { 11,  'R','R', abuf },
  /* 10 */  { 0,  0, 255, aout },
  /* State 3 */
  /* 11 */  { 29,  'D','D', abuf },
  /* 12 */  { 0,  0, 255, aout },
  /* State 4 */
  /* 13 */  { 15, 'S','S',  abuf },
  /* 14 */  { 0,  0, 255, aout },
  /* State 5 */
  /* 15 */  { 31, 'T','T',  abuf },
  /* 16 */  { 0,  0, 255, aout },
  /* State 6 */
  /* 17 */  { 19, 'U', 'U', abuf },
  /* 18 */  { 0,  0, 255, aout },
  /* State 7 */
  /* 19 */  { 29,  'D','D',  abuf },
  /* 20 */  { 0,  0, 255, aout },
  /* State 8 */
  /* 21 */  { 23,  'U','U', abuf },
  /* 22 */  { 0,  0, 255,  aout },
  /* State 9 */
  /* 23 */  { 25, 'E', 'E', abuf },
  /* 24 */  { 0,  0, 255, aout },
  /* State 10 */
  /* 25 */  { 27,  'S','S', abuf },
  /* 26 */  { 0,  0, 255, aout },
  /* State 11  */
  /* 27 */  { 29,  'T','T', abuf },
  /* 28 */  { 0,  0, 255, aout },
  /* State 12  */
  /* 29 */  { 0,  'A','Z',  aout },
  /* 30 */  { 0,  0, 255, asub },
  /* State 13  */
  /* 31 */  { 0,  '-','-', aout },
  /* 32 */  { 0,   0, 255, asub },
  /* State 14  */
  /* 33 */  { 33, 'A',  'Z', aout },
  /* 34 */  { 0,  0, 255, aout }
};

uint8_t pold [] = "Nord SUDa Est Ouesr Nordi Est+Ouest aussi. Est-il l�?";
uint8_t pnew [2048];

uint8_t *subs [4] = { "Nord", "Est", "Sud", "Ouest" };
uint8_t rot = 1;

#define TRUE 1
#define FALSE 0

#define U(c) ((((c)>='a') && ((c)<='z'))? ((c)-0x20): (c))

uint8_t *substitute (uint8_t *buf, uint8_t *subs [], uint8_t a) {
  uint8_t i = 0;
  for (i=0; i<4; i++)
    if (strcmp (buf, subs [i]) == 0) return subs [(i+a)%4];
  return NULL;
}

uint8_t *pmov (uint8_t *pn, uint8_t *p) {
  uint8_t c;
  if (p == NULL) return NULL;
  while (*p != '\0') { c = *p++; *pn++ = c; }
  return pn;
}

void update_comment (uint8_t *old, uint8_t *new, uint8_t *subs[], uint8_t rot) {
  uint8_t buf [6];
  uint8_t t, c, uc, cc, d, e, *pn, *po, *pb, *p;
   
  po = old; pn = new; pb = buf;
  t = 0; c = *po++;
    do {
      uc = U(c);
      if ((uc>= fstate [t].cmin) && (uc<=fstate [t].cmax)) {
	switch (fstate [t].act) {
	case asub:
	  /* outbuf */
	  p = substitute (buf, subs, rot);
	  pn = pmov (pn, p);
	  pb = buf;
	  /* Note: current character c will be re-examined */
	  break;
	case aout:
	  pn = pmov (pn, buf); pb = buf;
	  *pn++ = c;
	  c = *po++;
	  break;
	case abuf:
	  *pb++ = c;
	  c = *po++;
	  break;
	case aexit:
	  *pn++ = c;
	}
	*pb = '\0';
	t = fstate [t].trans;      
      }
      else
	t++;
    } while (c != '\0' );
}

int main (int argc, char *argv[]) {
  printf (pold);
  printf ("\n");
  update_comment (pold, pnew, subs, rot);
  printf (pnew);
  return 0;
}
 
