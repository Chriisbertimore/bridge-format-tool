enum language { french, english };
#define sharp #

#define GEN(Name, French, English) \
char *t_##Name [2] = {  French,  English }; \
sharp define F_##Name	t_##Name[0] \
sharp define E_##Name	t_##Name[1]

GEN(YESNO,"Oui ou Non [O|N|Q|<ret>", "Yes or No [Y|N|q|<ret>")

void (void) {
  printf (F_YESNO);
}




