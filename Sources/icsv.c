#include <stdio.h>
#include <strings.h>
#include <stdint.h>

#define FALSE 0
#define TRUE  1
#define MAXLINE 80
#define BSIZE 12
#define NBOARD 8
#define MAXL 96
#define MAXC 12

enum mode_action {apprentissage, capture};
enum mode_action mode = apprentissage;

FILE *csv, *fs;
_Bool debug = FALSE;
int ib =1;
unsigned char act [MAXL+1];

#define DEBUG(...)                                              \
  if (debug) 							\
  do { fprintf (stderr, __VA_ARGS__); fprintf (stderr, "\n");   \
    fflush (stderr);						\
  } while (0)

void readword (unsigned char *act, int il, int it, unsigned char *buf, int sz) {
  int nc;
  fprintf (stderr, "Enter ta_[%d][%d] (Value=%s) ?", il,it, buf);
  fflush (stderr); fflush (stdin);
  fgets (act, MAXL, stdin);
  /* strip \n */
  nc = strlen (act); if (nc>0) act [nc-1] = '\0';
}

void initialize (void) {
  fs = fopen ("itab.c", "w");
  fprintf (fs, "#define BSIZE %d\n", BSIZE);
  fprintf (fs, "#define NBOARD %d\n", NBOARD);
  fprintf (fs, "enum action {donneur,bnum,vul,contrat, declarant,entame,resultat,\n");
  fprintf (fs, "o1,o2,o3,o4,o5,o6,n1,n2,n3,n4,n5,n6,e1,e2,e3,e4,e5,e6,s1,s2,s3,s4,s5,s6,\n");
  fprintf (fs, "np,nc,nk,nt,ep,ec,ek,et,op,oc,ok,ot,sp,sc,sk,st,\n");
  fprintf (fs, "c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16}; \n");
  fprintf (fs, "typedef struct _pair {\n");
  fprintf (fs, "  uint8_t il; uint8_t it; enum action fun; struct _pair *next;\n");
  fprintf (fs, "} Pair;\n" );
  fprintf (fs, "Pair *pfirst = NULL, *plast = NULL;\n");
  fprintf (fs, "void plist_init (void) {\n");
  fprintf (fs, "Pair *pp, *prec;\n");
  fflush (fs);
}

void action (unsigned char *act, int il, int it, unsigned char *buf, int sz){
static uint8_t count = 0;
/* sz : size of value whereas lg: size of act
 * example: act=donneur value=N
 */
int lg;
   /* Mode apprentissage */
   if (mode == apprentissage && il<BSIZE) { 
     if (sz>1) {
       readword (act,il,it,buf,sz);
       if ((lg = strlen (act)) != 0) {
	 fprintf (fs,"pp=(Pair *) malloc(sizeof(struct _pair));\n");
	 fprintf (fs,"pp->il=%d; pp->it=%d, pp->fun=%s;\n",il,it,act);
	 if (0==count++)
	   fprintf (fs, "pfirst=prec=pp;\n");
	 else
	   fprintf (fs, "prec->next=pp; prec=pp;\n");
       }
     }
   }
   if (mode == capture) {
   }
   fflush (fs);
}

void finalize (void) {
  fprintf (fs, "pp->next=NULL; plast=pp;\n");
  fprintf (fs, "}\n");
  fclose(fs);
}

void init_board (int nb)  {
  DEBUG ("Initialisation de la main # %d\n", nb);
}
void commit_board (int nb)  {
  DEBUG ("Enregistrement de la main # %d\n", nb);
}  

int main (void) {
  int i, il, it, sz, ib;
  _Bool eof, eol, eot;
  int ch;
  unsigned char buf [MAXLINE];

  initialize ();
  csv = fopen ("../Models/2MF.csv", "r");
  if (csv == NULL) { fprintf (stderr, "File not found\n"); return (-1); }
  eof = eol = eot = FALSE;
  il  = 1; it = 1; sz = 0;
  ib = 1; init_board (ib);
  while (! eof) {
    while (! eol) {
      while (! eot) { /* tant qu'il y a des tokens */
	ch = getc (csv);
	if (eof = (ch == -1)) /* si fin de fichier */
	  eol=eot= TRUE;
	else if (eol = (ch == '\n')) /* si fin de ligne */
	  eot= TRUE;
	else if (eot = (ch == ';')) /* si fin de token */
	  eot = TRUE;
	if (eot) {
	  buf [sz++] = '\0';
	  action (act, il, it, buf, sz);
	  DEBUG ("Action %d %d %s %d", il,it, buf, sz);
	  it++; sz = 0; eot = FALSE;
	} else
	  buf [sz++] = ch;
	if (eol || eof) break;
      }
    }
    /* Nouvelle ligne */
    il++; it = 1; sz = 0; eol = eot = FALSE;
    if ((il % BSIZE) == 1) {/* Fin de sous-tableau */
      DEBUG ("Nouveau sous-tableau a la ligne %d", il);
      commit_board (ib);
      ib++;
      init_board (ib);
    }
  }
  fclose (csv);
  finalize ();
  return 0;
}
