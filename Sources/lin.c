#include <ctype.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <strings.h>
#include <malloc.h>
#include <libgen.h>
#include <stdarg.h>
#include <dirent.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <stdlib.h>

#define true 1
#define false 0
int32_t lchar;
_Bool endoffile = false;
FILE *linfile;

/* Finite state automaton for .lin file decoding  */
/* Predicate functions */

_Bool is_a (int32_t c) { return c == 'a'; }
_Bool is_b (int32_t c) { return c == 'b'; }
_Bool is_B (int32_t c) { return c == 'B'; }
_Bool is_c (int32_t c) { return c == 'c'; }
_Bool is_d (int32_t c) { return c == 'd'; }
_Bool is_e (int32_t c) { return c == 'e'; }
_Bool is_E (int32_t c) { return c == 'E'; }
_Bool is_g (int32_t c) { return c == 'g'; }
_Bool is_h (int32_t c) { return c == 'h'; }
_Bool is_m (int32_t c) { return c == 'm'; }
_Bool is_n (int32_t c) { return c == 'n'; }
_Bool is_o (int32_t c) { return c == 'o'; }
_Bool is_O (int32_t c) { return c == 'O'; }
_Bool is_p (int32_t c) { return c == 'p'; }
_Bool is_r (int32_t c) { return c == 'r'; }
_Bool is_s (int32_t c) { return c == 's'; }
_Bool is_S (int32_t c) { return c == 'S'; }
_Bool is_t (int32_t c) { return c == 't'; }
_Bool is_u (int32_t c) { return c == 'u'; }
_Bool is_v (int32_t c) { return c == 'v'; }
_Bool is_0 (int32_t c) { return c == '0'; }
_Bool is_1 (int32_t c) { return c == '1'; }
_Bool is_blank (int32_t c) { return c == ' '; }
_Bool is_eol (int32_t c) { return c=='\n'; }
_Bool is_eof (int32_t c) { return c==EOF; }
_Bool is_eod (int32_t c) { return c==EOF||c=='\n'; }
_Bool is_bar (int32_t c) { return c == '|'; }
_Bool is_comma (int32_t c) { return c == ','; }
_Bool is_level (int32_t c) { return (c >= '1')&&(c <= '7'); }
_Bool  ucl (int32_t c) { return (c>='A')&&(c<='Z'); }
_Bool  lcl (int32_t c) { return (c>='a')&&(c<='z'); }
_Bool is_digit (int32_t c) { return (c>='0')&&(c<='9'); }
_Bool is_ (int32_t c) { return c == '_'; }
_Bool is_name (int32_t c) { return (lcl(c))||(is_digit(c))||(is_(c)); }
_Bool is_card_f (int32_t c) { return
  (c=='A')||(c=='R')||(c=='D')||(c=='V')||(c=='X')||((c>='1')&&(c<='9')); }
_Bool is_card_e (int32_t c) { return
  (c=='A')||(c=='K')||(c=='Q')||(c=='J')||(c=='T')||((c>='1')&&(c<='9')); }
_Bool is_colour_f (int32_t c) { return
  (c=='P')||(c=='C')||(c=='K')||(c=='T'); }
_Bool is_colour_e (int32_t c) { return
  (c=='S')||(c=='H')||(c=='D')||(c=='C'); }
_Bool is_denom_f (int32_t c) { return
  (c=='P')||(c=='C')||(c=='K')||(c=='T')||(c=='S'); }
_Bool is_denom_e (int32_t c) { return
  (c=='S')||(c=='H')||(c=='D')||(c=='C')||(c=='N'); }
_Bool is_nul (int32_t c) { return c == '\0'; }
_Bool lambda (int32_t c) { return true; }
uint8_t commit_item (void) {}
void init_board (void) {}
uint8_t bn=0, cb=0;

void l_commit (void) {
  
  /* We assume that ONE eol terminates a board definition */
  /* But we tolerate several consecutive semi-empty line  */
  commit_item ();
  bn++; cb++;
  /* Initialize parameters for next board */
  init_board ();
  /* Reach next non empty section */
  while (lchar != EOF && (lchar == '\t' || lchar == '\n'|| lchar ==' '))
    endoffile |= (EOF == (lchar = fgetc (linfile)));
}

void l_pass (void) {}
void l_double (void) {}
void l_redouble (void) {}
void l_decl (void) {}
void l_bid (void) {}
void l_num (void) {}
void l_buf (void) {}
void l_name (void) {}
void l_result (void) {}
void l_pos (void) {}
void l_suit (void) {}
void l_card (void) {}
void l_newpos (void) {}
void l_play (void) {}
void l_board (void) {}
void l_check_deal (void) {}
void l_exit (void) {}
void l_err (void) { fprintf (stderr, "Syntax error"); fflush (stderr); }
void l_none (void) {}

/* A transition is defined by;
 * - a state : next state of the automaton (0 is the restarting point)
 * - a predicate : condition on current character to change states 
 * - a semantics action : function performed before the transition.
 * the order is :
 * - apply predicate function to current char (lchar)
 * - if true execute the sem. function, change states and read next char
 * if false try next transition line in the automaton
 * lambda catches all remaining characters and causes an error messag
 */
struct _ltr {
  uint8_t state; _Bool (*predicate)(int32_t);  void (*l_sem)(void);
} l_state [] = {
  /* State 0 */
  #define state_0 0
  /*  0 */  {  1, is_a,   l_none },
  /*  1 */  {  3, is_b,   l_none },
  /*  2 */  {  3, is_o,   l_none },
  /*  3 */  { 40, is_p,   l_none },
  /*  4 */  { 16, is_m,   l_none },
  /*  5 */  { 22, is_B,   l_none },
  /*  6 */  { 28, is_r,   l_none },
  /*  7 */  { 31, is_s,   l_none },
  /*  8 */  {  0, is_eod, l_commit },
  /*  9 */  {  0, lambda, l_err },
  
  /* State 1 : a - */
  #define state_1 10
  /* 10 */  { 2,  is_h,  l_none },
  /* 11 */  { 0,  lambda,l_err },

  /* State 2 : ah  - */
  #define state_2 12
  /* 12 */  { 0, is_bar, l_none },
  /* 13 */  { 0, lambda, l_err },
  
  /* State 3 : b or o -*/
  #define state_3 14
  /* 14 */  { 4, is_bar, l_none },
  /* 15 */  { 0, lambda, l_err },
   
  /* State 4 : b| - */
  #define state_4 16
  /* 16*/  {  5, is_m,  l_none },
  /* 17*/  {  10, is_p,  l_none },
  /* 18 */  { 0,  lambda, l_err },
  
  /* State 5 : b|m - */
  #define state_5 19
    /* 19 */  { 6, is_b,  l_none },
  /* 20 */  { 0, lambda, l_err },
  
  /* State 6 : b|mb - */
  #define state_6 21
  /* 21 */  { 7, is_bar, l_none },
  /* 22 */  { 0,  lambda, l_err },
  
  /* State 7 : b|mb| - */
  #define state_7 23
  /* 23 */  { 8, is_p, l_pass },
  /* 24 */  { 8, is_d, l_double },
  /* 25 */  { 8, is_r, l_redouble },
  /* 26 */  { 9, is_level, l_num },
  /* 27 */  { 0, lambda, l_err },
  
 /* State 8 : b|mb|<decl> - */
  #define state_8 28
  /* 28 */  { 4, is_bar, l_decl },
  /* 29 */  { 0, lambda, l_err },
  
  /* State 9 : b|mb|<level>| - */
  #define state_9 30
  /* 30 */  { 8,  is_denom_e, l_bid },
  /* 31 */  { 0,  lambda, l_err },
  
  /* State 10 : b|p - */
   #define state_10 32
  /* 32 */  { 11, is_g, l_none },
  /* 33 */  { 0,  lambda, l_err },
  
  /* State 11  : b|pg - */
  #define state_11 34
  /* 34 */  { 12, is_bar, l_none },
  /* 35 */  { 0,  lambda, l_err },
  
  /* State 12 Final b|pg|| */
  #define state_12 36
  /* 36 */  { 0,  is_bar, l_none },
  /* 37 */  { 0,  lambda, l_err },

  /* State 13  : p - */ /* Patched by E40 */
  #define state_13 38
  /* 38 */  { 14, is_n, l_none },
  /* 39 */  { 0,  lambda, l_err },
  
  /* State 14 : pn - */
  #define state_14 40
  /* 40 */  { 15, is_bar, l_none },
  /* 41 */  { 0, lambda, l_err },

  /* State 15 : pn| -  */
  #define state_15 42
    /* 42 */  { 15, is_name, l_buf },
    /* 43 */  { 15, is_comma, l_name },
    /* 44 */  { 0, is_bar, l_name },
  /* 45 */  { 0, lambda, l_err },
  
  /* State 16 : m - */
  #define state_16 46
  /* 46 */  { 41, is_c, l_none },
  /* 47 */  { 18, is_d, l_none },
  /* 48 */  { 0, lambda, l_err },
  
  /* State 17 : mc - */ /* Patched by E41, E42, E43 */
  #define state_17 49
    /* 49 */  { 17, is_digit, l_num },
    /* 50 */  { 0,  is_bar, l_result },

    /* State 18 : md - */
  #define state_18 51
  /* 51 */  { 19, is_bar, l_none },
  /* 52 */  { 0,  lambda, l_err },
  
    /* State 19 : md| - */
  #define state_19 53
    /* 53 */  { 20, is_digit, l_pos },
  /* 54 */  { 0,  lambda, l_err },
  
    /* State 20 : md|<pos> - */
  #define state_20 55
    /* 55 */  { 21, is_colour_e, l_suit },
    /* 56 */  { 0,  is_bar, l_check_deal },
    /* 57 */  { 0,  lambda, l_err },
  
    /* State 21 : md|<pos><col> - */
  #define state_21 58
    /* 58 */  { 21, is_card_e, l_card },
    /* 59 */  { 21, is_colour_e, l_suit },
    /* 60 */  { 20, is_comma, l_newpos },
    /* 61 */  { 0,  lambda, l_err },

      /* State 22 : B - */
  #define state_22 62
  /* 62 */  { 23, is_o, l_none },
  /* 63 */  { 0,  lambda, l_err },
  
      /* State 23 : Bo - */
  #define state_23 64
  /* 64 */  { 24, is_a, l_none },
  /* 65 */  { 0,  lambda, l_err },
  
      /* State 24 : Boa - */
  #define state_24 66
  /* 66 */  { 25, is_r, l_none },
  /* 67 */  { 0,  lambda, l_err },
  
      /* State 25 : Boar - */
  #define state_25 68
  /* 68 */  { 26, is_d, l_none },
  /* 69 */  { 0,  lambda, l_err },
  
      /* State 26 : Board - */
  #define state_26 70
  /* 70 */  { 27, is_blank, l_none },
  /* 71 */  { 0,  lambda, l_err },
    
     /* State 27 : Board< > - */
  #define state_27 72
  /* 72 */  { 27, is_digit, l_num }, 
  /* 73 */  { 0,  is_bar, l_board },
  /* 74 */  { 0,  lambda, l_err },

    /* State 28 : r - */
  #define state_28 75
  /* 75 */  { 29, is_h, l_none },
  /* 76 */  { 0,  lambda, l_err },
  
      /* State 29 : rh - */
  #define state_29 77
  /* 77 */  { 30, is_bar, l_none },
  /* 78 */  { 0,  lambda, l_err },
  
      /* State 30 : rh| - */
  #define state_30 79
  /* 79 */  { 0, is_bar, l_none },
  /* 80 */  { 0,  lambda, l_err },
  
      /* State 31 : s - */
  #define state_31 81
  /* 81 */  { 32, is_v, l_none },
  /* 82 */  { 33, is_t, l_none },
  /* 83 */  { 0,  lambda, l_err },
  
      /* State 32 : sv - */
  #define state_32 84
  /* 84 */  { 0, is_bar, l_none },
  /* 85 */  { 0,  lambda, l_err },
  
     /* State 33 : st  - */
  #define state_33 86
  /* 86 */  { 34,  is_bar, l_none },
  /* 87 */  { 0,  lambda, l_err },
  
     /* State 34 : st|  - */
  #define state_34 88
  /* 88 */  { 0,  is_bar, l_none },
  /* 89 */  { 0,  lambda, l_err },

     /* State 35 eol or eof  */ /* nused */
  #define state_35 90
  /* 90 */  { 0,  is_eol, l_commit },
  /* 91 */  { 0,  is_eof, l_commit },

     /* State 36 : pc -  */
  #define state_36 92
  /* 92 */  { 37,  is_bar, l_none },
  /* 93 */  { 0, lambda, l_err },

  /* State 37 : pc| -  */
  #define state_37 94
    /* 94 */  { 38, is_colour_e, l_suit },
  /* 95 */  { 0, lambda, l_err},

  /* State 38 : pc|<level> -  */
  #define state_38 96
    /* 96 */  { 39, is_card_e, l_card },
  /* 97 */  { 0, lambda, l_err },

  /* State 39 : pc|<level><card> -  */
  #define state_39 98
    /* 98 */  { 0, is_bar, l_play },
  /* 99 */  { 0, lambda, l_err },

  /* State 40 : p -  patch 13 */
  #define state_40 100
  /* 100 */  { 14, is_n, l_none },
  /* 101 */  { 36, is_c, l_none },
  /* 102 */  { 11, is_g, l_none },
  /* 103 */  { 0,  lambda, l_err },


  s = 0; t= 0;
  endoffile |= (EOF == (lchar = fgetc (linfile)));
    while (! endoffile) {
    if (l_state [t].predicate (lchar)) {
      os = s; ot = t;
      /* Call semantic action */
      l_state [t].l_sem ();
      /* Change state and transition */
      s = l_state [t].state;
      t = state_trans [s];
      fprintf (stderr, "Transition %d ->%d lchar = %c\n", os, s, lchar);
      fflush (stderr);
      /* Read next char if needed */
      /* if (l_state [t].fetch) */
      endoffile |= (EOF == (lchar = fgetc (linfile)));
   }  else
      /* Try next entry */
      t++;
  }
}
                                                                                             
/* Load LIN file <origdir>/<name> into memory boards */
uint16_t main (void) {

  end linfile = fopen ("tt.lin", "r");
  if (linfile==NULL)fprintf (stderr, "Lin file %s does not exist", "tt.lin");
  decode_lin ();
  fclose (linfile);
  return 0;
}
