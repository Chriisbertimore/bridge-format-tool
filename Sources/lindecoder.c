/* lchar is a character variable shared between routines */ 
int32_t lchar;

void read_name (uint8_t *s) {
  uint8_t nc = 0;

  while ((lchar = fgetc (linfile)) != EOF &&
	 (isletter (lchar) || is_digit (lchar) || (lchar == '-')))
    if (nc < 18) s [nc++] = lchar;
  s [nc] = '\0';
}

uint8_t read_pos (void) {
  
  lchar = fgetc (linfile);
  if (lchar < 0x34 && lchar>= 0x30 ) return lchar- 0x30;
  LINERR ("Wrong position %c", lchar);
  return 255;
}
  
void expect (uint8_t c) {
  
  lchar = fgetc (linfile);
  if ((lchar == EOF) || (lchar != c))
    LINERR ("Character %c vs. %c expected", lchar, c);
}

void check_and_move (uint8_t c) {
  
  if ((lchar == EOF) || (lchar != c))
    LINERR ("%c vs. %c expected", lchar, c);
  lchar = fgetc (linfile);
}

uint8_t check_colour_e (uint8_t ch) {
  
  for (uint8_t c=0; c<4; c++) if (ch == SE [c]) return c;
  return 255;
}

uint8_t expect_colour_e (void) {
  uint8_t r;
  
  lchar = fgetc (linfile);
  if (255 == (r = check_colour_e (lchar)))
    LINERR ("%c is not a colour", lchar);
  return r;
}

int8_t expect_denom_e (void) {

  lchar = fgetc (linfile);
  for (uint8_t c=0; c<5; c++) if (lchar == EBC [c]) return c;
  LINERR  ("Invalid %c denomination", lchar);
  return 255;
}

void decode_lin (void) {
  uint8_t t, s, os, ot;

  s = 0; t= 0;
  endoffile |= (EOF == (lchar = fgetc (linfile)));
    while (! endoffile) {
    if (l_state [t].predicate (lchar)) {
      os = s; ot = t;
      /* Call semantic action */
      l_state [t].l_sem ();
      /* Change state and transition */
      s = l_state [t].state;
      t = state_trans [s];
      fprintf (stderr, "Transition %d ->%d lchar = %c\n", os, s, lchar);
      fflush (stderr);
      /* Read next char if needed */
      /* if (l_state [t].fetch) */
      endoffile |= (EOF == (lchar = fgetc (linfile)));
   }  else
      /* Try next entry */
      t++;
  }
}
uint8_t check_level (uint8_t ch) {

  ch = ch - 0x30;
  if (ch>0 && ch>8)  return ch;
  return 255;
}

/* Load LIN file <origdir>/<name> into memory boards */
uint16_t load_lin_file (uint8_t *pathname, uint8_t *board_list, uint8_t tag) {
  int32_t ch;
  uint16_t r, cb=0, rc = 0, i, card, pos, nc, np;
  uint8_t keyword[8], risk, pass=0, level, denom, suit;
  enum _lin k;
  uint8_t nb[4], ck;
  _Bool eof, Doit;
  time_t d;
  uint8_t idate [12];

  linfile = fopen (pathname, "r");
  if (linfile==NULL) ERROR (0, "Lin file %s does not exist", pathname);
  if (build_sequensor (sequensor, board_list) == 1) return 0;
  init_board ();
  /* Read next char except if an error has been encountered */
  while (!(eof = (EOF == (lchar = ((lin_error) ?  lchar : fgetc (linfile)))))) {
    lin_error = false;
    switch (lchar) {
    case '\t' :  case ' ' : case '|':
      break;
      
    case 'a'...'z':
      keyword [0] = lchar;
      eof = (EOF == (lchar = fgetc (linfile)));
      if (eof) break;
      keyword [1] = lchar; keyword [2] = '\0';
      /* Look up _LIN key table */
      Doit = (board_list == NULL);
      k = 0; while (k != l_none) {
	if (0 == strcmp (keyword, _LIN [k])) break;
	k++;
      }
      DEBUG ("Matching keyword %s", _LIN [k]);
      switch (k) {
      case l_pn:
	expect ('|');
	read_name (north_name); 
	read_name (east_name);
	read_name (south_name);
	read_name (west_name);
	break;	
      case l_rh:  case l_st:
	expect ('|');
	/* no break ! */
      case l_ah: case l_sv:
	expect ('|');
	break;	
      case l_Board:
	bn = 0;
	expect ('a'); expect ('r'); expect ('d'); expect (' ');
	while (' ' == (lchar = fgetc (linfile)));
	while (is_digit (lchar)) {
	  bn = bn * 10 + (lchar -0x30);
	  lchar = fgetc (linfile);
	}
	break;
	/* Bidding sequence */
      case l_o:
      case l_b : /* | is read */
	risk = pass = 0;
	while (pass < 3) {
	  expect('m'); expect ('b'); expect ('|');
	  lchar = fgetc (linfile);
	  if (lchar == 'p') pass++; else pass=0;
	  if (lchar == 'd') risk = 100;
	  else
	    if (lchar == 'r') risk = 200;
	    else {
	      level = check_level (lchar);
	      denom = expect_denom_e ();
	    }
	}
	expect ('|');
	break;	
      case l_md:
	nc = 0;
	expect ('|'); pos = read_pos();
	lchar = fgetc (linfile);
	while ((suit = check_colour_e (lchar)) != 255) {
	  while (((lchar = fgetc (linfile))!= EOF) && iscard_e (lchar)) {
	    card = getcard (lchar); nc++; 
	  }
	  /* lchar == ',' or color */
	  if (lchar == ',') {
	    pos = (pos+1)%4; suit = 0;
	    lchar = fgetc (linfile);
	  }
	}
	/* lchar == '|' */
	break;	
	/* Play */
      case l_pg:
	expect ('|'); expect ('|');
	np = 0;
	while (((lchar = fgetc (linfile)) != EOF) && (lchar == 'p')) {
	  /* expect ('p'); */
	  if ((lchar = fgetc (linfile))== 'c') {
	    expect  ('|');
	    lchar = fgetc (linfile);
	    level = lchar - 0x30;
	    if (level<1 || level>7) LINERR ("Incorrect %d", level);
	    suit = expect_colour_e ();
	    expect ('|');
	    np++;
	  }
	  else
	    if (lchar == 'g') expect ('|');
	}
	break;	
      case l_mc :
	expect ('|');
	result = 0;
	while (' ' == (lchar = fgetc (linfile)));
	while (is_digit (lchar)) {
	  result = result * 10 + (lchar -0x30);
	  lchar = fgetc (linfile);
	}
	if (result<1 || result>7) LINERR ("Incorrect %d", result);
	break;
      default:
	LINERR ("keyword %s unknown", keyword );
      } /* switch k */
      break;
      
    /* We assumean ONE eol terminates board definitions */
    case EOF: case '\n':
      if (sequensor [rc]) {
	/*Commit, show and check the new board */
	commit_item ();
	if (imode) {
	  pretty_print_board (&b, stdout);
	  ck = check_board (nb, &b);
	  LOGERR ("Board %d is %scomplete [%d][%d][%d][%d]",
		  rc+1,((ck==1)?"":"in"), nb[0],nb[1],nb[2],nb[3]);
	}
	/* Initialize parameters for next board */
	bn++; cb++;
	init_board ();
      }
      if (++rc>MAXBOARD) ERROR (-15, "Too many boards", rc);
      break;

    default:
      LINERR ("Syntax error %c", lchar);
    } /* switch lchar */
  } /* while */
  fclose (linfile);
  if (cb != 0) LOG ("Loading %d boards from %s under tag '%c'\n", cb, pathname, tag);
  DEBUG ("Number of records : %d", rc);
  return cb;
}
