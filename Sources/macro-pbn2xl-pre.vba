Private Sub Workbook_Open()
' Version Module1
' Macro VBA to extract values from environment and populte an Excel file
' Keywords radicals - prefixed wuth PBN_

' "EVENT", "BOARD_NUMBER", "CONTRACT", "DECLARER",
' "SITE1", "SITE2", "SITE3", "SCHOOL",
' "DATE", "LEADCARD", "DEALER", "VULNRABILITY",
' "RESULT",
' "NORTHS", "NORTHH", "NORTHD", "NORTHC",
' "EASTS", "EASTH", "EASTD", "EASTC",
' "SOUTHS", "SOUTHH", "SOUTHD", "SOUTHC",
' "WESTS", "WESTH", "WESTD", "WESTC",
' "B11", "B12", "B13", "B14",
' "B21", "B22", "B23", "B24",
' "B31", "B32", "B33", "B34",
' "B41", "B42", "B43", "B44",
' "B51", "B52", "B53", "B54",
' "COM1", "COM2", "COM3", "COM4",
' "COM5", "COM6", "COM7", "COM8",
' "COM9", "COM10", "COM11", "COM12",
' "COM13", "COM14", "COM15"

Dim k(128) As String
Dim x(128) As Chars
Dim y(128) As Integer
k(0) = "EVENT"
x(0) = "L"
y(0) = 12
k(1) = "BOARD_NUMBER"
x(1) = "C"
y(1) = 6
k(2) = "CONTRACT"
x(2) = "A"
y(2) = 11
k(3) = "DECLARER"
x(3) = "A"
y(3) = 12
k(4) = "SITE1"
x(4) = "0"
y(4) = 0
k(5) = "SITE2"
x(5) = "0"
y(5) = 0
k(6) = ""
x(6) = "0"
y(6) = 0
k(7) = "SCHOOL"
x(7) = "0"
y(7) = 0
k(8) = "DATE"
x(8) = "M"
y(8) = 12
k(9) = "LEADCARD"
x(9) = "A"
y(9) = 3
k(10) = "DEALER"
x(10) = "A"
y(10) = 1
k(11) = "VULNERABILITY"
x(11) = "A"
y(11) = 2
k(12) = "RESULT"
x(12) = "0"
y(12) = 0
k(13) = "NORTHS"
x(13) = "D"
y(13) = 1
k(14) = "NORTHH"
x(14) = "D"
y(14) = 2
k(15) = "NORTHD"
x(15) = "D"
y(15) = 3
k(16) = "NORTHC"
x(16) = "D"
y(16) = 4
k(17) = "EASTS"
x(17) = "F"
y(17) = 5
k(18) = "EASTH"
x(18) = "F"
y(18) = 6
k(19) = "EASTD"
x(19) = "F"
y(19) = 7
k(20) = "EASTC"
x(20) = "F"
y(20) = 8
k(21) = "SOUTHS"
x(21) = "D"
y(21) = 9
k(22) = "SOUTHH"
x(22) = "D"
y(22) = 10
k(23) = "SOUTHD"
x(23) = "D"
y(23) = 11
k(24) = "SOUTHC"
x(24) = "D"
y(24) = 12
k(25) = "WESTS"
x(25) = "B"
y(25) = 5
k(26) = "WESTH"
x(26) = "B"
y(26) = 6
k(27) = "WESTD"
x(27) = "D"
y(27) = 7
k(28) = "WESTC"
x(28) = "D"
y(28) = 8
k(29) = "B11"
x(29) = "G"
y(29) = 2
k(30) = "B12"
x(30) = "H"
y(30) = 2
k(31) = "B13"
x(31) = "I"
y(31) = 2
k(32) = "B14"
x(32) = "J"
y(32) = 2
k(33) = "B21"
x(33) = "G"
y(33) = 3
k(34) = "B22"
x(34) = "H"
y(34) = 3
k(35) = "B23"
x(35) = "I"
y(35) = 3
k(36) = "B24"
x(36) = "J"
y(36) = 3
k(37) = "B31"
x(37) = "G"
y(37) = 4
k(38) = "B32"
x(38) = "H"
y(38) = 4
k(39) = "B33"
x(39) = "I"
y(39) = 4
k(40) = "B34"
x(40) = "J"
y(40) = 4
k(41) = "B41"
x(41) = "G"
y(41) = 5
k(42) = "B42"
x(42) = "H"
y(42) = 5
k(43) = "B43"
x(43) = "I"
y(43) = 5
k(44) = "B44"
x(44) = "J"
y(44) = 5
k(45) = "B51"
x(45) = "G"
y(45) = 6
k(46) = "B52"
x(46) = "H"
y(46) = 6
k(47) = "B53"
x(47) = "I"
y(47) = 6
k(48) = "B54"
x(48) = "J"
y(48) = 6
k(49) = ""
x(49) = "0"
y(49) = 0
k(50) = "COM1"
x(50) = "K"
y(50) = 1
k(51) = "COM2"
x(51) = "K"
y(51) = 2
k(52) = "COM3"
x(52) = "K"
y(52) = 3
k(53) = "COM4"
x(53) = "K"
y(53) = 4
k(54) = "COM5"
x(54) = "K"
y(54) = 5
k(55) = "COM6"
x(55) = "G"
y(55) = 6
k(56) = "COM7"
x(56) = "G"
y(56) = 7
k(57) = "COM8"
x(57) = "G"
y(57) = 8
k(58) = "COM9"
x(58) = "G"
y(58) = 9
k(59) = "COM10"
x(59) = "G"
y(59) = 10
k(60) = "COM11"
x(60) = "G"
y(60) = 11
k(61) = "COM12"
x(61) = "G"
y(61) = 12
k(62) = "COM13"
x(62) = "G"
y(62) = 13
k(63) = "COM14"
x(63) = "G"
y(63) = 14
k(64) = "COM15"
x(64) = "G"
y(64) = 15
k(65) = "STOP"
x(65) = 0
y(65) = 0

XLFile = Environ("PBN_EXCELFILE")
N$ = Environ("PBN_SHEET")
If N$ <> "" Then
    Num = CInt(N$)
'   Sheets("model").Copy After:=Sheets(Num)
'   Sheets("model (2)").Name = N$
End If
If Num <= 6 Then
    Sheets("Donnes 1-6").Select
Else
    Sheets("Donnes 7-12").Select
    Num = Num - 6
End If

LSIZE = 12 'Nombre de lignes par donne
iter = 0
While 0 <> StrComp(k(iter), "STOP")
A$ = Environ("PBN_" + k(iter))
If A$ <> "" Then
   c$ = CStr(((Num - 1) * LSIZE + y(iter)))
   Set rng = ActiveSheet.Range(x(iter) + c$).Resize(1, 1)
   rng.Value = A$
   iter = iter + 1
Wend

Application.DisplayAlerts = False
LAST$ = Environ("PBN_LAST")
If XLFile <> "" Then
    ActiveWorkbook.Save
    If LAST$ = "YES" Then
       Application.Quit
       Else
       Application.Quit
    End If
End If
End Sub





