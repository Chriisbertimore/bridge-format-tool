#include <string.h>
/* Check if a string represents a valid bid in language l */
_Bool check_bid (unsigned char *s, int l) {
  unsigned char c, d, x, xx, sz, s0, s1, t, t1=2, t2=2, t3=2, t4=2, t5=2, t6=2;
  
  /* Pass/p or "*"/fin or AP/TP are valid end of auction */
  sz = strlen (s);
  s0 = (sz>0)? s[0] : 0;
  s1 = (sz>1)? s[1]: 0;
  return
    (t1=(sz == 0))
    || (t2=(s0 == 'P'))
    || (t3=(s0 == 'X' && sz == 1))    
    || (t4=(s0 == 'X' && s1 == 'X' && sz == 2)) 
    || (t5=(s1 == 'P' && (s0 == 'A' || s0 == 'T'))) 
    || (t6=(s0 == '*' || s0 == 'F')) ;
}

void main(void) {
  int t;

t= check_bid ("", 1);
t= check_bid ("P", 1);
t= check_bid ("XX", 1);
t= check_bid ("AP", 1);
t= check_bid ("fin", 1);
t= check_bid ("3P", 1);
t= check_bid ("7S", 1);
t= check_bid ("AP", 1);
t= check_bid ("AP", 0);
}
