/*
 *  Utility programs to manipulate Bridge duplication files
 *  Christian Bertin - D�cembre 2018
 *  (christian.bert1@free.fr)
 *  Program source under GNU General Public License v2 and later
 */
#include <stdio.h>
#include <unistd.h>
#include <strings.h>
#include <malloc.h>
#include <libgen.h>
#include <stdarg.h>

#define ERROR(n, ...) do { fprintf (stderr, __VA_ARGS__); return n; } while(0)

#define BOARD_ELEMENT 13
/* 
 * Board representation : A board contains 4 hands, one per cardinal position
 * Each hand has 13 random cards, in 4 suits.
 * A card is defined by its suit [S,H,D,C] ands its height [A,K,Q,J,T,9,8,7,6,5,4,3,2,1]
 * Card number c is in [0-12], 0 for Ace .. 12 for Two 
 * Hand number h is in [0-3],  0 for North, 1 for astE, 2 for South, 3 for West
 * Table uchar bb[13] is a vector of 52 2-bit elements for encoding the hands
 * Byte 0 = [ AS.AH.AD.AC] ... byte 12 = [2S.2H.2D.2C] 
 * Example: bits 4-5 of bb[3] == 0b10) => South holds the Queen of Diamonds 
 */
struct board { 
  unsigned char bb [BOARD_ELEMENT];
  unsigned char tag;
  unsigned short number;
  struct board *next;
};
#define BOARD_SIZE (sizeof(struct board))
/* Static board buffer for BRI decoding */
unsigned char bri [BOARD_ELEMENT];
/* Dynamic board buffer for DGE decoding */
unsigned char *dge;
/* 
 * DUP Format definition: BRI record (78) + DGE record (68) + Control (10) 
 * BRI encoding: 
 * Only North, East and South hands are encoded 
 * A hand is 13 * 2 characters representing a number from 1 to 52 
 * Spade [01..13], Heart [14..26], Diamond [27..39], Club [40..52] in the order A, K,...,2.
 * DGE encoding:
 * North, East, South and West hands are encoded in 4 17-byte records:
 * S1, <n> card bytes, S2, <m> card bytes, S3 <p> card bytes, S4 <q> card bytes
 * By construction in a valid deal n + m + p + q == 13
 * Suit bytes Si is 0x06 for Spade, 0x03 for Heart, 0x04 for Diamond, 0x05 for Club 
 * Cards are represented by one ascii caracter among [A,K,Q,J,T,9,8,7,6,5,4,3,2,1] 
 * BRI and DGE decoding results must be identical
 */
#define BRI_RECORD_SIZE 78 /* 3 * 26 */
#define BRI_HAND_SIZE 26   /* 2 * 13 */
#define DGE_RECORD_SIZE 68  /* 4 * 17 */
#define DGE_HAND_SIZE 17  /* 13 + 4 */
#define CONTROL_AREA (BRI_RECORD_SIZE + DGE_RECORD_SIZE)
#define DUP_CONTROL_AREA_SIZE 10

/* DGE board linked list control variables */
struct board *item, *next, *last = NULL, *first = NULL;
enum algo { BRI, DGE, BRE };
char *algo_name[] = { "BRI", "DGE", "BRE" };
int algo_size [3] = { BRI_HAND_SIZE, DGE_HAND_SIZE, 0 };  
unsigned char ctrl [10];
#define DUP_RECORD_SIZE BRI_RECORD_SIZE + DGE_RECORD_SIZE + DUP_CONTROL_AREA_SIZE
unsigned char board_record [DUP_RECORD_SIZE];
unsigned int debug = 0;
unsigned int count = 0;
unsigned char tag ='a';

unsigned char tt[]  = "ARDVX98765432";
unsigned char tta[] = "AKQJT98765432";
unsigned short int mask [] = {
  0x8000, 0x4000, 0x2000, 0x1000, 0x0800, 0x0400, 0x0200,
  0x0100, 0x0080, 0x0040, 0x0020, 0x0010, 0x0008
};

unsigned short ascii [] = {
  /*0   1,  2,  3,  4,  5,  6,  7,  8,  9*/
  255,255,255, 14, 15, 16, 13,255,255,255,/*00*/
  255,255,255,255,255,255,255,255,255,255,/*10*/
  255,255,255,255,255,255,255,255,255,255,/*20*/
  255,255,255,255,255,255,255,255,255,255,/*30*/
  255,255,255,255,255,255,255,255,255,255,/*40*/
   12, 11, 10,  9,  8,  7,  6,  5,255,255,/*50*/
  255,255,255,255,255,  0,255,255,  2,255,/*60*/
  255,255,255,255,  3,  1,255,255,255,255,/*70*/
  255,  2,  1,255,  4,255,  3,255,  4,255,/*80*/
  255,255,255,255,255,255,255,255,255,255,/*900*/
  255,255,255,255,255,255,255,255,255,255,/*100*/
  255,255,255,255,255,255,255,255,255,255,/*110*/
  255,255,255,255,255,255,255,255         /*120*/
};

enum orientation { north, east, south, west };
unsigned char SC[] = { 0x6, 0x3, 0x4, 0x5 };
unsigned char mask1 [4] = { 0b00111111, 0b11001111, 0b11110011, 0b11111100 };
unsigned char mask0 [4] = { 0b11000000, 0b00110000, 0b00001100, 0b00000011 };
char *orientation [4] = { "Nord :", "Est  :", "Sud  :", "Ouest:" };
unsigned char S[4] = { 'P', 'C', 'K', 'T' };
unsigned char SA[4] = { 'S', 'H', 'D', 'C' };
unsigned char z[17][85];
unsigned short Y[] = { 30, 58, 30, 2 };
unsigned short X[] = { 3, 8, 13, 8 };
/* Strings s to be written in z at line x, column y) */
struct s { unsigned char *s; unsigned short int x, y; };
struct s tch [] = {
  { "Donne - ", 1, 0 },
  { "Donneur:   Vul:", 1, 56 },
  { "P-", 3, 28 }, { "P-", 8, 56 }, { "P-", 13, 28 }, { "P-", 8, 0 },
  { "C-", 4, 28 }, { "C-", 9, 56 }, { "C-", 14, 28 }, { "C-", 9, 0 },
  { "K-", 5, 28 }, { "K-", 10, 56 }, { "K-", 15, 28 }, { "K-", 10, 0 },
  { "T-", 6, 28 }, { "T-", 11, 56 }, { "T-", 16, 28 }, { "T-", 11, 0 },
  { "N", 8, 31 }, { "E", 9, 33 }, { "S", 10, 31 }, { "O", 9, 29 }
};

/* Write 2-bit position in byte board[card] in bit n, n+1 depending on suit */
void set (unsigned char *board, unsigned short card, unsigned short suit, unsigned short position) {
  unsigned char byte, m1;

  /* Set the 2-bit suit in board [card] to position value */
  byte = board [card]; m1 = mask1 [suit]; 
  board [card] = (byte & m1) | (position << (2*(3-suit)));
}

/* Read the 2-bit position from bits n, n+1 of byte board[card] depending on suit */
unsigned short get (unsigned char *board, unsigned short card, unsigned short suit){
  unsigned char byte, m0, position;

  /* Get orient value from 2-bit suit in board [card] */
  byte = board [card]; m0 = mask0 [suit];
  position = (byte & m0) >> (2*(3-suit));
  return position;
}

/* Initialize BRI and DGE board representations  */
void init (unsigned char a [], unsigned char b []) { 
  int i;
  
  /* Initialize all position values with 0x3 (West) for BRI complementary hand */
  for (i=0; i<BOARD_ELEMENT; i++) a [i]= b[i] = 0xFF; /* 4 times 0b11 */

}

/* Build z table |17, 85] with fixed strings */
void build_frame (void) {
  unsigned short int x, y, i;
  unsigned short l, c;
  unsigned char *p;
  
  for (x=0; x<17; x++) { for (y=0; y<84; y++) z[x][y]=' '; z[x][84] ='\0'; }
  for (i=0; i<sizeof(tch)/sizeof(struct s); i++) {
    x = tch[i].x; y = tch[i].y; p = tch[i].s; while (*p != '\0') z[x][y++] = *p++;
  }
}

/* Decode hands along BRI or DGE algorithm (a), 3 for BRI and 4 for DGE */
/* p points to the board record region to decode, limit gives the upper bound index */
void hand_decode (enum algo a, unsigned char *p, int limit, unsigned char *hand) {
  
  unsigned short int x;
  unsigned short v, suit, card, position, h;
  unsigned char c;
  unsigned short int size = algo_size[a];
  unsigned short int inc = (a == BRI) ? 2 : 1;
  suit = 0;
  for (x = 0; x < limit; x += inc) {
    position = (x / size); h = 4 * position;
    switch (a) {
    case BRI:
      v = (p[x]-0x30)*10+(p[x+1]-0x30);
      card = (v-1)%13;  suit = (v-1)/13;
      if (v>52) fprintf (stderr, "BRI: Invalid code at index %x\n", x);
      else set (hand, card, suit, position);
      break;
    case DGE:
      card = ascii [p[x]];
      if (card==255) fprintf (stderr, "DGE: Invalid code at index %x\n", x);
      if (card>12) suit = card - 13; 
      else set (hand, card, suit, position);
      break;
    default: 
      fprintf (stderr, "Algorithm non supported %d\n", a);
      return;
    } 
  }
}

int board_decode (unsigned char *b) {
  unsigned short int i;

 /* Decode 3 hands for BRI */
  hand_decode (BRI, b, 3*BRI_HAND_SIZE, bri);
  /* Decode 4 hands for DGE */
  hand_decode (DGE, b + BRI_RECORD_SIZE, 4*DGE_HAND_SIZE, dge);
  /* Ctrl is a static buffer */
  for (i=0; i<10; i++) ctrl[i] = b [BRI_RECORD_SIZE+DGE_RECORD_SIZE+i];
  return 0;
}

void board_encode (unsigned char *r, unsigned char *b, unsigned short count) {
  unsigned short int i, v, card, suite, position, i_bri, i_dge;
  unsigned char LAST[6];
  /* Encode 3 hands for BRI an DGE*/
  if (debug) for (i=0; i<DUP_RECORD_SIZE; i++) r [i] = 0xFF;
  i_bri =0; i_dge = BRI_RECORD_SIZE;
  for (position=0; position <4; position++) {  
    for (suite=0; suite<4; suite++) {
      r [i_dge++] = SC[suite];
      for (card=0; card<13; card++) {
	if ( get (b, card, suite) == position) {
	  v = 1+card+suite*13;
	  if (position<3) { r [i_bri++] = 0x30+v/10; r [i_bri++] = 0x30+v%10; }
	  r [i_dge++] = tta [card];
	}
      }
    }
  }
  for (i=0; i<7; i++) r [CONTROL_AREA+i] =  ctrl[i];
  sprintf (LAST, "%d", count); 
  i=0; 
  while ((i<3) && (LAST[i] !='\0')) { r [CONTROL_AREA+7+i] = LAST [i]; i++; }
  while (i<3) r [CONTROL_AREA+7+(i++)] = ' ';
}

void dump_board (enum algo a, unsigned char *board, unsigned char tag, unsigned short bn) {
  unsigned short m;
  unsigned short int h, suit, card, i;

  printf ("Encodage %s - Donne %c%02d:\n", algo_name[a], tag, bn);
  /* Dump encoding */
  printf ("Board address 0x%08x : ", board); 
  for (i=0; i<13; i++) printf ("0x%02x ", board[i]); printf ("\n");

  for (h=0; h<4; h++) {
    printf ("%s", orientation [h]);
    for (suit=0; suit<4; suit++) {
      printf ("%c-", S[suit]);
      for (card=0; card<BOARD_ELEMENT; card++) if (get (board, card, suit) == h) printf("%c ", tt[card]);
    }
    printf("\n");
  }
  fflush (stdout);
}

void pretty_print_board (unsigned int bn, unsigned char tag, unsigned char *board) {
   
  unsigned short m, x, y, y0;
  unsigned short int h, suit, i, card;
  
  build_frame ();
  z[1][9] = tag;  z[1][10] = 0x30+(bn/10); z[1][11] = 0x30+(bn%10); 
  for (h=0; h<4; h++ /* , board +=4 */) {
     x = X[h]; y0 = Y[h];
    for (suit=0; suit<4; suit++, x++) {
      y = y0; m = board [suit];
      for (card=0; card<BOARD_ELEMENT; card++) if (h == get (board, card, suit)) z [x][y++] = tt[card];
    }
  }
  for (x=0; x<17; x++) printf ("%s\n", z [x]);
  fflush (stdout);
}

struct board * remove_board (struct board *item, struct board *prec) {

  if (prec != NULL) prec->next = item->next;
  if (item == first) first = item->next;
  return item->next;  
}

int load_file (char *name, unsigned char tag) {
  unsigned int r, bn=0, i;
  /*DUP Input file */
  FILE *dupfile;
  
  dupfile = fopen (name, "r");
  if (dupfile == NULL) ERROR (-2, "File %s does not exist\n", name);
  while ((r = fread (board_record, DUP_RECORD_SIZE, 1, dupfile) == 1)) {
    if (r != 1) ERROR (-4, "Read error\n", name);
    bn++;
    item =(struct board *) malloc (BOARD_SIZE);  
    if (item == NULL) ERROR (-3, "Memory allocation problem\n");
    if (first == NULL) first = item;
    if (last != NULL) last->next = item; last = item; item->next = NULL;
    item->number = bn; item->tag = tag;
    dge = item->bb;
    init (bri, dge);
    board_decode (board_record);
    if (debug) { dump_board (BRI, bri, tag, bn); dump_board (DGE, dge, tag, bn); }
    /* Compare results */
    for (i=0; i<BOARD_ELEMENT; i++) if (bri[i]!=dge[i]) ERROR (-1, "Boards %d differ\n", bn);
  }
  fclose (dupfile);
  return bn;
}
int store_file (char *name, unsigned int count) {
  unsigned int r, i;
  FILE *destfile;
  
  destfile = fopen (name, "wb");
  if (destfile == NULL) ERROR (-2, "File %s cannot be created\n", name);
  item=first; while (item!=NULL) {
    board_encode (board_record, item->bb, count);
    fwrite (board_record, DUP_RECORD_SIZE, 1, destfile);
    item=item->next;
  }
  fclose (destfile);
  return r;
}

/* Assemble dup files; adup [-d] destfile [dupfile]* */
int adup (char *prog, int argc, char *argv[]) {
  unsigned short int i, n, r, count =0;
  unsigned char *destfile;

    if (argc < 2) ERROR (-1, "Syntax; adup [-d] destfile [dupfile]\n");  
     destfile = argv [1];
     for (i=2; i<argc; i++) {
      count += load_file (argv [i], tag++);
      fprintf (stderr, "Loading %s \n", argv [i]);
    }
    fprintf (stderr, "Creating %s \n", argv [1]);
    r = store_file (destfile, count);
    return r;
}

unsigned short int getnum (unsigned char *p) {
  unsigned short v = 0;
  while (((*p)<= 0x39)&&((*p)>=0x30)) { 
    v=v*10 + *(p++)-0x30; 
  } 
  return v;
}

/* Delete dup file records; ddup [-d] dupfile [n-m]*  */
int ddup (char *prog, int argc, char *argv[]) {
  unsigned short int i, n, r, count, index, deleted, min, max;
  struct board *prec;
  unsigned char *p; 
  unsigned char *destfile;
  
  if (argc == 2) ERROR (-1, "Syntax: ddup [-d] dupfile [<n>]|[<n>-<m>]\n");
  destfile = argv[1];
  count += load_file (destfile, tag++);
  index = 1; deleted = 0;
  while ((p = argv[2]) != NULL) {
    max = min = 0;
    while (((*p)<= 0x39)&&((*p)>=0x30)) { min=min*10 + *(p++)-0x30; } 
    switch (*p++) {
    case '\0': 
      max = min; break;
    case '-': case ',':
      while (((*p)<= 0x39)&&((*p)>=0x30)) { max=max*10 + *(p++)-0x30; }
      break;
    default:
      ERROR (-1, "Syntax; ddup [-d] dupfile [<n>]|[<n>-<m>]*\n");
    }
    if (min<index || max>count) ERROR (-5, "Invalid board number intervals\n");
    item=first; prec = NULL;
    while (item!=NULL && index<min) { 
      prec = item; item=item->next; index++;
    }
    while (item!=NULL && index<=max) { 
      item = remove_board (item, prec); deleted++; index++; 
    }
    argc--; argv++;
  }
  fprintf (stderr, "Deleting %d records\n", deleted);
  count -= deleted;
  r = store_file (destfile, count);
  return r;
}

/* Interactive mode */
int idup (char *prog, int argc, char *argv[]) {
  unsigned short int i, n, r, count =0;

  return 0;
}

/* Pivote dup file records: pdup [-d] dupfile [<board number>-<angle>]* */
int pdup (char *prog, int argc, char *argv[]) {
  unsigned short int i, n, r, index, bn, angle, shiftc, card, suit, position;
  struct board *prec;
  unsigned char *p; 
  unsigned char *destfile;
  
  if (argc<3) ERROR (-1, "Syntax: pdup [-d] dupfile [<board number>-<angle]\n");
  destfile = argv[1];
  count += load_file (destfile, tag++);
  while ((p = argv[2]) != NULL) {
    bn = angle = 0;
    while (((*p)<= 0x39)&&((*p)>=0x30)) { bn=bn*10 + *(p++)-0x30; } 
    switch (*p++) {
    case '-': case ',':
      while (((*p)<= 0x39)&&((*p)>=0x30)) { angle=angle*10 + *(p++)-0x30; }      shiftc = angle/90;
      break;
    case '\0': default:
      ERROR (-1, "Syntax: pdup [-d] dupfile [<board number>-<angle>]*\n");
    }
    if (bn<index || bn>count) ERROR (-5, "Invalid board number%d\n", bn);
    item=first; index = 1;
    while (item!=NULL && index<bn) { 
      item=item->next; index++;
    }
    for (card=0; card<13; card++) {
      for (suit=0; suit<4; suit++) { 
	position = get (item->bb, card, suit);
	position = (position + shiftc ) % 4;
	set (item->bb, card, suit, position);
      }
    }
    fprintf (stderr, "Pivoting board #%d by %d\n", bn, angle);
    argc--; argv++;
  }
  r = store_file (destfile, count);
  return r;
}

/* Show dup file records */
int rdup (char *prog, int argc, char *argv[]) {
  unsigned short int i, n, r, count =0;

  if (argc != 2) ERROR (-1, "Syntax: rdup [-d] dupfile\n");  
  count += load_file (argv [1], tag++);
  item=first; while (item!=NULL) {
    pretty_print_board (item->number, item->tag, item->bb);
    item=item->next;
  }
  return 0;
}

/* Split dup file: sdup [-d] dupfile [<radical>] */
int sdup (char *prog, int argc, char *argv[]) {
  unsigned short int i, fn, r;
  unsigned char *rad;
  char fname [128];
  FILE* destfile;

  if (argc>3) ERROR (-6, "Syntax: sdup dupfile [radical]");
  if (argc == 3) rad = argv[2]; else rad = basename (argv[1]); 
  count += load_file (argv [1], tag);
  item=first; fn = 1;
  while (item!=NULL) {
   sprintf (fname, "./%s-%02d", rad, fn); 
   destfile = fopen (fname, "wb");
   board_encode (board_record, item->bb, 1);
   r = fwrite (board_record, DUP_RECORD_SIZE, 1, destfile);
   fclose (destfile);
   fprintf (stderr, "Creating file %s \n", fname);
   item=item->next; fn++;
  }  
  return 0;
}

int getsuit(unsigned char c) {
  unsigned short i;

  for (i=0; i<4; i++) if (c==S[i]) return i;
  return -1;
}

/* Exchange cards within a dupfile record: xdup [-d] dupfile [<board#> XY-ZT]* */
int xdup (char *prog, int argc, char *argv[]) {
  unsigned short int i, n, r, index, bn;
  unsigned short int card1, suit1, position1;
  unsigned short int card2, suit2, position2;
  unsigned char c1, c2, s1, s2; 
  unsigned char *destfile, *p;
  
  if (argc<5) ERROR (-1, "Syntax: xdup [-d] dupfile [<board number> XY-UV ]*\n");
  destfile = argv[1];
  count += load_file (destfile, tag++);
  while (argc >= 3) {
    sscanf (argv[2], "%d", &bn);
    p = argv[3];
    card1 = card2 = suit1 = suit2 = 0;
    card1 = ascii [c1 = *p++]; suit1= getsuit(s1 = *p++);
    if (*p == '-' || *p == ',') { p++; card2 = ascii [c2 = *p++]; suit2 = getsuit(s2 = *p++); }
    else ERROR (-1, "Syntax: xdup [-d] dupfile [<boardnumber> XY-UV ]*\n");
    if (card1 == 0xFF || suit1 == 0xFF || card2 == 0xFF || suit2 == 0xFF) 
      ERROR (-5, " board #%d : Invalid card: %c%c or %c%c\n", bn, c1, s1, c2, s2);
    item = first; index = 1;
    while (item !=NULL && index < bn) { item=item->next; index++; } 
    position1 = get (item->bb, card1, suit1); 
    position2 = get (item->bb, card2, suit2);
    set (item->bb, card1, suit1, position2); 
    set (item->bb, card2, suit2, position1);
    fprintf (stderr, "Exchanging cards %c%c and %c%c in board #%d by %d\n", c1, s1, c2; s2, bn);
    argc--; argv++;
    }
  }
  r = store_file (destfile, count);
  return r;
}

int unknown (char *prog, int argc, char *argv []) {

  ERROR (-1, "Uknown program variant %s\n", prog);
}

struct cmd { 
  char *name; 
  int (*func)(char *prog, int argc, char *argv []);
} command [] = {
  "adup.exe", adup, /* assemble */
  "ddup.exe", ddup, /* delete */
  "idup.exe", idup, /* interactive */
  "pdup.exe", pdup, /* pivote */
  "rdup.exe", rdup, /* show */
  "sdup.exe", sdup, /* split */
  "xdup.exe", xdup, /* exchange */
  "",      unknown, /* Unknown */
};

int main (int argc, char *argv[]) {
  unsigned short int i, n, r, count =0;
  unsigned char *prog, *base;
  enum command { assemble, delete, interactive, pivote, show, split, exchange,none, } choice =none;

  if ((argc > 1) && (strcmp (argv [1], "-d") == 0)) {
    argc--; argv++;  debug = 1; 
  }
  n = strlen(argv[0]); prog = malloc (n+1); strcpy (prog, argv[0]);
  base = basename (prog);
  for (i=0; i!=none; i++) {
    if (strcmp (base, command [i].name) == 0) { 
      choice = i; 
      r = (*command [i].func) (prog, argc, argv);
      break;
    }
  }
  return r;
}
