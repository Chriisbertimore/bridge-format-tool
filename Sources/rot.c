#include <stdio.h>

#define BRI_RECORD_SIZE 78
#define DGE_RECORD_SIZE 68  /* 4 * 17 */
#define DUP_CONTROL_AREA_SIZE 10
#define DUP_RECORD_SIZE BRI_RECORD_SIZE + DGE_RECORD_SIZE + DUP_CONTROL_AREA_SIZE
#define DGE_HAND_SIZE 17  /* 12 + 4 */

unsigned char board [DUP_RECORD_SIZE];
FILE *dupfile;
/* Translation table 0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ */ 
unsigned char t[] = "--23456789:;<=>?@AbcdefghiVRlmnopDrsXuvwxyz";


void print_hand (unsigned char * p, int index) {
int limit = index + DGE_HAND_SIZE;
unsigned char c;

    while (index < limit) {
	c = p[index++];
	switch (c) {
	  case 0x06:
	       printf ("Piques: "); break;	
	  case 0X03: 
	       printf ("Coeurs: "); break;	
	  case 0X04: 
	       printf ("Carreaux: "); break;	
	  case 0X05: 
	       printf ("Trèfles: "); break;	
	  case '2':
	  case '3':
	  case '4':
	  case '5':
	  case '6':
	  case '7':
	  case '8':
	  case '9':
	  case 'T': 
	  case 'J':  
	  case 'Q': 
	  case 'K': 
	  case 'A': 
	       printf ("%c ", t[c-0x30] ); break;
	  default: 
	       printf ("Error: invalid character %d \n", c); 
	}
    }
}

void board_dump (unsigned  char *b) {
int i, r, control[10];
char fname [10] ;
static int count = 0;
FILE* out;
	
	for (i=0; i<10; i++) control[i] = b[147+i];
        count++;
        sprintf (fname, "./d%02d", count); 
	out = fopen (fname, "wb");
	fprintf (stderr, "Creating file %s \n", fname);
        r = fwrite (b, DUP_RECORD_SIZE, 1, out);
	fclose (out);
}

void rotate (unsigned char *b, int a) {
int i;
unsigned char *north = b+BRI_RECORD_SIZE + 0*DGE_HAND_SIZE;
unsigned char *east  = b+BRI_RECORD_SIZE + 1*DGE_HAND_SIZE;
unsigned char *south = b+BRI_RECORD_SIZE + 2*DGE_HAND_SIZE;
unsigned char *west  = b+BRI_RECORD_SIZE + 3*DGE_HAND_SIZE;
unsigned char tmp [DGE_HAND_SIZE*4], *d, *s;

   for (i=0; i<DGE_HAND_SIZE * 4; i++) tmp[i] = *(north+i);
   d = north;
   if (a==90)  d = east;
   if (a==180) d = south;
   if (a==270) d = west;
   for (s = tmp, i=0; i<DGE_HAND_SIZE*4; i++, s++, d++) { *d = *s; if (d == (west + DGE_HAND_SIZE)) d= north; }
}
int main (int argc, char *argv[]) {
int i, r, a;
FILE *fdest, *fdup;
char *dest;

   if (argc != 3) fprintf (stderr, "Syntax: rot dupfile [90|180|270]\n");
   dest = argv [1]; 
   sscanf (argv[2], "%d", &a);
   fdup = fopen (dest, "r+");
   r = fread (board, DUP_RECORD_SIZE, 1, fdup);
   rotate (board, a);
   fseek (fdup, 0L, SEEK_SET);
   fwrite (board, DUP_RECORD_SIZE, 1, fdup);
   fclose (fdup);
   fprintf (stderr, "Rotated %s by %d degrees \n", dest, a);
   fclose (fdup); 
   return r;
}
