#include <stdio.h>
#include <process.h>

int main (int argc, char *argv[]) {
  intptr_t r;
  
  r = _spawnlp (P_WAIT, "cmd.exe", "cmd.exe", "/c", "dir", "/p",  NULL);
  printf ("r = %x\n", r);
  r = _spawnlp (P_WAIT, "cmd.exe", "cmd.exe", "/c", "copy", "tt-01", "tt-011",  NULL);
  printf ("r = %x\n", r);
  r = _spawnlp (P_WAIT, "cmd.exe", "cmd.exe", "/c", "start emacs ",  NULL);
  printf ("r = %x\n", r);
  return 0;
}
