#include <inttypes.h>
#undef GEN
#define GEN(x) #x

uint8_t action_name[] = {
   GEN(donneur), GEN(bnum), GEN(vul), GEN(contrat),
   GEN(declarant), GEN(entame), GEN(resultat),
   GEN(o1), GEN(n1), GEN(e1), GEN(s1),
   GEN(o2), GEN(n2), GEN(e2), GEN(s2),
   GEN(o3), GEN(n3), GEN(e3), GEN(s3),
   GEN(o4), GEN(n4), GEN(e4), GEN(s4),
   GEN(o5), GEN(n5), GEN(e5), GEN(s5),
   GEN(np), GEN(nc), GEN(nk), GEN(nt),
   GEN(ep), GEN(ec), GEN(ek), GEN(et),
   GEN(op), GEN(oc), GEN(ok), GEN(ot),
   GEN(sp), GEN(sc), GEN(sk), GEN(st),
   GEN(c1), GEN(c2), GEN(c3), GEN(c4),
   GEN(c5), GEN(c6), GEN(c7), GEN(c8),
   GEN(c9), GEN(c10), GEN(c11),
   GEN(c12), GEN(c13), GEN(none)
};

#undef GEN
#define GEN(x) x
enum action {
   GEN(donneur), GEN(bnum), GEN(vul), GEN(contrat),
   GEN(declarant), GEN(entame), GEN(resultat),
   GEN(o1), GEN(n1), GEN(e1), GEN(s1),
   GEN(o2), GEN(n2), GEN(e2), GEN(s2),
   GEN(o3), GEN(n3), GEN(e3), GEN(s3),
   GEN(o4), GEN(n4), GEN(e4), GEN(s4),
   GEN(o5), GEN(n5), GEN(e5), GEN(s5),
   GEN(np), GEN(nc), GEN(nk), GEN(nt),
   GEN(ep), GEN(ec), GEN(ek), GEN(et),
   GEN(op), GEN(oc), GEN(ok), GEN(ot),
   GEN(sp), GEN(sc), GEN(sk), GEN(st),
   GEN(c1), GEN(c2), GEN(c3), GEN(c4),
   GEN(c5), GEN(c6), GEN(c7), GEN(c8),
   GEN(c9), GEN(c10), GEN(c11),
   GEN(c12), GEN(c13), GEN(none)
};
