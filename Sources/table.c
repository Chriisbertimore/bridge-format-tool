#include <stdio.h>

void main (void) {

  int i;
  for (i=0; i<255; i++) {
    if ((i%10)==0) printf ( "  ");
    printf ("%3d,", i);
    if ((i%10)==9) printf ("/*%3d*/\n", (i/10)*10);
  }
  printf ("255                 /*250*/\n");
  fflush (stdout);
}
