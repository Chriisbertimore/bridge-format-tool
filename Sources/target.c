/* Definition of template_table */
uint32_t tstart =0, tend =0, tsize =0;
uint8_t template_name [LINE_SIZE] = NULL;
#define MAX_NB_TEMPLATE 1
uint8_t tnumber = MAX_NB_TEMPLATE+1;
struct _template {
        uint8_t *tname; /* Template nick name */
	uint32_t tstart;/* Start index in driver_table */
	uint32_t tend;	/* End index in driver-table */
	uint32_t tsize;	/* Number of entries */
} template_table [MAX_NB_TEMPLATE+1] = { 
{"M12", 0, 55, 56 },
{"", 0, 0, 0 }
};
/* Definition of driver_table */
#define MAX_NB_DRIVER 56
struct _driver {
 	uint8_t dl;	/* line number */ 
	uint8_t dc;	/* Column number */ 
	uint8_t *dname;	/* Enum string */ 
} driver_table [MAX_NB_DRIVER+1] = { 
{ 0,0,"donneur" },
{ 0,3,"np" },
{ 0,10,"c1" },
{ 1,1,"vul" },
{ 1,4,"nc" },
{ 1,7,"o1" },
{ 1,8,"n1" },
{ 1,9,"e1" },
{ 1,10,"s1" },
{ 1,11,"c2" },
{ 2,1,"entame" },
{ 2,4,"nk" },
{ 2,7,"o2" },
{ 2,8,"n2" },
{ 2,9,"e2" },
{ 2,10,"s2" },
{ 2,11,"c3" },
{ 3,4,"nt" },
{ 3,7,"o3" },
{ 3,8,"n3" },
{ 3,9,"e3" },
{ 3,10,"s3" },
{ 3,11,"c4" },
{ 4,2,"op" },
{ 4,6,"ep" },
{ 4,7,"o4" },
{ 4,8,"n4" },
{ 4,9,"e4" },
{ 4,10,"s4" },
{ 4,11,"c5" },
{ 5,2,"oc" },
{ 5,3,"bnum" },
{ 5,6,"ec" },
{ 5,7,"o5" },
{ 5,8,"n5" },
{ 5,9,"e5" },
{ 5,10,"s5" },
{ 5,11,"c6" },
{ 6,2,"ok" },
{ 6,6,"ek" },
{ 6,7,"c7" },
{ 7,2,"ot" },
{ 7,6,"et" },
{ 7,7,"c8" },
{ 8,4,"sp" },
{ 8,7,"c9" },
{ 9,4,"sc" },
{ 9,7,"c10" },
{ 10,1,"contrat" },
{ 10,4,"sk" },
{ 10,7,"c11" },
{ 11,1,"declarant" },
{ 11,4,"st" },
{ 11,7,"c12"},
{0, 0, "niet" }
};
