/*
 *  Utility programs to manipulate Bridge duplication files
 *  Christian Bertin - June 2020
 *  (christian.bert1@free.fr)
 *  This program source is under GNU General Public License v2 and later
 */
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <strings.h>
#include <malloc.h>
#include <libgen.h>
#include <stdarg.h>
#include <dirent.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <stdlib.h>

#define TRUE 1
#define FALSE 0
#define forever while (1)
#define NOT !
#define LINE_SIZE 80
int debug = 1;
#define FATAL(n, ...)						\
  do { fprintf (stderr,__VA_ARGS__);  fprintf (stderr, "\n");   \
    exit (n);							\
  } while(0) 
#define ERROR(n, ...)						\
  do { fprintf (stderr,__VA_ARGS__);  fprintf (stderr, "\n");   \
    fflush (stderr); return n;					\
  } while(0)
#define _ERROR(...)						\
  do { fprintf (stderr,__VA_ARGS__);  fprintf (stderr, "\n");   \
    fflush (stderr); return;					\
  } while(0)
#define SYNTAX(p, ...)						\
  do { fprintf (stderr, "Syntax : %s", synth [p] __VA_ARGS__);	\
    fprintf (stderr, "\n"); fflush (stderr); return -1;		\
  } while(0)
#define LOGERR(...)					        \
  do { fprintf (stderr, __VA_ARGS__); fprintf (stderr, "\n");	\
    fflush (stderr);						\
  } while (0)
#define LOG(...)				                \
  do { fprintf (stdout, __VA_ARGS__);		                \
    fflush (stdout);				                \
  } while (0)
#define DEBUG(...)                                              \
  if (debug) 							\
    do { fprintf (stderr, __VA_ARGS__); fprintf (stderr, "\n");	\
      fflush (stderr);						\
    } while (0)
#define MAX_NB_TEMPLATE 64

/* Predicate functions */

_Bool is_N (uint8_t c) { return c == 'N'; }

_Bool is_o (uint8_t c) { return c == 'o'; }

_Bool is_r (uint8_t c) { return c == 'r'; }

_Bool is_d (uint8_t c) { return c == 'd'; }

_Bool is_E (uint8_t c) { return c == 'E'; }

_Bool is_s (uint8_t c) { return c == 's'; }

_Bool is_S (uint8_t c) { return c == 'S'; }

_Bool is_u (uint8_t c) { return c == 'u'; }

_Bool is_O (uint8_t c) { return c == 'O'; }

_Bool is_e (uint8_t c) { return c == 'e'; }

_Bool is_t (uint8_t c) { return c == 't'; }

_Bool is_1 (uint8_t c) { return c == '1'; }

_Bool is_0 (uint8_t c) { return c == '0'; }

_Bool  ucl (uint8_t c) { return ((c>='A')&&(c<='Z'));}

_Bool  lcl (uint8_t c) { return ((c>='a')&&(c<='z'));}

_Bool  acc (uint8_t c) { return
    (c=='�')||(c=='�')||(c=='�')
    ||(c=='�')||(c=='�')||(c=='�')||(c=='�')
    ||(c=='�')||(c=='�')
    ||(c=='�')||(c=='�')
    ||(c=='�')||(c=='�')||(c=='�')
    ||(c=='�'); }

_Bool digit (uint8_t c) { return ((c>='0')&&(c<='9'));}

_Bool letter (uint8_t c) { return ucl(c)|lcl(c)|acc(c); }

_Bool card (uint8_t c) { return
    (c=='A')||(c=='R')||(c=='D')||(c=='V')||(c=='X')||((c>='1')&&(c<='9')); }

_Bool colour (uint8_t c) { return (c=='P')||(c=='C')||(c=='K')||(c=='T'); }

_Bool is_nul (uint8_t c) { return c == '\0'; }

_Bool is_ (uint8_t c) { return c == '-'; }

_Bool ispoint (uint8_t c) { return c == '.'; }

_Bool lambda (uint8_t c) { return TRUE; }


FILE *ptfile; /* template.c */
FILE *ptemp; /* *.def files to append */
#define SIZE_LINE 80
uint8_t line [SIZE_LINE];
uint8_t  *ptfilename = "Sources/template.c";
/* template_table definition */
uint8_t *s1 =
  "struct _template { \n"
  "\tuint8_t *tname;\t/* Name of the template */\n"
  "\tuint32_t tstart;\t/* Start index in driver_table */\n"
  "\tuint32_t tend;\t/* End index in driver-table */\n"
  "\tuint32_t tsize;\t/* Number of entries */\n"
  "} template_table [MAX_NB_TEMPLATE+1] = { \n";

uint8_t *s2 =
  "struct _driver {\n"
  "\tuint8_t dl;\t/* line number */ \n"
  "\tuint8_t dc;\t/* Column number */ \n"
  "\tuint8_t *dname;\t/* Enum string */ \n"
  "} driver_table [MAX_NB_DRIVER+1] = { \n";

#define MAX_NAME 32
uint8_t  *nicktn [MAX_NAME];
uint32_t  nickts [MAX_NAME];
uint32_t  nickte [MAX_NAME];
uint32_t  nicktz [MAX_NAME];
uint8_t   tnumber;

/* Valid name is  ([a-z]|{A-Z]|[0-9]|-|.)* with accentq */
_Bool check (uint8_t *name) {
  uint8_t ch, i, sz;
  sz = strlen (name);
  for (i=0; i<sz; i++) {
    ch = name [i];
    if (digit (ch) || letter (ch) || is_(ch) || ispoint (ch)) continue;
    else return FALSE;
  }
  return TRUE;
}

/* Search templates files in directory tdir */
int tsearch (uint8_t *tdir) {
  uint8_t  i, *dname, sz, *ps, *pe, tnum;
  DIR *dir;
  struct dirent *ent;
  struct stat s;
  tnum = 0;
  if ((dir = opendir (tdir)) == NULL) ERROR (-2, "Could not open %s", tdir);
  if (chdir (tdir) == -1) ERROR (-3, "Could not chdir to %s", tdir); 
  DEBUG ( "Entering %s directory", tdir);
  while ((ent = readdir (dir)) != NULL) {
    dname = ent->d_name;
    sz = strlen (dname); ps = dname; pe = dname + sz - 4;
    stat (dname, &s);
    /* Does it match "<model>.def" ? */
    if ((s.st_mode & S_IFREG) && check (dname) && strcmp (pe, ".def") == 0) {
      *pe = 0;
      pe = (uint8_t *) malloc (strlen (ps)+1); strcpy (pe, ps);
      if (tnum >= MAX_NB_TEMPLATE-1) ERROR (-4, "Too many templates");
      nicktn [tnum++] = pe;
      DEBUG ("Adding valid name %s", pe);
    }
  }
  nicktn [tnum] = 0;
  DEBUG ("Leaving %s directory :", tdir);
  if (chdir ("..") == -1) ERROR (-5, "Could not chdir to ..");
  closedir (dir);
  return tnum;
}

int main (int argc, char *argv []) {
  uint8_t it, tnumber;
  uint32_t ts, te, tz, inc, linenbr;
  uint8_t pathname [1024];
  
  ptfile = fopen (ptfilename, "w");
  if (ptfile == NULL) ERROR (-1, "Cannot open %s for writing");
  
  if ((tnumber = tsearch ("Templates")) == 0) ERROR (-2, "No def files");
  inc = 0;
  
  /* Pass One : visit all .def files in Template directory */
  for (it = 0; it<tnumber; it++) {
    strcpy (pathname, "Templates/"); strcat (pathname, nicktn [it]);
    strcat (pathname,".def");
    ptemp = fopen (pathname, "r");
    if (ptemp == NULL) ERROR (-6, "Cannot open %s", pathname);
    /* First line contains parameters : tname, tstart, tend and tsz */
    fscanf (ptemp,"%[0-9a-zA-Z-], %d, %d, %d\n", line, &ts, &te, &tz);
    if (strcmp (nicktn [it], line) != 0)
      ERROR (-9, "First line is incoherent with template name, %s!=%s",
	     line, nicktn [it]);
    nickts [it] = ts+inc;
    nickte [it] = te+inc-1;
    nicktz [it] = tz;
    DEBUG ("Template # %d, tname=%s, tstart=%d tend=%d tsize=%d",
	   it, line, ts+inc, te+inc-1, tz);
    inc += tz;
    fclose (ptemp);
  }

  /* Pass Two ; writing template_table contents */
  /* Writing start of "template.c" */
  fprintf (ptfile, "/* Definition of template_table */\n");
  fprintf (ptfile, "#define MAX_NB_TEMPLATE %d\n", tnumber);
  fprintf (ptfile, "/* Number of templates */\n");
  fprintf (ptfile, "uint8_t tnumber = %d;\n", tnumber);
  fprintf (ptfile, "/* Start and end index in driver_table */\n");
  fprintf (ptfile, "uint32_t tstart=0, tend=0, tsize=0;\n");
  fprintf (ptfile, "/* Template name */\n");
  fprintf (ptfile, "uint8_t template_name [%d];\n", LINE_SIZE);
  fprintf (ptfile, s1);
  /* Initializer */ 
  for (it=0; it<tnumber; it++) 
    fprintf (ptfile, "{\"%s\", %d, %d, %d, },\n",
	     nicktn[it], nickts[it], nickte[it], nicktz[it]);
  fprintf (ptfile, "{\"\", 0, 0, 0 }\n};\n");
  
  /* Pass Three ; writing driver_table contents, re-opening *.def files */
  fprintf (ptfile, "/* Definition of driver_table */\n");
  fprintf (ptfile, "#define MAX_NB_DRIVER %d\n", inc);
  fprintf (ptfile, s2);
  linenbr = 0;
  for (it = 0; it<tnumber; it++) {
    strcpy (pathname, "Templates/"); strcat (pathname, nicktn [it]);
    strcat (pathname,".def");
    ptemp = fopen (pathname, "r");
    fgets (line, SIZE_LINE, ptemp);
     while (NULL != fgets (line, SIZE_LINE, ptemp)) {
      fprintf (ptfile, "/* %s : %4d */ %s", nicktn [it], linenbr, line);
      linenbr++;
    }
    fclose (ptemp);
  }
  fprintf (ptfile, "{0, 0, \"niet\" }\n};\n");
  fclose (ptfile);
  LOG ("Generated file available in template.c\n");
  fflush (stdout);
  return 0;
}
