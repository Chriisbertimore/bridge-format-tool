# Central installation 
# ROOTDIR:=c:/Home-Christian/Dropbox/Dup-utils/Sources
VPATH=C:/Home-Christian/Dropbox/Private/Dup-utils/Sources
ROOTDIR:="C:/Program Files (x86)/Dup-utils"

# There are some logical dependencies between idup data organization and
# gen-template, but there is no data definition shared between the two modules.
# The decision to rebuild gen-template follows the decision to align it to
# a change in idup. An example is the number of lines to describe a board in a 
# model excel file. It is fixed to 12 in idup-v3, but should be parameterized
# and read from the first line of M<nn>.def as a fourth parameter.
#
gen-template.exe : gen-template.c
	gcc -g -Bstatic -o $@ $^
# M<nn>.def files are generated using the xm <csvfile> command, e.g.
# 	idup> xm M12.csv
# M12.csv file is obtained by selecting the first sheet of the M12.xslx model,
# editing it and exporting it as a DOS CSV format as M12.csv.
# M20.csv is the virtual concatenation of M20-1.csv, M20-2.csv and M20-3.csv where
# M20-1 is the first sheet of M20.xlsx "Exercices (1)" annotated and M20-3 is the third one  
# Each field where a value is expected must be filled with a keyword string 
# defining the semantics of the field,  e.g. very first field [A,1] is 'donneur'
# meaning that a letter from 'N', 'E', 'S', 'O' is expected in this field by 
# 	idup> xl M12 <boards>.csv
# Valid keywords are :
#   donneur, bnum, tag, vul, contrat, declarant, entame, resultat, ckn, bid11... 
#   bid64,  np... ot, c1... c16, event, site_, school, date_, niet
#
# gen-template.exe must be rerun when a new M<nn>.def is added in Templates,
# or when a M<nn>.def file is modified.
#
Models := $(wildcard Templates/M[0-9]*.def)
template.c : gen-template.exe $(Models)
	echo Models $(Models) are tabulated in template.c
	gen-template.exe
#
# The circular dependency of idup.exe to itself is solved by filling by hand
# the very first file template.c.
#
idup.exe : dup-utility-v3.c template.c
	gcc -g -o $@ -Bstatic $<

symlinks :
	symlinks .

all :	gen-template.exe idup.exe symlinks

clean :
	del /F/Q idup.exe gen-template.exe d2l.exe d2p.exe d2x.exe l2d.exe l2p.exe l2x.exe p2d.exe p2l.exe p2x.exe x2d.exe x2l.exe x2p.exe

# For mysterious reason, administrator permission is needed to copy idup.cfg ...
# wheras the execuable causes no issue
install : all
	copy /Y idup.exe $(ROOTDIR)
	copy /Y idup.cfg $(ROOTDIR)
	symlinks $(ROOTDIR)

dup-utility-v3.i : dup-utility-v3.c
	gcc -E $< >$@i
#
# Miscellaneous test programs
#
icsv.exe : icsv.c
	gcc -g -o $@ -Bstatic $<
gm.exe : gm.c
	gcc -g -o $@ -Bstatic $<
gm.c  :  g.c
	gcc -g -o $@ -Bstatic $<
f.exe :  f.c
	gcc -g -o $@ -Bstatic $<
w.exe :  w.c
	gcc -g -o $@ -Bstatic $<
r.exe :  r.c
	gcc -g -o $@ -Bstatic $<
rd.exe : crd.c
	gcc -g -o $@ -Bstatic $<
# linsaef.c is a stand-alone parser of lin file. It has been integrated indup-uility-v3.c
linaef.exe : linaef.c
	gcc -g -o $@ -Bstatic $<
